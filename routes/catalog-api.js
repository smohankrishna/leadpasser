var express = require("express");
var router = express.Router();

var mlSvc = require("../services/masterlist.js");
var jwt = require('jsonwebtoken');
const env = require('../environment');

router.use(function (req, res, next) {
    
    var token = req.header("app-auth-token");
    
    validateAuthToken(token, function (result, data) {
        if (result === true) {
            req.uinfo = data.uinfo;
            next();
        } else {
            res.sendStatus(403);
        }
    });
});

var validateAuthToken = function (token, callback) {
    jwt.verify(token, env.api.secret, function (err, decoded) {
        if (err) {
            callback(false);
        } else {
            callback(true, decoded);
        }
    });
};

router.get("/", function (req, res, next) {

    var filter = {
        code: '_product_catalog',
        parent_uid: null
    };

    mlSvc.getDetailsByCode(filter.code, filter.parent_uid, function (err,
            results, rowCount) {

        var output = {
            error: err,
            details: (results && results.length === 1) ? results[0] : {}
        };

        res.status(200).json(output);
    });
});

router.get("/fullcatalog", function (req, res, next) {


    mlSvc.getCustomlists("get_productcatalogmaster", function (err, results) {

        //Need to change beacuse of android, ios
        if(req.header("isFromMobile")){
			results.splice(results.length-1,1);
		}
        var output = {
            error: err,
            details: results
        };

        console.log();

        res.status(200).json(output);
    });
});

router.get("/categories", function (req, res, next) {

    var filter = {
        code: '_product_catalog',
        parent_uid: null
    };

    mlSvc.getDetailsByCode(filter.code, filter.parent_uid, function (err,
            results, rowCount) {
        var catalog = (results && results.length === 1) ? results[0] : {};

        if (catalog.uid) {
            var catalogFilter = {
                parent_uid: catalog.uid
            };

            mlSvc.getMasterlists(catalogFilter,
                    function (err, results, rowCount) {
                        var output = {
                            error: err,
                            list: results,
                            rowCount: rowCount || 0
                        };

                        res.status(200).json(output);
                    });
        } else {
            var output = {
                error: err,
                list: [],
                rowCount: 0
            };
            res.status(200).json(output);
        }
    });
});

router.get("/products/:category", function (req, res, next) {

    var filter = {
        parent_uid: req.params.category
    };

    mlSvc.getMasterlists(filter, function (err, results, rowCount) {
        var output = {
            error: err,
            list: results,
            rowCount: rowCount || 0
        };

        res.status(200).json(output);
    });
});

router.get("/checkcode", function (req, res, next) {

    var code = req.query.code;
    var parent_uid = req.query.parent_uid;

    if (!code) {
        res.status(401).send("Unauthorized Call");
        return;
    }

    mlSvc.checkForListCode(code, parent_uid, function (err, rowCount) {
        var output = {
            error: err,
            result: (rowCount > 0) ? false : true
        };
        res.status(200).json(output);
    });
});

router.get("/details/:uid", function (req, res, next) {

    var uid = req.params.uid;

    if (!uid) {
        res.status(401).send("Unauthorized Call");
        return;
    }

    mlSvc.getDetailsByUid(uid, function (err, results) {
        var output = {
            error: err,
            details: (results.length === 1) ? results[0] : {}
        };
        res.status(200).json(output);
    });
});

router.post("/details/insert", function (req, res, next) {

    if (!req.body || !req.body.mlinfo) {
        res.status(401).send("Invalid Call");
        return;
    }

    var mlInfo = req.body.mlinfo;

    mlSvc.insertList(mlInfo, function (err, result) {

        var output = {
            error: err,
            result: result
        };
        res.status(200).json(output);
    });
});

router.post("/details/update", function (req, res, next) {

    if (!req.body || !req.body.mlinfo) {
        res.status(401).send("Invalid Call");
        return;
    }

    var mlInfo = req.body.mlinfo

    if (!mlInfo.uid) {
        res.status(401).send("Invalid Call Data");
        return;
    }

    mlSvc.updateList(mlInfo, function (err, result) {
        console.log(err);
        var output = {
            error: err,
            result: result
        };
        res.status(200).json(output);
    });
});

module.exports.route = router;