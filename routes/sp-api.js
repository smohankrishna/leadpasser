var express = require("express");
var router = express.Router();

var spSvc = require("../services/sp.js");

var jwt = require('jsonwebtoken');
const env = require('../environment');


router.use(function (req, res, next) {
    
    var token = req.header("app-auth-token");
    
    validateAuthToken(token, function (result, data) {
        if (result === true) {
            req.uinfo = data.uinfo;
            next();
        } else {
            res.sendStatus(403);
        }
    });
});

var validateAuthToken = function (token, callback) {
    jwt.verify(token, env.api.secret, function (err, decoded) {
        if (err) {
            callback(false);
        } else {
            callback(true, decoded);
        }
    });
};

router.get("/catalog/:spuid", function(req, res, next) {

	var options = null;
	var sp_uid = req.params.spuid;

	if (req.query.options) {
		options = JSON.parse(req.query.options);
	}

	if (!options) {
		options = {
			limit : 999,
			offset : 0
		}
	}

	spSvc.getSPCatalog(sp_uid, options, function(err, results, rowCount) {
		var output = {
			error : err,
			list : results,
			rowCount : rowCount
		};
		res.status(200).json(output);
	});
});

router.post("/catalog/insert", function(req, res, next) {

	if (!req.body || !req.body.data) {
		res.status(401).send("Invalid Call");
		return;
	}

	var catalogItem = req.body.data

	spSvc.insertCatalogItem(catalogItem, function(err, result) {
		if(err){
			console.log(err);
		}
		
		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});

router.post("/catalog/update/:uid", function(req, res, next) {

	if (!req.body || !req.body.data) {
		res.status(401).send("Invalid Call");
		return;
	}

	var uid = req.params.uid;
	var catalogItem = JSON.parse(JSON.stringify(req.body.data));

	if (uid != catalogItem.uid) {
		res.status(401).send("Invalid Call");
		return;
	};

	spSvc.updateCatalogItem(catalogItem, function(err, result) {
		if(err){
			console.log(err);
		}

		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});


/**
 * SP Locations
 * 
 * ***/
router.get("/locations/:spcataloguid", function(req, res, next) {

	var options = null;
	var sp_catalog_uid = req.params.spcataloguid;

	if (req.query.options) {
		options = JSON.parse(req.query.options);
	}

	if (!options) {
		options = {
			limit : 999,
			offset : 0
		}
	}

	spSvc.getSPCatalogLocations(sp_catalog_uid, options, function(err, results, rowCount) {
		var output = {
			error : err,
			list : results,
			rowCount : rowCount
		};
		res.status(200).json(output);
	});
});

router.post("/locations/insert", function(req, res, next) {

	if (!req.body || !req.body.data) {
		res.status(401).send("Invalid Call");
		return;
	}

	var locationData = req.body.data

	spSvc.insertSPCatalogLocation(locationData, function(err, result) {
		if(err){
			console.log(err);
		}
		
		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});

router.post("/locations/remove/:uid", function(req, res, next) {

	var uid = req.params.uid;

		spSvc.removeSPCatalogLocation(uid, function(err, result) {
		if(err){
			console.log(err);
		}

		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});


module.exports.route = router;