var express = require("express");
var router = express.Router();
//var http = require("http");


var userSvc = require("../services/user.js");
var env = require('../environment.json');
var webSmsSrv = require("../services/websms.js");

router.use(function (req, res, next) {
    next();
});

router.get("/authenticate", function (req, res, next) {

    var username = req.query.username;
    var passwd = req.query.passwd;
    var mobile = req.query.mobile;
    var otp = req.query.otp;

    if ((!mobile && !username) || (!passwd && !otp)) {
        res.status(401).send("Unauthorized Call");
        return;
    }

    userSvc.authenticate(mobile,username, passwd, otp, function (err, success, userInfo, token) {
        var output = {
            error: err,
            authenticated: success,
            userinfo: userInfo
        };
        
        if (token) {
            res.setHeader('app-auth-token', token);
        }
        
        res.status(200).json(output);
    });
});

router.get("/list/:type", function (req, res, next) {

    var utype = req.params.type;

    var options = JSON.parse(req.query.options);
    var extraFilter = JSON.parse(req.query.filter);
    var searchFilter = req.query.search;

    if (searchFilter) {
        searchFilter = JSON.parse(searchFilter);
    }

    if (!utype) {
        res.status(401).send("Unauthorized Call");
        return;
    }

    if (!options) {
        options = {
            limit: 999,
            offset: 0
        };
    }

    var filter = {
        utype: utype
    };

    for (var p in extraFilter) {
        filter[p] = extraFilter[p];
    }

    // userSvc.getUserListByType(utype, options, function(err, results) {
    userSvc.getUserListByFilter(filter, searchFilter, options, function (err,
            results, rowCount) {
        var output = {
            error: err,
            list: results,
            rowCount: rowCount
        };
        res.status(200).json(output);
    });
});

router.get("/details/:uid", function (req, res, next) {

    var uid = req.params.uid;

    if (!uid) {
        res.status(401).send("Unauthorized Call");
        return;
    }

    userSvc.getUserDetails(uid, function (err, results) {
        var output = {
            error: err,
            details: (results.length === 1) ? results[0] : {}
        };

        res.status(200).json(output);
    });
});

router.get("/detailsbyuname/:uname", function (req, res, next) {

    var uname = req.params.uname;

    userSvc.getUserDetailsByUname(uname, function (err, results) {
        var output = {
            error: err,
            details: (results.length === 1) ? results[0] : {}
        };
        res.status(200).json(output);
    });
});


router.get("/usernamecheck", function (req, res, next) {

    var uname = req.query.uname;

    if (!uname) {
        res.status(401).send("Unauthorized Call");
        return;
    }

    userSvc.checkForUsername(uname, function (err, rowCount) {
        var output = {
            error: err,
            result: (rowCount > 0) ? false : true
        };
        res.status(200).json(output);
    });
});

router.get("/validateotp", function (req, res, next) {

    var uname = req.query.uname;
    var userOTP = req.query.otp;
    var mobilenum =  req.query.mobilenum;
    var signin = req.query.signin;

    if ((!mobilenum && !uname) || !userOTP) {
        res.status(401).send("Unauthorized Call");
        return;
    }

    userSvc.validateOTP(uname, mobilenum,userOTP, function (result) {
        var output = {
            valid: result
        };
        res.status(200).json(output);
    });
});

router.get("/mobilenumcheck", function (req, res, next) {

    var mobilenum = req.query.mobilenum;

    if (!mobilenum) {
        res.status(401).send("Unauthorized Call");
        return;
    }

    userSvc.getUserDetailsByMobileNum(mobilenum, function (err, data, rowCount) {
        var output = {
            error: err,
            result: (rowCount > 0) ? false : true
        };
        res.status(200).json(output);
    });
});

router.post("/details/insert", function (req, res, next) {

    if (!req.body || !req.body.userinfo) {
        res.status(401).send("Invalid Call");
        return;
    }

    var uinfo = req.body.userinfo;
    userSvc.insertUserDetails(uinfo, function (err, result) {
        console.log(err);
        var output = {
            error: err,
            result: result
        };
        res.status(200).json(output);
    });
});

router.post("/details/registeruser", function (req, res, next) {

    if (!req.body || !req.body.userinfo) {
        res.status(401).send("Invalid Call");
        return;
    }

    var mcodevalidity = new Date();
		//console.log(mcodevalidity);
		mcodevalidity = mcodevalidity
				.setMinutes(mcodevalidity.getMinutes() + 20);  //validity for 20 mins
    var uinfo = req.body.userinfo;
    var to = uinfo.mobilenum;

    uinfo.mcodevalidity = new Date(mcodevalidity);
    uinfo.utype = "lg";
    uinfo.mcode = Math.floor(100000 + Math.random() * 999999);   //6 digit random number
    uinfo.active = false;

    var text = "Your LeadPassers activation OTP is ${otp}. This is valid for 15 min.";
    text = text.replace("${otp}", uinfo.mcode);

    userSvc.insertUserDetails(uinfo, function (err, result) {
        console.log(err);
        var output = {
            error: err,
            result: result
        };

        if(!err)
{
    var smsresponse = webSmsSrv.sendSms(to,text,function(error,object){
        output.smsResult=object;
        return res.status(200).json(output);
    });
}else{
        res.status(200).json(output);}
    });
});



router.post("/details/update", function (req, res, next) {
   console.log('---> In update user \nreq : '+JSON.stringify(req.body));

    if (!req.body || !req.body.userinfo) {
        res.status(401).send("Invalid Call");
        return;
    }

    var uinfo = req.body.userinfo;

    if (!uinfo.uid) {
        res.status(401).send("Invalid Call Data");
        return;
    }

    userSvc.updateUserDetails(uinfo, function (err, result) {
        var output = {
            error: err,
            result: result
        };
        res.status(200).json(output);
    });
});



router.get("/bankdetails/:uid", function (req, res, next) {

    var uid = req.params.uid;
    if (!uid) {
        res.status(401).send("Unauthorized Call");
        return;
    }
    userSvc.getUserBankDetails(uid, function (err, results) {
        var output = {
            error: err,
            details: (results.length > 0) ? results[0] : {}
        };
        
        res.status(200).json(output);
    });
});

router.post("/bankdetails/insert", function (req, res, next) {

    if (!req.body || !req.body.bankinfo) {
        res.status(401).send("Invalid Call");
        return;
    }

    var bankinfo = req.body.bankinfo;

    userSvc.insertUserBankDetails(bankinfo, function (err, result) {
        console.log(err);
        var output = {
            error: err,
            result: result
        };
        res.status(200).json(output);
    });
});



router.post("/bankdetails/update", function (req, res, next) {

    if (!req.body || !req.body.bankinfo) {
        res.status(401).send("Invalid Call");
        return;
    }

    var bankinfo = req.body.bankinfo

    if (!bankinfo.uid) {
        res.status(401).send("Invalid Call Data");
        return;
    }

    userSvc.updateUserBankDetails(bankinfo, function (err, result) {
        var output = {
            error: err,
            result: result
        };
        res.status(200).json(output);
    });
});

module.exports.route = router;