var express = require("express");
var router = express.Router();

var userSrv = require("../services/user.js");
var emailSrv = require("../services/email.js");
var webSmsSrv = require("../services/websms.js");

router.use(function(req, res, next) {
	next();
});

router.get("/", function(req, res, next) {
	gotoHome(req, res, next);
});

router.post("/sendmail", function(req, res, next) {
	sendmail(req, res, next);
});

router.post("/sendtext", function(req, res, next) {
	sendText(req, res, next);
});

/** *********************************************** */
var gotoHome = function(req, res, next) {
	var result = {
		error : "You are NOT suppose to be here"
	};

	res.status(401).send("Unauthorized Call");
};
/** *********************************************** */

var sendmail = function(req, res, next){
	
	// CHECK HEADER
	
	var toList = req.body.to;
	var ccList = req.body.cc;
	var bccList = req.body.bcc;
	var subject = req.body.subject;
	var text = req.body.text;
	var html = req.body.html;
	
	emailSrv.sendMail(toList, ccList, bccList, subject, text, html, function(err, response){
		if(err){
			res.status(500).send(err);
		}else{
			res.status(200).send(response);
		}
	})
};

var sendText = function(req, res, next){

	console.log("/n/n Send text request fromMobileApp : "+req.body.fromMobileApp);
	var to = req.body.to;
	var text = req.body.text;
	var fromMobileApp = req.body.fromMobileApp;

	webSmsSrv.sendSms(to, text, function(err, response){
		if(err){
			res.status(500).send(err);
		}else{
			var out = {
					result : response
			}
			if(fromMobileApp){
                var OTP_Regex = /[0-9]{6}[.]/g;
                let otp = String(text.match(OTP_Regex)).replace(".","");
                userSrv.getUserDetailsByMobileNum(to,function (err,results,rowCount) {
                    let uinfo;
                    if (results && results.length === 1) {
                        uinfo = results[0];

                        uinfo.mcode = otp;
                        uinfo.mcodevalidity = new Date();

                        userSrv.updateUserDetails(uinfo,function (err,response) {
                            if(err){
                                return res.status(500).send("Updation failed");
                            }
                        });

                    } else {
                        return res.status(400).send("Got " + results.length + " user for same mobile number");
                    }
                });
			}
			res.status(200).send(out);
		}
	})
};


/** *********************************************** */

module.exports.route = router;