var express = require("express");
var router = express.Router();
var multer = require('multer');
var docSvc = require("../services/document.js");
var fs = require('fs');
var mime = require('mime');
var env = require('../environment.json');

var jwt = require('jsonwebtoken');

if (!env.docs.path.endsWith("/")) {
    env.docs.path = env.docs.path + "/";
}

router.use(function (req, res, next) {

    var token = req.header("app-auth-token");

    validateAuthToken(token, function (result, data) {
        if (result === true) {
            req.uinfo = data.uinfo;
            next();
        } else {
            res.sendStatus(403);
        }
    });
});

var validateAuthToken = function (token, callback) {
    jwt.verify(token, env.api.secret, function (err, decoded) {
        if (err) {
            callback(false);
        } else {
            callback(true, decoded);
        }
    });
};
router.use(function (req, res, next) {
    next();
});

router.get("/types/:utype", function (req, res, next) {

    getDocTypes(req, res);

});

router.get("/list/:user_uid", function (req, res, next) {
    getDocList(req, res);
});




router.post("/storeImagePath",function (req, res, next) {
   var docItem = req.body;
   docItem.user_uid = req.uinfo.uid;
   docSvc.insertCloudDocument(docItem,function(err,result){
       if(err){
           var errMsg = {
               "error":"Upload Failed"
           }
            res.status(500).json(errMsg);
       }else{
           res.status(200).json(result);
       }

   });
   
});


router.post("/insert", function (req, res, next) {

    if (!req.body || !req.body.data) {
        res.status(401).send("Invalid Call");
        return;
    }

    var docItem = req.body.data
    docInsert(docItem, function (err, result) {
        var output = {
            error: err,
            result: result
        };
        if (err) {
            res.status(500).json(output);
        } else {
            res.status(200).json(output);
        }
    });
});

router.post("/update/:uid", function (req, res, next) {
    var uid = req.params.uid;
    var docItem = JSON.parse(JSON.stringify(req.body.data));

    docUpdate(docItem, uid, function (err, result) {
        var output = {
            error: err,
            result: result
        };
        if (err) {
            res.status(500).json(output);
        } else {

            res.status(200).json(output);
        }
    });
});

router.post("/remove/:uid", function (req, res, next) {
    var uid = req.params.uid;
    docRemove(uid, function (err, result) {
        var output = {
            error: err,
            result: result
        };
        if (err) {
            res.status(500).json(output);
        } else {
            res.status(200).json(output);
        }
    });
});

router.post('/upload', function (req, res) {
    docUpload(req, res);
});
router.get('/download/:user_uid/:filename', function (req, res) {
    var user_uid = req.params.user_uid;
    var filename = req.params.filename;

    var docPath = env.docs.path + user_uid + "/" + filename;
    var mimetype = mime.lookup(docPath);

    res.setHeader('Content-disposition', 'attachment; filename=' + filename);
    res.setHeader('Content-type', mimetype);

    var filestream = fs.createReadStream(docPath);
    filestream.pipe(res);

});

// *************************************************************************

var getDocTypes = function (req, res) {
    var options = null;
    var utype = req.params.utype.toLowerCase();

    var docTypeCode = '';

    if (utype == 'sp') {
        docTypeCode = '_sp_docs';
    } else if (utype = 'lg') {
        docTypeCode = '_lg_docs';
    } else {
        res.status(500).json("Invalid argument utype");
    }

    if (req.query.options) {
        options = JSON.parse(req.query.options);
    }

    if (!options) {
        options = {
            limit: 999,
            offset: 0
        }
    }

    docSvc.getDocumentTypes(docTypeCode, options, function (err, results,
            rowCount) {
        var output = {
            error: err,
            list: results,
            rowCount: rowCount
        };
        res.status(200).json(output);
    });
};

var getDocList = function (req, res) {
    var options = null;
    var user_uid = req.params.user_uid;

    docSvc.getDocumentList(user_uid, options, function (err, results, rowCount) {
        var output = {
            error: err,
            list: results,
            rowCount: rowCount
        };
        res.status(200).json(output);
    });
};

var docInsert = function (docItem, cb) {
    // console.log(docItem);
    docSvc.insertDocument(docItem, function (err, result) {
        return cb(err, result);
    });
};

var docUpdate = function (docItem, uid, cb) {

    if (uid != docItem.uid) {
        return cb({
            "error": "UID mismatch"
        }, null);
    }

    docSvc.updateDocument(docItem, function (err, result) {
        return cb(err, result);
    });
};

var docRemove = function (uid, cb) {
    docSvc.removeDocument(uid, function (err, result) {
        return cb(err, result);
    });
};

var docUpload = function (req, res) {
    upload(req, res, function (err) {

        var output = {
            error: err
        };
        if (err) {
            console.log(err);
            res.status(500).json(output);
        } else {
            if (req.body.doc) {
                var doc = JSON.parse(JSON.stringify(req.body.doc));

                var docPath = env.docs.path + doc.user_uid + "/"
                        + doc.doc_path;
                var tmpDocPath = docPath + ".tmp";

                if (doc.uid) {
                    // UPDATE
                    docUpdate(doc, doc.uid, function (err, result) {
                        if (err) {
                            console.log(err);
                        }
                        var output = {
                            error: err,
                            result: result
                        };
                        if (err) {
                            fs.unlinkSync(tmpDocPath);
                            res.status(500).json(output);
                        } else {
                            fs.renameSync(tmpDocPath, docPath);
                            res.status(200).json(output);
                        }
                    });
                } else {
                    // INSERT
                    docInsert(doc, function (err, result) {
                        if (err) {
                            console.log(err);
                        }
                        var output = {
                            error: err,
                            result: result
                        };
                        if (err) {
                            fs.unlinkSync(tmpDocPath);
                            res.status(500).json(output);
                        } else {
                            fs.renameSync(tmpDocPath, docPath);
                            res.status(200).json(output);
                        }
                    });
                }
            } else {

                res.status(500).json(output);
            }

        }
    });
}

// ***************** FILE UPLOADER ************************************
var storage = multer.diskStorage({// multers disk storage settings
    destination: function (req, file, cb) {
        var doc = req.body.doc;

        var maindir = env.docs.path;//'./upload-docs/';
        if (!fs.existsSync(maindir)) {
            fs.mkdirSync(maindir);
        }

        var childdir = maindir + doc.user_uid;
        if (!fs.existsSync(childdir)) {
            fs.mkdirSync(childdir);
        }

        cb(null, childdir);
    },
    filename: function (req, file, cb) {

        var doc = req.body.doc;

        cb(null, doc.doc_path + ".tmp");
    }
});
var upload = multer({// multer settings
    storage: storage
}).single('file');

// *********************************************************************

module.exports.route = router;
