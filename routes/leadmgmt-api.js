var express = require("express");
var router = express.Router();

var lmSvc = require("../services/leadmanagement.js");
var jwt = require('jsonwebtoken');
const env = require('../environment');


router.use(function (req, res, next) {
    
    var token = req.header("app-auth-token");
    
    validateAuthToken(token, function (result, data) {
        if (result === true) {
            req.uinfo = data.uinfo;
            next();
        } else {
            res.sendStatus(403);
        }
    });
});

var validateAuthToken = function (token, callback) {
    jwt.verify(token, env.api.secret, function (err, decoded) {
        if (err) {
            callback(false);
        } else {
            callback(true, decoded);
        }
    });
};

router.get("/list/:utype/:leadtype", function(req, res, next) {

	var filter = req.query.filter;
	var options = req.query.options;
	var searchFilter = req.query.search;

	var utype = req.params.utype.toLowerCase();
	var leadtype = req.params.leadtype;

	if (filter) {
		filter = JSON.parse(filter)
	} else {
		filter = {};
	}

	if (options) {
		options = JSON.parse(options);
	} else {
		options = {
			limit : 999,
			offset : 0
		}
	}

	if (searchFilter) {
		searchFilter = JSON.parse(searchFilter);
	}

	lmSvc.getLeads(utype, leadtype, filter, searchFilter, options,
			function(err, results, rowCount) {
				var output = {
					error : err,
					list : results,
					rowCount : rowCount
				};
				console.log(results);
				res.status(200).json(output);
			});
});

router.get("/details/:uid", function(req, res, next) {

	var uid = req.params.uid;

	if (!uid) {
		res.status(401).send("Unauthorized Call");
		return;
	}
	lmSvc.getLeadDetails(uid, function(err, results) {
		var output = {
			error : err,
			details : (results.length === 1) ? results[0] : {}
		};
		res.status(200).json(output);
	});
});

router.post("/details/insert", function(req, res, next) {

	if (!req.body || !req.body.data) {
		res.status(401).send("Invalid Call");
		return;
	}

	var leadinfo = req.body.data

	lmSvc.insertLead(leadinfo, function(err, result) {
		if (err) {
			console.log(err);
		}

		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});

router.post("/details/update", function(req, res, next) {

	if (!req.body || !req.body.data) {
		res.status(401).send("Invalid Call");
		return;
	}

	var leadinfo = req.body.data

	if (!leadinfo.uid) {
		res.status(401).send("Invalid Call Data");
		return;
	}

	lmSvc.updateLead(leadinfo, function(err, result) {
		if (err) {
			console.log(err);

			var output = {
				error : err,
				result : result
			};
			res.status(200).json(output);

		} else {
			var output = {
				error : err,
				result : result
			};
			res.status(200).json(output);
		}

	});
});

router.get("/checkclosure/:leaduid/:source", function(req, res, next) {

	var leaduid = req.params.leaduid;
	var source = req.params.source;

	lmSvc.checkForLeadClosure(leaduid, source, function(err, result) {
		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});

router.get("/spleaddetails/:uid", function(req, res, next) {

	var uid = req.params.uid;

	if (!uid) {
		res.status(401).send("Unauthorized Call");
		return;
	}
	lmSvc.getSPLeadDetails(uid, function(err, results) {
		var output = {
			error : err,
			details : (results.length === 1) ? results[0] : {}
		};
		res.status(200).json(output);
	});
});
router.post("/splist/insert", function(req, res, next) {

	if (!req.body || !req.body.data) {
		res.status(401).send("Invalid Call");
		return;
	}

	var leadspinfo = req.body.data

	lmSvc.insertLeadServiceProvider(leadspinfo, function(err, result) {
		if (err) {
			console.log(err);
		}

		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});

router.post("/splist/update", function(req, res, next) {

	if (!req.body || !req.body.data) {
		res.status(401).send("Invalid Call");
		return;
	}

	var leadspinfo = req.body.data

	if (!leadspinfo.uid) {
		res.status(401).send("Invalid Call Data");
		return;
	}

	lmSvc.updateLeadServiceProvider(leadspinfo, function(err, result) {
		if (err) {
			console.log(err);
		}
		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});

router.get("/splist/:leaduid", function(req, res, next) {

	var leaduid = req.params.leaduid;

	if (!leaduid) {
		res.status(401).send("Unauthorized Call");
		return;
	}
	lmSvc.getLeadServiceProviders(leaduid, null, function(err, results) {
		var output = {
			error : err,
			details : results
		};
		res.status(200).json(output);
	});
});

router.get("/splist/:leaduid/:uid", function(req, res, next) {

	var leaduid = req.params.leaduid;
	var uid = req.params.uid;

	if (!uid) {
		res.status(401).send("Unauthorized Call");
		return;
	}
	lmSvc.getLeadServiceProviders(leaduid, uid, function(err, results) {
		var output = {
			error : err,
			details : results
		};
		res.status(200).json(output);
	});
});

module.exports.route = router;