var express = require("express");
var router = express.Router();

var mlSvc = require("../services/masterlist.js");

var jwt = require('jsonwebtoken');
const env = require('../environment');


router.use(function (req, res, next) {
    
    var token = req.header("app-auth-token");
    
    validateAuthToken(token, function (result, data) {
        if (result === true) {
            req.uinfo = data.uinfo;
            next();
        } else {
            res.sendStatus(403);
        }
    });
});

var validateAuthToken = function (token, callback) {
    jwt.verify(token, env.api.secret, function (err, decoded) {
        if (err) {
            callback(false);
        } else {
            callback(true, decoded);
        }
    });
};

router.get("/", function(req, res, next) {

	getCountries(function(response) {
		res.status(200).json(response);
	});

});

router.get("/alllocations", function(req, res, next) {

	mlSvc.getCustomlists("get_locationmaster", function(err, results) {
		
		//Need to change beacuse of android, ios
		if(req.header("isFromMobile")){
			results.splice(results.length-1,1);
		}

		var output = {
			error : err,
			details : results
		};

		res.status(200).json(output);
	});
});

router.get("/states/:country", function(req, res, next) {

	var countryuid = req.params.country;

	getInnerList(countryuid, function(response) {
		res.status(200).json(response);
	});

});


router.get("/districts/:state", function(req, res, next) {

	var stateuid = req.params.state;

	getInnerList(stateuid, function(response) {
		res.status(200).json(response);
	});
});

router.get("/cities/:distt", function(req, res, next) {

	var disttuid = req.params.distt;

	getInnerList(disttuid, function(response) {
		res.status(200).json(response);
	});
});

router.get("/citieslist/:state", function(req, res, next) {
//need to modify it
	var disttuid = "a945da20-1042-11e7-ac0c-0254001bc12d";

	getInnerList(disttuid, function(response) {
		res.status(200).json(response);
	});
});

router.get("/citieslist2/:state", function(req, res, next) {

	var stateuid = req.params.state;

	var citylist=[""];

	getInnerList(stateuid, function(response) {

		response.list.forEach(function(list) {
			console.log(list.uid);
			getInnerList(list.uid, function(data) {
			citylist.push(data);
			//	citylist = citylist.concat(data);	
			});


		  });

		res.status(200).json(citylist);
	});
});

router.get("/details/:uid", function(req, res, next) {

	var uid = req.params.uid;

	if (!uid) {
		res.status(401).send("Unauthorized Call");
		return;
	}

	mlSvc.getDetailsByUid(uid, function(err, results) {
		var output = {
			error : err,
			details : (results.length === 1) ? results[0] : {}
		};
		res.status(200).json(output);
	});
});

router.post("/details/insert", function(req, res, next) {

	if (!req.body || !req.body.mlinfo) {
		res.status(401).send("Invalid Call");
		return;
	}

	var mlInfo = req.body.mlinfo;
	
//	console.log(mlInfo);

	mlSvc.insertList(mlInfo, function(err, result) {

		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});

router.post("/details/update", function(req, res, next) {

	if (!req.body || !req.body.mlinfo) {
		res.status(401).send("Invalid Call");
		return;
	}

	var mlInfo = req.body.mlinfo

	if (!mlInfo.uid) {
		res.status(401).send("Invalid Call Data");
		return;
	}

	mlSvc.updateList(mlInfo, function(err, result) {
		var output = {
			error : err,
			result : result
		};
		res.status(200).json(output);
	});
});

var getCountries = function(callback) {
	var filter = {
		code : '_geo_locations',
		parent_uid : null
	};

	mlSvc.getDetailsByCode(filter.code, filter.parent_uid, function(err,
			results, rowCount) {
		var locationDetails = (results && results.length === 1)
				? results[0]
				: {}

		if (locationDetails.uid) {
			var locationFilter = {
				parent_uid : locationDetails.uid
			};

			mlSvc.getMasterlists(locationFilter, function(err, results,
					rowCount) {
				var output = {
					error : err,
					list : results,
					rowCount : rowCount || 0
				};

				return callback(output);
			});
		} else {
			var output = {
				error : err,
				list : [],
				rowCount : 0
			};
			return callback(output);
		}
	});
};

var getInnerList = function(locationuid, callback) {
	var locationFilter = {
		parent_uid : locationuid
	};

	mlSvc.getMasterlists(locationFilter, function(err, results, rowCount) {
		var output = {
			error : err,
			list : results,
			rowCount : rowCount || 0
		};
		
		return callback(output);
		
	});
};
module.exports.route = router;