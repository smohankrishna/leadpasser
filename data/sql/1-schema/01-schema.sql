--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `uid` varchar(40) NOT NULL,
  `name` varchar(100) NOT NULL,
  `organization` varchar(100) DEFAULT NULL,
  `utype` char(2) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `upass` varchar(4000) NOT NULL,
  `temp_pwd` varchar(100) DEFAULT NULL,
  `resetpwd` tinyint(1) NOT NULL DEFAULT '1',
  `email` varchar(200) DEFAULT NULL,
  `emailverified` tinyint(1) NOT NULL DEFAULT '0',
  `ecode` varchar(45) DEFAULT NULL,
  `ecodevalidity` datetime DEFAULT NULL,
  `mobilenum` varchar(15) NOT NULL,
  `mobileverified` tinyint(1) NOT NULL DEFAULT '0',
  `mcode` varchar(45) DEFAULT NULL,
  `mcodevalidity` datetime DEFAULT NULL,
  `comments` varchar(4000) DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `pincode` varchar(10) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `name2` varchar(100) DEFAULT NULL,
  `email2` varchar(200) DEFAULT NULL,
  `mobilenum2` varchar(15) DEFAULT NULL,
  `enable_display` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `createdby` varchar(50) NOT NULL DEFAULT 'System',
  `createdon` datetime DEFAULT NULL,
  `modifiedby` varchar(50) NOT NULL DEFAULT 'System',
  `modifiedon` datetime DEFAULT NULL,
  `role` varchar(45) NOT NULL DEFAULT 'user',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uname_UNIQUE` (`uname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `uid` varchar(40) NOT NULL,
  `user_uid` varchar(40) NOT NULL,
  `doc_type_uid` varchar(40) NOT NULL,
  `doc_title` varchar(50) NOT NULL,
  `doc_path` varchar(1000) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `createdby` varchar(50) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedby` varchar(50) NOT NULL,
  `modifiedon` datetime NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `UQ_USER_TITLE` (`user_uid`,`doc_title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `leads`;

CREATE TABLE `leads` (
  `uid` varchar(40) NOT NULL,
  `user_uid` varchar(40) NOT NULL,
  `client_name` varchar(200) NOT NULL,
  `client_org` varchar(100) DEFAULT NULL,
  `client_phone` varchar(15) NOT NULL,
  `client_email` varchar(200) DEFAULT NULL,
  `state_uid` varchar(40) DEFAULT NULL,
  `city_uid` varchar(40) DEFAULT NULL,
  `other_state` varchar(50) DEFAULT NULL,
  `other_city` varchar(50) DEFAULT NULL,
  `category_uid` varchar(40) DEFAULT NULL,
  `subcategory_uid` varchar(40) DEFAULT NULL,
  `timeframe_uid` varchar(40) DEFAULT NULL,
  `ticket_value` decimal(15,2) DEFAULT NULL,
  `otherinfo` varchar(4000) DEFAULT NULL,
  `assigned_to_uid` varchar(40) DEFAULT NULL,
  `status` varchar(40) NOT NULL DEFAULT 'New',
  `comments_verification` varchar(200) DEFAULT NULL,
  `comments_final` varchar(200) DEFAULT NULL,
  `apr_ticket_value` decimal(15,2) DEFAULT NULL,
  `createdby` varchar(50) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedby` varchar(50) NOT NULL,
  `modifiedon` datetime NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `FK_USERUID_idx` (`user_uid`),
  CONSTRAINT `FK_USERUID` FOREIGN KEY (`user_uid`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `lg_bank_details`
--

DROP TABLE IF EXISTS `lg_bank_details`;

CREATE TABLE `lg_bank_details` (
  `uid` varchar(40) NOT NULL,
  `user_uid` varchar(40) NOT NULL,
  `owner_name` varchar(200) NOT NULL,
  `pan_number` varchar(20) DEFAULT NULL,
  `account_number` varchar(45) NOT NULL,
  `account_type` varchar(45) DEFAULT NULL,
  `bank` varchar(100) NOT NULL,
  `branch` varchar(200) DEFAULT NULL,
  `ifsc_code` varchar(20) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '0',
  `editable` tinyint(1) DEFAULT '1',
  `createdby` varchar(50) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` varchar(50) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `UQ_USER_UID` (`user_uid`),
  KEY `FK_USRER_UID_idx` (`user_uid`),
  CONSTRAINT `FK_USRER_UID` FOREIGN KEY (`user_uid`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `masterlist`
--

DROP TABLE IF EXISTS `masterlist`;

CREATE TABLE `masterlist` (
  `uid` varchar(40) NOT NULL,
  `code` varchar(30) NOT NULL,
  `description` varchar(100) NOT NULL,
  `parent_uid` varchar(40) DEFAULT NULL,
  `seq` int(11) NOT NULL DEFAULT '999',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `createdby` varchar(50) NOT NULL DEFAULT 'system',
  `createdon` datetime NOT NULL,
  `modifiedby` varchar(50) NOT NULL DEFAULT 'system',
  `modifiedon` datetime NOT NULL,
  `numfield_1` decimal(12,4) DEFAULT NULL,
  `numfield_2` decimal(12,4) DEFAULT NULL,
  `txtfield_1` varchar(100) DEFAULT NULL,
  `txtfield_2` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `UQ_CODE_PARENTUID` (`parent_uid`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `sp_catalog`
--

DROP TABLE IF EXISTS `sp_catalog`;

CREATE TABLE `sp_catalog` (
  `uid` varchar(45) NOT NULL,
  `sp_uid` varchar(40) NOT NULL,
  `category_uid` varchar(40) NOT NULL,
  `subcategory_uid` varchar(40) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `createdby` varchar(50) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` varchar(50) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `UQ_SPUID_CATEGORY_SUBCATEGORY` (`sp_uid`,`category_uid`,`subcategory_uid`),
  KEY `FK_CATEGORY_idx` (`category_uid`),
  KEY `FK_SUBCATEGORY_idx` (`subcategory_uid`),
  CONSTRAINT `FK_CATEGORY` FOREIGN KEY (`category_uid`) REFERENCES `masterlist` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_SPUID` FOREIGN KEY (`sp_uid`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_SUBCATEGORY` FOREIGN KEY (`subcategory_uid`) REFERENCES `masterlist` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `sp_catalog_locations`
--

DROP TABLE IF EXISTS `sp_catalog_locations`;

CREATE TABLE `sp_catalog_locations` (
  `uid` varchar(40) NOT NULL,
  `sp_catalog_uid` varchar(40) NOT NULL,
  `country_uid` varchar(40) NOT NULL,
  `state_uid` varchar(40) NOT NULL DEFAULT '',
  `district_uid` varchar(40) NOT NULL DEFAULT '',
  `city_uid` varchar(40) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `createdby` varchar(50) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` varchar(50) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `UQ_COUNTRY_STATE_DIST_CITY` (`sp_catalog_uid`,`country_uid`,`state_uid`,`district_uid`,`city_uid`) USING BTREE,
  CONSTRAINT `FK_SPCATALOG_UID` FOREIGN KEY (`sp_catalog_uid`) REFERENCES `sp_catalog` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `sp_leads`
--

DROP TABLE IF EXISTS `sp_leads`;

CREATE TABLE `sp_leads` (
  `uid` varchar(40) NOT NULL,
  `sp_uid` varchar(40) NOT NULL,
  `lead_uid` varchar(40) NOT NULL,
  `shared_on` datetime DEFAULT NULL,
  `viewed_on` datetime DEFAULT NULL,
  `acceptance_status` varchar(50) DEFAULT NULL,
  `accepted_on` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT 'New',
  `comments` varchar(4000) DEFAULT NULL,
  `apr_ticket_value` decimal(15,2) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `createdby` varchar(45) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` varchar(45) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `UQ_SPUID_LEADUID` (`sp_uid`,`lead_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;

CREATE TABLE `user_details` (
  `uid` varchar(40) NOT NULL,
  `user_uid` varchar(40) NOT NULL,
  `info_key` varchar(30) NOT NULL,
  `info_value` varchar(1000) DEFAULT NULL,
  `createdby` varchar(50) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedby` varchar(50) NOT NULL,
  `modifiedon` datetime NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `UNQ_UID_KEY` (`user_uid`,`info_key`),
  CONSTRAINT `FK_USERS` FOREIGN KEY (`user_uid`) REFERENCES `users` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

