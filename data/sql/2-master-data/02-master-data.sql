LOCK TABLES `masterlist` WRITE;

ALTER TABLE `masterlist` DISABLE KEYS;

INSERT INTO `masterlist` VALUES 
('1d861154-5f05-11e6-a3e1-025400b812aa','passport','Passport','eb7530d7-18ed-11e6-a7d5-02b453d9ac26',15,1,'sysadmin','2016-08-10 19:47:05','sysadmin','2016-08-10 19:47:05',NULL,NULL,NULL,NULL),
('2ec02a37-5ee4-11e6-a3e1-025400b812aa','loans','Loans','ebdda41f-18ed-11e6-a7d5-02b453d9ac26',10,1,'sysadmin','2016-08-10 15:51:21','sysadmin','2016-08-10 15:51:21',NULL,NULL,'Fixed','Fixed'),
('3275bb10-5f04-11e6-a3e1-025400b812aa','bike','Bike','2ec02a37-5ee4-11e6-a3e1-025400b812aa',20,1,'sysadmin','2016-08-10 19:40:31','sysadmin','2016-08-10 19:40:31',400.0000,800.0000,'Fixed','Fixed'),
('3760ed79-5f05-11e6-a3e1-025400b812aa','la','Lease Agreement','fedf6f37-48ab-11e6-8a1c-02b453d9ac26',5,1,'sysadmin','2016-08-10 19:47:48','sysadmin','2016-08-10 19:47:48',NULL,NULL,NULL,NULL),
('3b68d84b-1b30-11e6-a7d5-02b453d9ac26','in','India','ebbd6164-18ed-11e6-a7d5-02b453d9ac26',999,1,'superuser','2016-05-16 12:04:25','superuser','2016-05-16 12:04:25',NULL,NULL,NULL,NULL),
('3cb246ca-4d83-11e6-8a1c-02b453d9ac26','_timeframe','Timeframe',NULL,999,1,'superuser','2016-07-19 13:04:33','superuser','2016-07-19 13:04:33',NULL,NULL,NULL,NULL),
('3d0ce807-5f05-11e6-a3e1-025400b812aa','br','Business Registration','fedf6f37-48ab-11e6-8a1c-02b453d9ac26',10,1,'sysadmin','2016-08-10 19:47:58','sysadmin','2016-08-10 19:47:58',NULL,NULL,NULL,NULL),
('460337a9-5f05-11e6-a3e1-025400b812aa','pan','PAN Card','fedf6f37-48ab-11e6-8a1c-02b453d9ac26',15,1,'sysadmin','2016-08-10 19:48:13','sysadmin','2016-08-10 19:48:13',NULL,NULL,NULL,NULL),
('471d3060-5ee4-11e6-a3e1-025400b812aa','home','Home','2ec02a37-5ee4-11e6-a3e1-025400b812aa',5,1,'sysadmin','2016-08-10 15:52:02','sysadmin','2016-08-10 15:52:02',0.0100,0.0200,'Percentage','Percentage'),
('477565f7-5f04-11e6-a3e1-025400b812aa','car','Car','2ec02a37-5ee4-11e6-a3e1-025400b812aa',40,1,'sysadmin','2016-08-10 19:41:06','sysadmin','2016-08-10 19:41:06',0.1000,0.3000,'Percentage','Percentage'),
('567b2fe0-5f04-11e6-a3e1-025400b812aa','personal','Personal','2ec02a37-5ee4-11e6-a3e1-025400b812aa',50,1,'sysadmin','2016-08-10 19:41:31','sysadmin','2016-08-10 19:41:31',0.1000,0.3000,'Percentage','Percentage'),
('5dcb3222-5f04-11e6-a3e1-025400b812aa','insurance','Insurance','ebdda41f-18ed-11e6-a7d5-02b453d9ac26',20,1,'sysadmin','2016-08-10 19:41:43','user','2016-08-14 11:24:12',NULL,NULL,'Fixed','Fixed'),
('6e143f3b-5f04-11e6-a3e1-025400b812aa','life','Life','5dcb3222-5f04-11e6-a3e1-025400b812aa',10,1,'sysadmin','2016-08-10 19:42:11','sysadmin','2016-08-10 19:42:11',0.0100,0.0300,'Percentage','Percentage'),
('7d8c8040-5f04-11e6-a3e1-025400b812aa','medical','Medical','5dcb3222-5f04-11e6-a3e1-025400b812aa',20,1,'sysadmin','2016-08-10 19:42:37','sysadmin','2016-08-10 19:42:37',0.2000,0.4000,'Percentage','Percentage'),
('81a6ff70-5ee2-11e6-a3e1-025400b812aa','immediately','Immediately','3cb246ca-4d83-11e6-8a1c-02b453d9ac26',5,1,'sysadmin','2016-08-10 15:39:21','sysadmin','2016-08-10 15:39:21',NULL,NULL,NULL,NULL),
('8d69ff7a-5f04-11e6-a3e1-025400b812aa','pan','PAN Card','eb7530d7-18ed-11e6-a7d5-02b453d9ac26',5,1,'sysadmin','2016-08-10 19:43:03','sysadmin','2016-08-10 19:43:03',NULL,NULL,NULL,NULL),
('8fbfdc42-5ee2-11e6-a3e1-025400b812aa','1mo','Within 1 Month','3cb246ca-4d83-11e6-8a1c-02b453d9ac26',10,1,'sysadmin','2016-08-10 15:39:44','sysadmin','2016-08-10 15:39:44',NULL,NULL,NULL,NULL),
('94d6aea7-5f04-11e6-a3e1-025400b812aa','dl','Driving Licence','eb7530d7-18ed-11e6-a7d5-02b453d9ac26',10,1,'sysadmin','2016-08-10 19:43:16','sysadmin','2016-08-10 19:43:16',NULL,NULL,NULL,NULL),
('9bab047a-5ee2-11e6-a3e1-025400b812aa','1to3mo','1 to 3 Months','3cb246ca-4d83-11e6-8a1c-02b453d9ac26',15,1,'sysadmin','2016-08-10 15:40:04','sysadmin','2016-08-10 15:40:04',NULL,NULL,NULL,NULL),
('9ecb9c30-1042-11e7-ac0c-0254001bc12d','ts','Telangana','3b68d84b-1b30-11e6-a7d5-02b453d9ac26',999,1,'sysadmin','2017-03-24 09:03:17','sysadmin','2017-03-24 09:03:17',NULL,NULL,NULL,NULL),
('a945da20-1042-11e7-ac0c-0254001bc12d','hyd','Hyderabad','9ecb9c30-1042-11e7-ac0c-0254001bc12d',999,1,'sysadmin','2017-03-24 09:03:35','sysadmin','2017-03-24 09:03:35',NULL,NULL,NULL,NULL),
('ab52bf92-5ee2-11e6-a3e1-025400b812aa','3to6mo','3 to 6 Months','3cb246ca-4d83-11e6-8a1c-02b453d9ac26',20,1,'sysadmin','2016-08-10 15:40:31','sysadmin','2016-08-10 15:40:46',NULL,NULL,NULL,NULL),
('b8b161d6-1042-11e7-ac0c-0254001bc12d','hyderabad','Hyderabad','a945da20-1042-11e7-ac0c-0254001bc12d',999,1,'sysadmin','2017-03-24 09:04:01','sysadmin','2017-03-24 09:04:01',NULL,NULL,NULL,NULL),
('da01f6f8-5ee2-11e6-a3e1-025400b812aa','later','Beyond 6 months','3cb246ca-4d83-11e6-8a1c-02b453d9ac26',999,1,'sysadmin','2016-08-10 15:41:49','sysadmin','2016-08-10 15:41:49',NULL,NULL,NULL,NULL),
('eb03fad8-18ed-11e6-a7d5-02b453d9ac26','_lead_status','Lead Status',NULL,999,1,'system','2016-05-13 09:34:41','superuser','2016-07-13 09:14:41',NULL,NULL,NULL,NULL),
('eb1ad8cd-18ed-11e6-a7d5-02b453d9ac26','_lead_status_reason','Lead Status Reason',NULL,999,1,'system','2016-05-13 09:34:41','superuser','2016-07-13 09:14:48',NULL,NULL,NULL,NULL),
('eb7530d7-18ed-11e6-a7d5-02b453d9ac26','_lg_docs','LG Documents',NULL,999,1,'system','2016-05-13 09:34:41','superuser','2016-07-13 09:14:27',NULL,NULL,NULL,NULL),
('ebbd6164-18ed-11e6-a7d5-02b453d9ac26','_geo_locations','Geographical Locations',NULL,999,1,'system','2016-05-13 09:34:42','superuser','2016-05-16 11:27:28',NULL,NULL,NULL,NULL),
('ebdda41f-18ed-11e6-a7d5-02b453d9ac26','_product_catalog','Product Catalog',NULL,999,1,'system','2016-05-13 09:34:42','system','2016-05-13 09:34:42',NULL,NULL,NULL,NULL),
('fedf6f37-48ab-11e6-8a1c-02b453d9ac26','_sp_docs','SP Documents',NULL,999,1,'superuser','2016-07-13 09:13:43','superuser','2016-07-13 09:13:43',NULL,NULL,NULL,NULL);

ALTER TABLE `masterlist` ENABLE KEYS;

UNLOCK TABLES;


LOCK TABLES `users` WRITE;

ALTER TABLE `users` DISABLE KEYS;

INSERT INTO `users` VALUES 
('eab436c6-18ed-11e6-a7d5-02b453d9ac26','System Admin',NULL,'su','sysadmin','5f4dcc3b5aa765d61d8327deb882cf99',NULL,0,'manish@shanrohi.com',1,NULL,NULL,'9989925151',0,'053783','2016-06-30 15:30:27',NULL,NULL,NULL,'Hyderabad','Telangana',NULL,'1998-05-05','Male',NULL,NULL,NULL,0,1,'system','2016-05-13 09:34:40','sysadmin','2016-09-26 23:38:08','admin'),
('eace7aee-18ed-11e6-a7d5-02b453d9ac26','Super User',NULL,'de','superuser','5f4dcc3b5aa765d61d8327deb882cf99','password',0,'manish.gupta@shanrohi.com',1,NULL,NULL,'9160402299',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,'system','2016-05-13 09:34:40','system','2016-05-13 09:34:40','user');

ALTER TABLE `users` ENABLE KEYS;

UNLOCK TABLES;
