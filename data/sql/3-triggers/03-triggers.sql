DELIMITER $$
CREATE TRIGGER `documents_BEFORE_INSERT` BEFORE INSERT ON `documents`
 FOR EACH ROW 
 BEGIN
	SET new.uid = uuid();
    SET new.createdon = NOW();
    SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$
CREATE TRIGGER `documents_BEFORE_UPDATE` BEFORE UPDATE ON `documents` FOR EACH ROW 
 BEGIN
	SET new.modifiedon = NOW();
END ;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `leads_BEFORE_INSERT` BEFORE INSERT ON `leads` FOR EACH ROW 
 BEGIN
	SET new.uid = uuid();
    SET new.createdon = NOW();
    SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$
CREATE TRIGGER `leads_BEFORE_UPDATE` BEFORE UPDATE ON `leads` FOR EACH ROW 
 BEGIN
	SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `lg_bank_details_BEFORE_INSERT` BEFORE INSERT ON `lg_bank_details` FOR EACH ROW
BEGIN
	SET new.uid = uuid();
    SET new.createdon = NOW();
    SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `lg_bank_details_BEFORE_UPDATE` BEFORE UPDATE ON `lg_bank_details` FOR EACH ROW
BEGIN
    SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `masterlist_BEFORE_INSERT` BEFORE INSERT ON `masterlist` FOR EACH ROW 
BEGIN
	SET new.uid = uuid();
    SET new.createdon = NOW();
    SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$


CREATE TRIGGER `masterlist_BEFORE_UPDATE` BEFORE UPDATE ON `masterlist` FOR EACH ROW 
BEGIN
	SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$
CREATE TRIGGER `sp_catalog_BEFORE_INSERT` BEFORE INSERT ON `sp_catalog` FOR EACH ROW 
BEGIN
	SET new.uid = uuid();
    SET new.createdon = NOW();
    SET new.modifiedon = NOW();
END ;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `sp_catalog_BEFORE_UPDATE` BEFORE UPDATE ON `sp_catalog` FOR EACH ROW 
BEGIN
	SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `sp_catalog_locations_BEFORE_INSERT` BEFORE INSERT ON `sp_catalog_locations` FOR EACH ROW 
BEGIN
	SET new.uid = uuid();
    SET new.createdon = NOW();
    SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `sp_catalog_locations_BEFORE_UPDATE` BEFORE UPDATE ON `sp_catalog_locations` FOR EACH ROW 
BEGIN
	SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `sp_leads_BEFORE_INSERT` BEFORE INSERT ON `sp_leads` FOR EACH ROW 
BEGIN

    SET new.uid = uuid();
    SET new.createdon = NOW();
    SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `sp_leads_AFTER_INSERT` AFTER INSERT ON `sp_leads` FOR EACH ROW 
BEGIN
	
    IF ((select  count(*) from sp_leads where lead_uid = new.lead_uid and acceptance_status is not null) > 0) THEN
		update leads
		set status = 'In Progress'
		where uid = new.lead_uid;
    ELSE
		update leads
		set status = 'Verified'
		where uid = new.lead_uid;
    END IF;
    
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `sp_leads_BEFORE_UPDATE` BEFORE UPDATE ON `sp_leads` FOR EACH ROW 
BEGIN
	SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `sp_leads_AFTER_UPDATE` AFTER UPDATE ON `sp_leads` FOR EACH ROW 
BEGIN
	
    IF ((select  count(*) from sp_leads where lead_uid = new.lead_uid and acceptance_status is not null) > 0) THEN
		update leads
		set status = 'In Progress'
		where uid = new.lead_uid;
    ELSE
		update leads
		set status = 'Verified'
		where uid = new.lead_uid;
    END IF;
    
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `user_details_BEFORE_INSERT` BEFORE INSERT ON `user_details` FOR EACH ROW 
BEGIN
	SET new.uid = uuid();
    SET new.createdon = NOW();
    SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `user_details_BEFORE_UPDATE` BEFORE UPDATE ON `user_details` FOR EACH ROW 
BEGIN
	SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `users_BEFORE_INSERT` BEFORE INSERT ON `users` FOR EACH ROW 
BEGIN

	SET new.uid = uuid();
    SET new.createdon = NOW();
    SET new.modifiedon = NOW();
END;$$

DELIMITER $

DELIMITER $$

CREATE TRIGGER `users_BEFORE_UPDATE` BEFORE UPDATE ON `users` FOR EACH ROW 
BEGIN
	SET new.modifiedon = NOW();
END;$$

DELIMITER $
