DELIMITER $$

DROP VIEW IF EXISTS `vw_leads`;$$

DELIMITER $$

CREATE VIEW `vw_leads` AS
    SELECT 
        `l`.`uid` AS `uid`,
        `l`.`user_uid` AS `user_uid`,
        `lg`.`name` AS `leadpasser`,
        `l`.`client_org` AS `client_org`,
        `l`.`client_name` AS `client_name`,
        `l`.`client_phone` AS `client_phone`,
        `l`.`client_email` AS `client_email`,
        `l`.`state_uid` AS `state_uid`,
        `s`.`description` AS `state`,
        `l`.`city_uid` AS `city_uid`,
        `ct`.`description` AS `city`,
        `l`.`other_state` AS `other_state`,
        `l`.`other_city` AS `other_city`,
        `l`.`category_uid` AS `category_uid`,
        `cat`.`description` AS `category`,
        `l`.`subcategory_uid` AS `subcategory_uid`,
        `scat`.`description` AS `subcategory`,
        `l`.`timeframe_uid` AS `timeframe_uid`,
        `tf`.`description` AS `timeframe`,
        `l`.`ticket_value` AS `ticket_value`,
        `l`.`otherinfo` AS `otherinfo`,
        `l`.`assigned_to_uid` AS `assigned_to_uid`,
        `ag`.`name` AS `agent`,
        `l`.`status` AS `status`,
        `l`.`createdby` AS `createdby`,
        `l`.`createdon` AS `createdon`,
        `l`.`modifiedby` AS `modifiedby`,
        `l`.`modifiedon` AS `modifiedon`,
        `l`.`comments_verification` AS `comments_verification`,
        (CASE
            WHEN
                (`l`.`status` IN ('Terminated' , 'Completed',
                    'Withdrawn',
                    'Declined',
                    'Invalid Lead',
                    'Verification Failed',
                    'Rejected By All'))
            THEN
                (TO_DAYS(`l`.`modifiedon`) - TO_DAYS(`l`.`createdon`))
            ELSE (TO_DAYS(NOW()) - TO_DAYS(`l`.`createdon`))
        END) AS `age`,
        (CASE
            WHEN (`scat`.`txtfield_1` = 'Fixed') THEN `scat`.`numfield_1`
            WHEN (`scat`.`txtfield_1` = 'Percentage') THEN ((`scat`.`numfield_1` * `l`.`ticket_value`) / 100)
        END) AS `reward`,
        (CASE
            WHEN (`scat`.`txtfield_2` = 'Fixed') THEN `scat`.`numfield_2`
            WHEN (`scat`.`txtfield_2` = 'Percentage') THEN ((`scat`.`numfield_2` * `l`.`ticket_value`) / 100)
        END) AS `purchase_cost`,
        (CASE
            WHEN
                (`scat`.`txtfield_1` = 'Fixed')
            THEN
                CAST(IF(ISNULL(`l`.`apr_ticket_value`),
                        0,
                        `scat`.`numfield_1`)
                    AS DECIMAL (15 , 2 ))
            WHEN
                (`scat`.`txtfield_1` = 'Percentage')
            THEN
                CAST(IFNULL(((`scat`.`numfield_1` * `l`.`apr_ticket_value`) / 100),
                            0)
                    AS DECIMAL (15 , 2 ))
        END) AS `apr_reward`,
        (CASE
            WHEN
                (`scat`.`txtfield_2` = 'Fixed')
            THEN
                CAST(IF(ISNULL(`l`.`apr_ticket_value`),
                        0,
                        `scat`.`numfield_2`)
                    AS DECIMAL (15 , 2 ))
            WHEN
                (`scat`.`txtfield_2` = 'Percentage')
            THEN
                CAST(IFNULL(((`scat`.`numfield_2` * `l`.`apr_ticket_value`) / 100),
                            0)
                    AS DECIMAL (15 , 2 ))
        END) AS `apr_purchase_cost`,
        (CASE
            WHEN
                (`l`.`status` IN ('Terminated' , 'Completed',
                    'Withdrawn',
                    'Declined',
                    'Invalid Lead',
                    'Verification Failed',
                    'Rejected By All'))
            THEN
                'Past'
            ELSE 'Open'
        END) AS `leadtype`
    FROM
        (((((((`leads` `l`
        JOIN `users` `lg` ON ((`l`.`user_uid` = `lg`.`uid`)))
        LEFT JOIN `masterlist` `s` ON ((`l`.`state_uid` = `s`.`uid`)))
        LEFT JOIN `masterlist` `ct` ON ((`l`.`city_uid` = `ct`.`uid`)))
        JOIN `masterlist` `cat` ON ((`l`.`category_uid` = `cat`.`uid`)))
        JOIN `masterlist` `scat` ON ((`l`.`subcategory_uid` = `scat`.`uid`)))
        JOIN `masterlist` `tf` ON ((`l`.`timeframe_uid` = `tf`.`uid`)))
        LEFT JOIN `users` `ag` ON ((`l`.`assigned_to_uid` = `ag`.`uid`)));$$


DELIMITER $$

DROP VIEW IF EXISTS `vw_leadserviceproviders`;$$

DELIMITER $$

CREATE VIEW `vw_leadserviceproviders` AS
    SELECT 
        `l`.`uid` AS `lead_uid`,
        `spc`.`sp_uid` AS `sp_uid`,
        `u`.`organization` AS `organization`,
        IF((`spcl`.`state_uid` = ''),
            'All States',
            'Selected State') AS `state_presence`,
        (CASE
            WHEN
                (ISNULL(`spl`.`uid`)
                    OR (`spl`.`active` = 0))
            THEN
                0
            ELSE 1
        END) AS `sharing_status`,
        `spl`.`uid` AS `spl_uid`,
        `spl`.`shared_on` AS `shared_on`,
        `spl`.`acceptance_status` AS `acceptance_status`,
        `spl`.`accepted_on` AS `accepted_on`,
        `spl`.`status` AS `status`,
        `spl`.`apr_ticket_value` AS `apr_ticket_value`,
        `spl`.`active` AS `active`
    FROM
        ((((`leads` `l`
        JOIN `sp_catalog` `spc` ON (((`spc`.`category_uid` = `l`.`category_uid`)
            AND (`spc`.`subcategory_uid` = `l`.`subcategory_uid`)
            AND (`spc`.`active` = 1))))
        JOIN `users` `u` ON (((`u`.`uid` = `spc`.`sp_uid`)
            AND (`u`.`active` = 1))))
        LEFT JOIN `sp_catalog_locations` `spcl` ON ((`spcl`.`sp_catalog_uid` = `spc`.`uid`)))
        LEFT JOIN `sp_leads` `spl` ON ((`spl`.`sp_uid` = `spc`.`sp_uid`)))
    WHERE
        ((`spcl`.`uid` IS NOT NULL)
            AND ((`spcl`.`state_uid` = '')
            OR (`spcl`.`state_uid` = IFNULL(`l`.`state_uid`, `spcl`.`state_uid`))));$$

DELIMITER $$

DROP VIEW IF EXISTS `vw_openleads`;$$

DELIMITER $$

CREATE VIEW `vw_openleads` AS
    SELECT 
        `l`.`uid` AS `uid`,
        `l`.`user_uid` AS `user_uid`,
        `lg`.`name` AS `leadpasser`,
        `l`.`client_org` AS `client_org`,
        `l`.`client_name` AS `client_name`,
        `l`.`client_phone` AS `client_phone`,
        `l`.`client_email` AS `client_email`,
        `l`.`state_uid` AS `state_uid`,
        `s`.`description` AS `state`,
        `l`.`city_uid` AS `city_uid`,
        `ct`.`description` AS `city`,
        `l`.`other_state` AS `other_state`,
        `l`.`other_city` AS `other_city`,
        `l`.`category_uid` AS `category_uid`,
        `cat`.`description` AS `category`,
        `l`.`subcategory_uid` AS `subcategory_uid`,
        `scat`.`description` AS `subcategory`,
        `l`.`timeframe_uid` AS `timeframe_uid`,
        `tf`.`description` AS `timeframe`,
        `l`.`ticket_value` AS `ticket_value`,
        `l`.`otherinfo` AS `otherinfo`,
        `l`.`assigned_to_uid` AS `assigned_to_uid`,
        `ag`.`name` AS `agent`,
        `l`.`status` AS `status`,
        `l`.`createdby` AS `createdby`,
        `l`.`createdon` AS `createdon`,
        `l`.`modifiedby` AS `modifiedby`,
        `l`.`modifiedon` AS `modifiedon`,
        (TO_DAYS(NOW()) - TO_DAYS(`l`.`createdon`)) AS `age`,
        (CASE
            WHEN (`scat`.`txtfield_1` = 'Fixed') THEN `scat`.`numfield_1`
            WHEN (`scat`.`txtfield_1` = 'Percentage') THEN ((`scat`.`numfield_1` * `l`.`ticket_value`) / 100)
        END) AS `reward`,
        (CASE
            WHEN (`scat`.`txtfield_2` = 'Fixed') THEN `scat`.`numfield_2`
            WHEN (`scat`.`txtfield_2` = 'Percentage') THEN ((`scat`.`numfield_2` * `l`.`ticket_value`) / 100)
        END) AS `purchase_cost`,
        `spl`.`lead_uid` AS `lead_uid`,
        `spl`.`total_count` AS `total_count`,
        `spl`.`inprocess_count` AS `inprocess_count`,
        `spl`.`accepted_count` AS `accepted_count`,
        `spl`.`rejected_count` AS `rejected_count`,
        `spl`.`terminated_count` AS `terminated_count`,
        `spl`.`inprogress_count` AS `inprogress_count`,
        `spl`.`approved_count` AS `approved_count`,
        `spl`.`declined_count` AS `declined_count`,
        (CASE
            WHEN (`spl`.`total_count` = `spl`.`rejected_count`) THEN 'Rejected'
            WHEN (`spl`.`total_count` = `spl`.`acceptance_terminated_count`) THEN 'Terminated'
            WHEN (`spl`.`total_count` = `spl`.`declined_count`) THEN 'Declined'
            WHEN (`spl`.`total_count` = `spl`.`approved_count`) THEN 'Approved'
            WHEN (`spl`.`accepted_count` = 0) THEN 'None Accepted'
            WHEN (`spl`.`total_count` = `spl`.`terminated_count`) THEN 'Terminated'
            WHEN
                ((`spl`.`inprocess_count` = 0)
                    AND (`spl`.`inprogress_count` = 0)
                    AND (`spl`.`approved_count` > 0))
            THEN
                'Approved'
            WHEN
                ((`spl`.`inprocess_count` = 0)
                    AND (`spl`.`inprogress_count` = 0)
                    AND ((`spl`.`terminated_count` > 0)
                    OR (`spl`.`declined_count` > 0)))
            THEN
                'Declined'
            WHEN
                ((`spl`.`inprogress_count` > 0)
                    AND (`spl`.`approved_count` = 0))
            THEN
                'In Progress'
            WHEN
                (((`spl`.`inprogress_count` > 0)
                    OR (`spl`.`inprocess_count` > 0))
                    AND (`spl`.`approved_count` > 0))
            THEN
                'Approved by some'
            ELSE 'In Progress'
        END) AS `sp_approvals`
    FROM
        ((((((((`leads` `l`
        JOIN `users` `lg` ON ((`l`.`user_uid` = `lg`.`uid`)))
        LEFT JOIN `masterlist` `s` ON ((`l`.`state_uid` = `s`.`uid`)))
        LEFT JOIN `masterlist` `ct` ON ((`l`.`city_uid` = `ct`.`uid`)))
        JOIN `masterlist` `cat` ON ((`l`.`category_uid` = `cat`.`uid`)))
        JOIN `masterlist` `scat` ON ((`l`.`subcategory_uid` = `scat`.`uid`)))
        JOIN `masterlist` `tf` ON ((`l`.`timeframe_uid` = `tf`.`uid`)))
        LEFT JOIN `users` `ag` ON ((`l`.`assigned_to_uid` = `ag`.`uid`)))
        LEFT JOIN `vw_spleads_counts` `spl` ON ((`spl`.`lead_uid` = `l`.`uid`)))
    WHERE
        (`l`.`status` NOT IN ('Terminated' , 'Completed',
            'Withdrawn',
            'Declined',
            'Invalid Lead',
            'Verification Failed',
            'Rejected By All'))
    ORDER BY (CASE
        WHEN (`spl`.`total_count` = `spl`.`rejected_count`) THEN 'Rejected'
        WHEN (`spl`.`total_count` = `spl`.`acceptance_terminated_count`) THEN 'Terminated'
        WHEN (`spl`.`total_count` = `spl`.`declined_count`) THEN 'Declined'
        WHEN (`spl`.`total_count` = `spl`.`approved_count`) THEN 'Approved'
        WHEN (`spl`.`accepted_count` = 0) THEN 'None Accepted'
        WHEN (`spl`.`total_count` = `spl`.`terminated_count`) THEN 'Terminated'
        WHEN
            ((`spl`.`inprocess_count` = 0)
                AND (`spl`.`inprogress_count` = 0)
                AND (`spl`.`approved_count` > 0))
        THEN
            'Approved'
        WHEN
            ((`spl`.`inprocess_count` = 0)
                AND (`spl`.`inprogress_count` = 0)
                AND ((`spl`.`terminated_count` > 0)
                OR (`spl`.`declined_count` > 0)))
        THEN
            'Declined'
        WHEN
            ((`spl`.`inprogress_count` > 0)
                AND (`spl`.`approved_count` = 0))
        THEN
            'In Progress'
        WHEN
            ((`spl`.`inprogress_count` > 0)
                AND (`spl`.`approved_count` > 0))
        THEN
            'Approved by some'
        ELSE 'In Progress'
    END) , `l`.`client_name` , `l`.`modifiedon` ;$$

DELIMITER $$

DROP VIEW IF EXISTS `vw_openleads_sp`;$$

DELIMITER $$

CREATE VIEW `vw_openleads_sp` AS
    SELECT 
        (CASE
            WHEN (`spl`.`acceptance_status` = 'Accepted') THEN `l`.`client_name`
            ELSE 'Undisclosed'
        END) AS `client_name`,
        (CASE
            WHEN (`spl`.`acceptance_status` = 'Accepted') THEN `l`.`client_phone`
            ELSE 'Undisclosed'
        END) AS `client_phone`,
        (CASE
            WHEN (`spl`.`acceptance_status` = 'Accepted') THEN `l`.`client_email`
            ELSE 'Undisclosed'
        END) AS `client_email`,
        `l`.`state_uid` AS `state_uid`,
        `s`.`description` AS `state`,
        `l`.`other_state` AS `other_state`,
        `l`.`city_uid` AS `city_uid`,
        `ct`.`description` AS `city`,
        `l`.`other_city` AS `other_city`,
        `l`.`category_uid` AS `category_uid`,
        `cat`.`description` AS `category`,
        `l`.`subcategory_uid` AS `subcategory_uid`,
        `scat`.`description` AS `subcategory`,
        `l`.`timeframe_uid` AS `timeframe_uid`,
        `tf`.`description` AS `timeframe`,
        `l`.`ticket_value` AS `ticket_value`,
        `l`.`otherinfo` AS `otherinfo`,
        `l`.`status` AS `lead_status`,
        `spl`.`uid` AS `uid`,
        `spl`.`sp_uid` AS `sp_uid`,
        `spl`.`lead_uid` AS `lead_uid`,
        `spl`.`shared_on` AS `shared_on`,
        `spl`.`viewed_on` AS `viewed_on`,
        `spl`.`acceptance_status` AS `acceptance_status`,
        `spl`.`accepted_on` AS `accepted_on`,
        `spl`.`status` AS `status`,
        `spl`.`comments` AS `comments`,
        `spl`.`apr_ticket_value` AS `apr_ticket_value`,
        `spl`.`active` AS `active`,
        `spl`.`modifiedon` AS `modifiedon`,
        (CASE
            WHEN
                ((`l`.`status` = 'In Progress')
                    AND (`spl`.`acceptance_status` IN ('Yet To View' , 'Viewed')))
            THEN
                (TO_DAYS(NOW()) - TO_DAYS(`spl`.`shared_on`))
            WHEN
                ((`l`.`status` = 'In Progress')
                    AND (`spl`.`acceptance_status` = 'Accepted'))
            THEN
                (TO_DAYS(NOW()) - TO_DAYS(`spl`.`accepted_on`))
        END) AS `age`
    FROM
        ((((((`sp_leads` `spl`
        JOIN `leads` `l` ON ((`l`.`uid` = `spl`.`lead_uid`)))
        LEFT JOIN `masterlist` `s` ON ((`s`.`uid` = `l`.`state_uid`)))
        LEFT JOIN `masterlist` `ct` ON ((`ct`.`uid` = `l`.`city_uid`)))
        JOIN `masterlist` `cat` ON ((`cat`.`uid` = `l`.`category_uid`)))
        JOIN `masterlist` `scat` ON ((`scat`.`uid` = `l`.`subcategory_uid`)))
        LEFT JOIN `masterlist` `tf` ON ((`tf`.`uid` = `l`.`timeframe_uid`)))
    WHERE
        ((`spl`.`active` = 1)
            AND (`l`.`status` = 'In Progress')
            AND (`spl`.`acceptance_status` IN ('Yet To View' , 'Viewed', 'Accepted'))
            AND (`spl`.`status` IN ('New' , 'In Progress')))
    ORDER BY (CASE
        WHEN (`spl`.`acceptance_status` = 'Accepted') THEN `l`.`client_name`
        ELSE 'Undisclosed'
    END) ;$$


DELIMITER $$

DROP VIEW IF EXISTS `vw_pastleads`;$$

DELIMITER $$

CREATE VIEW `vw_pastleads` AS
    SELECT 
        `l`.`uid` AS `uid`,
        `l`.`user_uid` AS `user_uid`,
        `lg`.`name` AS `leadpasser`,
        `l`.`client_org` AS `client_org`,
        `l`.`client_name` AS `client_name`,
        `l`.`client_phone` AS `client_phone`,
        `l`.`client_email` AS `client_email`,
        `l`.`state_uid` AS `state_uid`,
        `s`.`description` AS `state`,
        `l`.`city_uid` AS `city_uid`,
        `ct`.`description` AS `city`,
        `l`.`other_state` AS `other_state`,
        `l`.`other_city` AS `other_city`,
        `l`.`category_uid` AS `category_uid`,
        `cat`.`description` AS `category`,
        `l`.`subcategory_uid` AS `subcategory_uid`,
        `scat`.`description` AS `subcategory`,
        `l`.`timeframe_uid` AS `timeframe_uid`,
        `tf`.`description` AS `timeframe`,
        `l`.`ticket_value` AS `ticket_value`,
        `l`.`otherinfo` AS `otherinfo`,
        `l`.`assigned_to_uid` AS `assigned_to_uid`,
        `ag`.`name` AS `agent`,
        `l`.`status` AS `status`,
        `l`.`createdby` AS `createdby`,
        `l`.`createdon` AS `createdon`,
        `l`.`modifiedby` AS `modifiedby`,
        `l`.`modifiedon` AS `modifiedon`,
        (TO_DAYS(`l`.`modifiedon`) - TO_DAYS(`l`.`createdon`)) AS `age`,
        (CASE
            WHEN
                (`scat`.`txtfield_1` = 'Fixed')
            THEN
                CAST(IF(ISNULL(`l`.`apr_ticket_value`),
                        0,
                        `scat`.`numfield_1`)
                    AS DECIMAL (15 , 2 ))
            WHEN
                (`scat`.`txtfield_1` = 'Percentage')
            THEN
                CAST(IFNULL(((`scat`.`numfield_1` * `l`.`apr_ticket_value`) / 100),
                            0)
                    AS DECIMAL (15 , 2 ))
        END) AS `reward`,
        (CASE
            WHEN
                (`scat`.`txtfield_2` = 'Fixed')
            THEN
                CAST(IF(ISNULL(`l`.`apr_ticket_value`),
                        0,
                        `scat`.`numfield_2`)
                    AS DECIMAL (15 , 2 ))
            WHEN
                (`scat`.`txtfield_2` = 'Percentage')
            THEN
                CAST(IFNULL(((`scat`.`numfield_2` * `l`.`apr_ticket_value`) / 100),
                            0)
                    AS DECIMAL (15 , 2 ))
        END) AS `purchase_cost`,
        `l`.`comments_verification` AS `comments_verification`,
        `l`.`apr_ticket_value` AS `apr_ticket_value`,
        `spl`.`lead_uid` AS `lead_uid`,
        `spl`.`total_count` AS `total_count`,
        `spl`.`inprocess_count` AS `inprocess_count`,
        `spl`.`accepted_count` AS `accepted_count`,
        `spl`.`rejected_count` AS `rejected_count`,
        `spl`.`terminated_count` AS `terminated_count`,
        `spl`.`inprogress_count` AS `inprogress_count`,
        `spl`.`approved_count` AS `approved_count`,
        `spl`.`declined_count` AS `declined_count`
    FROM
        ((((((((`leads` `l`
        JOIN `users` `lg` ON ((`l`.`user_uid` = `lg`.`uid`)))
        LEFT JOIN `masterlist` `s` ON ((`l`.`state_uid` = `s`.`uid`)))
        LEFT JOIN `masterlist` `ct` ON ((`l`.`city_uid` = `ct`.`uid`)))
        JOIN `masterlist` `cat` ON ((`l`.`category_uid` = `cat`.`uid`)))
        JOIN `masterlist` `scat` ON ((`l`.`subcategory_uid` = `scat`.`uid`)))
        JOIN `masterlist` `tf` ON ((`l`.`timeframe_uid` = `tf`.`uid`)))
        LEFT JOIN `users` `ag` ON ((`l`.`assigned_to_uid` = `ag`.`uid`)))
        LEFT JOIN `vw_spleads_counts` `spl` ON ((`spl`.`lead_uid` = `l`.`uid`)))
    WHERE
        (`l`.`status` IN ('Terminated' , 'Completed',
            'Withdrawn',
            'Declined',
            'Invalid Lead',
            'Verification Failed',
            'Rejected By All'))
    ORDER BY `l`.`status` , `l`.`client_name` , `l`.`modifiedon` DESC;$$

DELIMITER $$

DROP VIEW IF EXISTS `vw_pastleads_sp`;$$

DELIMITER $$

CREATE VIEW `vw_pastleads_sp` AS
    SELECT 
        (CASE
            WHEN (`spl`.`acceptance_status` = 'Accepted') THEN `l`.`client_name`
            ELSE 'Undisclosed'
        END) AS `client_name`,
        (CASE
            WHEN (`spl`.`acceptance_status` = 'Accepted') THEN `l`.`client_org`
            ELSE 'Undisclosed'
        END) AS `client_org`,
        (CASE
            WHEN (`spl`.`acceptance_status` = 'Accepted') THEN `l`.`client_phone`
            ELSE 'Undisclosed'
        END) AS `client_phone`,
        (CASE
            WHEN (`spl`.`acceptance_status` = 'Accepted') THEN `l`.`client_email`
            ELSE 'Undisclosed'
        END) AS `client_email`,
        `l`.`state_uid` AS `state_uid`,
        `s`.`description` AS `state`,
        `l`.`other_state` AS `other_state`,
        `l`.`city_uid` AS `city_uid`,
        `ct`.`description` AS `city`,
        `l`.`other_city` AS `other_city`,
        `l`.`category_uid` AS `category_uid`,
        `cat`.`description` AS `category`,
        `l`.`subcategory_uid` AS `subcategory_uid`,
        `scat`.`description` AS `subcategory`,
        `l`.`timeframe_uid` AS `timeframe_uid`,
        `tf`.`description` AS `timeframe`,
        `l`.`ticket_value` AS `ticket_value`,
        `l`.`otherinfo` AS `otherinfo`,
        `l`.`status` AS `lead_status`,
        `spl`.`uid` AS `uid`,
        `spl`.`sp_uid` AS `sp_uid`,
        `spl`.`lead_uid` AS `lead_uid`,
        `spl`.`shared_on` AS `shared_on`,
        `spl`.`viewed_on` AS `viewed_on`,
        `spl`.`acceptance_status` AS `acceptance_status`,
        `spl`.`accepted_on` AS `accepted_on`,
        `spl`.`status` AS `status`,
        `spl`.`comments` AS `comments`,
        `spl`.`apr_ticket_value` AS `apr_ticket_value`,
        `spl`.`active` AS `active`,
        `spl`.`modifiedon` AS `modifiedon`,
        (TO_DAYS(`spl`.`modifiedon`) - TO_DAYS(`spl`.`shared_on`)) AS `age`
    FROM
        ((((((`sp_leads` `spl`
        JOIN `leads` `l` ON ((`l`.`uid` = `spl`.`lead_uid`)))
        LEFT JOIN `masterlist` `s` ON ((`s`.`uid` = `l`.`state_uid`)))
        LEFT JOIN `masterlist` `ct` ON ((`ct`.`uid` = `l`.`city_uid`)))
        JOIN `masterlist` `cat` ON ((`cat`.`uid` = `l`.`category_uid`)))
        JOIN `masterlist` `scat` ON ((`scat`.`uid` = `l`.`subcategory_uid`)))
        LEFT JOIN `masterlist` `tf` ON ((`tf`.`uid` = `l`.`timeframe_uid`)))
    WHERE
        ((`spl`.`active` = 1)
            AND ((`l`.`status` IN ('Completed' , 'Terminated', 'Rejected By All'))
            OR (`spl`.`acceptance_status` = 'Rejected')
            OR (`spl`.`status` IN ('Terminated' , 'Declined', 'Approved'))))
    ORDER BY `spl`.`status` , (CASE
        WHEN (`spl`.`acceptance_status` = 'Accepted') THEN `l`.`client_name`
        ELSE 'Undisclosed'
    END) ;$$
    

DELIMITER $$

DROP VIEW IF EXISTS `vw_spcatalog`;$$

DELIMITER $$

CREATE VIEW `vw_spcatalog` AS
    SELECT 
        `spc`.`uid` AS `uid`,
        `spc`.`sp_uid` AS `sp_uid`,
        `spc`.`category_uid` AS `category_uid`,
        `c`.`description` AS `category`,
        `spc`.`subcategory_uid` AS `subcategory_uid`,
        `s`.`description` AS `subcategory`,
        `spc`.`active` AS `active`,
        COUNT(`spcl`.`uid`) AS `locations`,
        `spc`.`createdby` AS `createdby`,
        `spc`.`createdon` AS `createdon`,
        `spc`.`modifiedby` AS `modifiedby`,
        `spc`.`modifiedon` AS `modifiedon`
    FROM
        ((((`sp_catalog` `spc`
        JOIN `users` `u` ON ((`u`.`uid` = `spc`.`sp_uid`)))
        JOIN `masterlist` `c` ON ((`c`.`uid` = `spc`.`category_uid`)))
        JOIN `masterlist` `s` ON ((`s`.`uid` = `spc`.`subcategory_uid`)))
        LEFT JOIN `sp_catalog_locations` `spcl` ON ((`spcl`.`sp_catalog_uid` = `spc`.`uid`)))
    GROUP BY `spc`.`uid` , `spc`.`sp_uid` , `spc`.`category_uid` , `c`.`description` , `spc`.`subcategory_uid` , `s`.`description` , `spc`.`active` , `spc`.`createdby` , `spc`.`createdon` , `spc`.`modifiedby` , `spc`.`modifiedon`
    ORDER BY `c`.`description` , `s`.`description`;$$

DELIMITER $$

DROP VIEW IF EXISTS `vw_spcataloglocations`;$$

DELIMITER $$

CREATE VIEW `vw_spcataloglocations` AS
    SELECT 
        `spcl`.`uid` AS `uid`,
        `spc`.`sp_uid` AS `sp_uid`,
        `spcl`.`sp_catalog_uid` AS `sp_catalog_uid`,
        `spcl`.`country_uid` AS `country_uid`,
        IFNULL(`c`.`description`, '') AS `country`,
        `spcl`.`state_uid` AS `state_uid`,
        IFNULL(`s`.`description`, '') AS `state`,
        `spcl`.`district_uid` AS `district_uid`,
        IFNULL(`d`.`description`, '') AS `district`,
        `spcl`.`city_uid` AS `city_uid`,
        IFNULL(`ct`.`description`, '') AS `city`,
        `spcl`.`active` AS `active`,
        `spcl`.`createdby` AS `createdby`,
        `spcl`.`createdon` AS `createdon`,
        `spcl`.`modifiedby` AS `modifiedby`,
        `spcl`.`modifiedon` AS `modifiedon`
    FROM
        ((((((`sp_catalog_locations` `spcl`
        JOIN `sp_catalog` `spc` ON ((`spc`.`uid` = `spcl`.`sp_catalog_uid`)))
        JOIN `users` `u` ON ((`u`.`uid` = `spc`.`sp_uid`)))
        JOIN `masterlist` `c` ON ((`c`.`uid` = `spcl`.`country_uid`)))
        LEFT JOIN `masterlist` `s` ON ((`s`.`uid` = `spcl`.`state_uid`)))
        LEFT JOIN `masterlist` `d` ON ((`d`.`uid` = `spcl`.`district_uid`)))
        LEFT JOIN `masterlist` `ct` ON ((`ct`.`uid` = `spcl`.`city_uid`)));$$ 


DELIMITER $$ 

DROP VIEW IF EXISTS `vw_spleads_counts`;$$

DELIMITER $$

CREATE VIEW `vw_spleads_counts` AS
    SELECT 
        `sp_leads`.`lead_uid` AS `lead_uid`,
        COUNT(0) AS `total_count`,
        SUM(IF(((`sp_leads`.`acceptance_status` = 'Yet To View')
                OR (`sp_leads`.`acceptance_status` = 'Viewed')),
            1,
            0)) AS `inprocess_count`,
        SUM(IF((`sp_leads`.`acceptance_status` = 'Accepted'),
            1,
            0)) AS `accepted_count`,
        SUM(IF((`sp_leads`.`acceptance_status` = 'Rejected'),
            1,
            0)) AS `rejected_count`,
        SUM(IF((`sp_leads`.`acceptance_status` = 'Terminated'),
            1,
            0)) AS `acceptance_terminated_count`,
        SUM(IF(((`sp_leads`.`acceptance_status` = 'Accepted')
                AND (`sp_leads`.`status` = 'In Progress')),
            1,
            0)) AS `inprogress_count`,
        SUM(IF(((`sp_leads`.`acceptance_status` = 'Accepted')
                AND (`sp_leads`.`status` = 'Approved')),
            1,
            0)) AS `approved_count`,
        SUM(IF(((`sp_leads`.`acceptance_status` = 'Accepted')
                AND (`sp_leads`.`status` = 'Declined')),
            1,
            0)) AS `declined_count`,
        SUM(IF((`sp_leads`.`status` = 'Terminated'),
            1,
            0)) AS `terminated_count`
    FROM
        `sp_leads`
    WHERE
        (`sp_leads`.`active` = 1)
    GROUP BY `sp_leads`.`lead_uid`;$$

DELIMITER $$


