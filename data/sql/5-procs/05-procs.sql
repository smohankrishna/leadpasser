DELIMITER $$

DROP PROCEDURE IF EXISTS `get_documenttypes`;$$

DELIMITER $$

CREATE  PROCEDURE `get_documenttypes`(`doctypecode` VARCHAR(20))
BEGIN
    select 
        d.uid as doc_type_uid
    , 	d.description as doc_type
    ,	d.active
    ,	d.seq
    from masterlist as l
    left join masterlist as d on d.parent_uid = l.uid
    where 
            l.code = doctypecode
    order by 
            d.active desc, d.seq, d.description;
END ;$$

DELIMITER $$

DROP PROCEDURE IF EXISTS `get_leaddetails`;$$


DELIMITER $$

CREATE  PROCEDURE `get_leaddetails`(`uid` VARCHAR(40))
BEGIN
    SELECT 
        `l`.`uid` AS `uid`,
        `l`.`user_uid` AS `user_uid`,
        `lg`.`name` AS `leadpasser`,
        `l`.`client_org` AS `client_org`,
        `l`.`client_name` AS `client_name`,
        `l`.`client_phone` AS `client_phone`,
        `l`.`client_email` AS `client_email`,
        `l`.`state_uid` AS `state_uid`,
        `s`.`description` AS `state`,
        `l`.`city_uid` AS `city_uid`,
        `ct`.`description` AS `city`,
        `l`.`other_state` AS `other_state`,
        `l`.`other_city` AS `other_city`,
        `l`.`category_uid` AS `category_uid`,
        `cat`.`description` AS `category`,
        `l`.`subcategory_uid` AS `subcategory_uid`,
        `scat`.`description` AS `subcategory`,
        `l`.`timeframe_uid` AS `timeframe_uid`,
        `tf`.`description` AS `timeframe`,
        `l`.`ticket_value` AS `ticket_value`,
        `l`.`otherinfo` AS `otherinfo`,
        `l`.`assigned_to_uid` AS `assigned_to_uid`,
        `ag`.`name` AS `agent`,
        `l`.`status` AS `status`,
        `l`.`createdby` AS `createdby`,
        `l`.`createdon` AS `createdon`,
        `l`.`modifiedby` AS `modifiedby`,
        `l`.`modifiedon` AS `modifiedon`,
        l.apr_ticket_value,
        l.comments_verification,
        l.comments_final,
        (TO_DAYS(NOW()) - TO_DAYS(`l`.`createdon`)) AS `age`,
        (CASE
            WHEN (`scat`.`txtfield_1` = 'Fixed') THEN `scat`.`numfield_1`
            WHEN (`scat`.`txtfield_1` = 'Percentage') THEN (`scat`.`numfield_1` * `l`.`ticket_value`/100)
        END) AS `reward`,
        (CASE
            WHEN (`scat`.`txtfield_2` = 'Fixed') THEN `scat`.`numfield_2`
            WHEN (`scat`.`txtfield_2` = 'Percentage') THEN (`scat`.`numfield_2` * `l`.`ticket_value`/100)
        END) AS `purchase_cost`,


        (CASE
            WHEN
                (`scat`.`txtfield_1` = 'Fixed')
            THEN
                CAST(IF(ISNULL(`l`.`apr_ticket_value`),
                        0,
                        `scat`.`numfield_1`)
                    AS DECIMAL (15 , 2 ))
            WHEN
                (`scat`.`txtfield_1` = 'Percentage')
            THEN
                CAST(IFNULL(((`scat`.`numfield_1` * `l`.`apr_ticket_value`) / 100),
                            0)
                    AS DECIMAL (15 , 2 ))
        END) AS `apr_reward`,
        (CASE
            WHEN
                (`scat`.`txtfield_2` = 'Fixed')
            THEN
                CAST(IF(ISNULL(`l`.`apr_ticket_value`),
                        0,
                        `scat`.`numfield_2`)
                    AS DECIMAL (15 , 2 ))
            WHEN
                (`scat`.`txtfield_2` = 'Percentage')
            THEN
                CAST(IFNULL(((`scat`.`numfield_2` * `l`.`apr_ticket_value`) / 100),
                            0)
                    AS DECIMAL (15 , 2 ))
        END) AS `apr_purchase_cost`,

        case
			WHEN `status` IN ('Terminated' , 'Completed',
            'Withdrawn', 'Declined',
            'Invalid Lead',
            'Verification Failed',
            'Rejected By All') THEN 'Past'
            ELSE 'Open'
		END as `leadtype`,
        `spl`.`lead_uid` AS `lead_uid`,
        `spl`.`total_count` AS `total_count`,
        `spl`.`inprocess_count` AS `inprocess_count`,
        `spl`.`accepted_count` AS `accepted_count`,
        `spl`.`rejected_count` AS `rejected_count`,
        `spl`.`terminated_count` AS `terminated_count`,
        `spl`.`inprogress_count` AS `inprogress_count`,
        `spl`.`approved_count` AS `approved_count`,
        `spl`.`declined_count` AS `declined_count`
    FROM
        (((((((`leads` `l`
        JOIN `users` `lg` ON ((`l`.`user_uid` = `lg`.`uid`)))
        LEFT JOIN `masterlist` `s` ON ((`l`.`state_uid` = `s`.`uid`)))
        LEFT JOIN `masterlist` `ct` ON ((`l`.`city_uid` = `ct`.`uid`)))
        JOIN `masterlist` `cat` ON ((`l`.`category_uid` = `cat`.`uid`)))
        JOIN `masterlist` `scat` ON ((`l`.`subcategory_uid` = `scat`.`uid`)))
        JOIN `masterlist` `tf` ON ((`l`.`timeframe_uid` = `tf`.`uid`)))
        LEFT JOIN `users` `ag` ON ((`l`.`assigned_to_uid` = `ag`.`uid`)))
        LEFT JOIN (SELECT 
            `sp_leads`.`lead_uid` AS `lead_uid`,
                COUNT(0) AS `total_count`,
                SUM(IF(((`sp_leads`.`acceptance_status` = 'Yet To View')
                    OR (`sp_leads`.`acceptance_status` = 'Viewed')), 1, 0)) AS `inprocess_count`,
                SUM(IF((`sp_leads`.`acceptance_status` = 'Accepted'), 1, 0)) AS `accepted_count`,
                SUM(IF((`sp_leads`.`acceptance_status` = 'Rejected'), 1, 0)) AS `rejected_count`,
                SUM(IF((`sp_leads`.`acceptance_status` = 'Terminated'), 1, 0)) AS `terminated_count`,
                SUM(IF(((`sp_leads`.`acceptance_status` = 'Accepted')
                    AND (`sp_leads`.`status` = 'In Progress')), 1, 0)) AS `inprogress_count`,
                SUM(IF(((`sp_leads`.`acceptance_status` = 'Accepted')
                    AND (`sp_leads`.`status` = 'Approved')), 1, 0)) AS `approved_count`,
                SUM(IF(((`sp_leads`.`acceptance_status` = 'Accepted')
                    AND (`sp_leads`.`status` = 'Declined')), 1, 0)) AS `declined_count`
        FROM
            `sp_leads`
        WHERE
            (`sp_leads`.`active` = 1)
        GROUP BY `sp_leads`.`lead_uid`) `spl` ON ((`spl`.`lead_uid` = `l`.`uid`))
    WHERE
        l.uid = uid;

END ;$$

DELIMITER $$

DROP PROCEDURE IF EXISTS `get_leadserviceproviders`;$$

DELIMITER $$

CREATE  PROCEDURE `get_leadserviceproviders`(`p_lead_uid` VARCHAR(40), `p_sp_uid` VARCHAR(40))
BEGIN

	select * from vw_leads where uid = p_lead_uid;

	SELECT 
        `l`.`uid` AS `lead_uid`,
        `spc`.`sp_uid` AS `sp_uid`,
        `u`.`organization` AS `organization`,
        IF((`spcl`.`state_uid` = ''),
            'All States',
            'Selected State') AS `state_presence`,
        (CASE
            WHEN
                (ISNULL(`spl`.`uid`)
                    OR (`spl`.`active` = 0))
            THEN
                0
            ELSE 1
        END) AS `sharing_status`,
        `spl`.`uid` AS `spl_uid`,
        `spl`.`shared_on` AS `shared_on`,
        `spl`.`viewed_on` AS `viewed_on`,
        `spl`.`acceptance_status` AS `acceptance_status`,
        `spl`.`accepted_on` AS `accepted_on`,
        `spl`.`status` AS `status`,
        `spl`.`apr_ticket_value` AS `apr_ticket_value`,
        `spl`.`active` AS `active`
    FROM
        ((((`leads` `l`
        JOIN `sp_catalog` `spc` ON (((`spc`.`category_uid` = `l`.`category_uid`)
            AND (`spc`.`subcategory_uid` = `l`.`subcategory_uid`)
            AND (`spc`.`active` = 1))))
        JOIN `users` `u` ON (((`u`.`uid` = `spc`.`sp_uid`)
            AND (`u`.`active` = 1))))
        LEFT JOIN `sp_catalog_locations` `spcl` ON ((`spcl`.`sp_catalog_uid` = `spc`.`uid`)))
        LEFT JOIN `sp_leads` `spl` ON ((`spl`.`sp_uid` = `spc`.`sp_uid` AND spl.lead_uid = l.uid)))
    WHERE
		l.uid = p_lead_uid and
        spc.sp_uid = IFNULL(p_sp_uid, spc.sp_uid) and
        ((`spcl`.`uid` IS NOT NULL)
            AND ((`spcl`.`state_uid` = '')
            OR (`spcl`.`state_uid` = IFNULL(`l`.`state_uid`, `spcl`.`state_uid`))));
            


END;$$


DELIMITER $$

DROP PROCEDURE IF EXISTS `get_locationmaster`;$$

DELIMITER $$

CREATE  PROCEDURE `get_locationmaster`()
BEGIN

	select DISTINCT 
		c.uid as 'country_uid',
		c.code as 'country_code',
		c.description as 'country_name',
		c.active as 'country_active'
	from masterlist as l
	INNER join masterlist as c on c.parent_uid = l.uid and c.active = 1
	where 
		l.code='_geo_locations'
	and c.code = 'IN'
	order by c.description;

    select DISTINCT 
		c.uid as 'country_uid',
        s.uid as 'state_uid',
		s.code as 'state_code',
		s.description as 'state_name',
		s.active as 'state_active'
	from masterlist as l
	INNER join masterlist as c on c.parent_uid = l.uid and c.active = 1
    inner join masterlist as s on s.parent_uid = c.uid and s.active = 1
	where 
		l.code='_geo_locations'
	and c.code = 'IN'
	order by c.uid, s.description;

	select DISTINCT 
		s.uid as 'state_uid',
        d.uid as 'district_uid',
		d.code as 'district_code',
		d.description as 'district_name',
		d.active as 'district_active'
	from masterlist as l
	INNER join masterlist as c on c.parent_uid = l.uid and c.active = 1
    inner join masterlist as s on s.parent_uid = c.uid and s.active = 1
    inner join masterlist as d on d.parent_uid = s.uid and d.active = 1
	where 
		l.code='_geo_locations'
	and c.code = 'IN'
	order by s.uid, d.description;
    
    select DISTINCT 
		s.uid as 'state_uid',
        d.uid as 'district_uid',
        ct.uid as 'city_uid',
		ct.code as 'city_code',
		ct.description as 'city_name',
		ct.active as 'city_active'
	from masterlist as l
	INNER join masterlist as c on c.parent_uid = l.uid and c.active = 1
    inner join masterlist as s on s.parent_uid = c.uid and s.active = 1
    inner join masterlist as d on d.parent_uid = s.uid and d.active = 1
    inner join masterlist as ct on ct.parent_uid = d.uid and ct.active = 1
	where 
		l.code='_geo_locations'
	and c.code = 'IN'
	order by d.uid, ct.description;
END;$$

DELIMITER $$

DROP PROCEDURE IF EXISTS `get_locations`;$$

DELIMITER $$

CREATE  PROCEDURE `get_locations`()
BEGIN

	select 
		c.uid as CountryUID
	,	s.uid as StateUID
	,	d.uid as DistrictUID
	,	ct.uid as CityUID
	, 	c.code as CountryCode
	, 	c.description as CountryName
	,	s.code as StateCode
	,	s.description as StateName
	,	d.code as DistrictCode
	,	d.description as DistrictName
	,	ct.code as CityCode
	,	ct.description as CityName
	from masterlist as l
	left join masterlist as c on c.parent_uid = l.uid
	left join masterlist as s on s.parent_uid = c.uid
	left join masterlist as d on d.parent_uid = s.uid
	left join masterlist as ct on ct.parent_uid = d.uid
	where 
		l.code = '_geo_locations'
	order by 
		c.description, s.description, d.description, ct.description;
    

END;$$

DELIMITER $$

DROP PROCEDURE IF EXISTS `get_masterlist`;$$

DELIMITER $$

CREATE  PROCEDURE `get_masterlist`(`parent_uid` VARCHAR(40))
BEGIN

    select 
        p1.uid, 
        p1.code, 
        p1.description, 
		p1.parent_uid,
        p1.active, 
        p1.seq,
        p1.txtfield_1,
        p1.txtfield_2,
        p1.numfield_1,
        p1.numfield_2,
        p1.createdon, 
        p1.createdby,
        p1.modifiedon,
        p1.modifiedby
	from masterlist as p1
	where ifnull(p1.parent_uid,'') = IFNULL(parent_uid,'');

END;$$

DELIMITER $$

DROP PROCEDURE IF EXISTS `get_masterlistbycode`;$$

DELIMITER $$

CREATE  PROCEDURE `get_masterlistbycode`(`parent_code` VARCHAR(30))
BEGIN
	select 
		l.uid, 
        l.code, 
        l.description, 
		l.parent_uid,
        l.active, 
        l.seq,
        l.txtfield_1,
        l.txtfield_2,
        l.numfield_1,
        l.numfield_2,
        l.createdon, 
        l.createdby,
        l.modifiedon,
        l.modifiedby
	from masterlist as l
    inner join masterlist as p on l.parent_uid = p.uid
    where p.code = parent_code
    order by seq;
END;$$

DELIMITER $$

DROP PROCEDURE IF EXISTS `get_productcatalogmaster`;$$

DELIMITER $$

CREATE  PROCEDURE `get_productcatalogmaster`()
BEGIN
  
    
    select DISTINCT 
		c.uid,
		c.code,
		c.description,
		c.active
	from masterlist as pc
	INNER join masterlist as c on c.parent_uid = pc.uid and c.active = 1
	left outer join masterlist as sc on sc.parent_uid = c.uid and sc.active = 1
	where 
		pc.code='_product_catalog'
	and sc.uid IS NOT NULL
	order by c.description;

	select
		c.uid as 'categoryuid',
		sc.uid ,
		sc.code,
		sc.description,
        sc.txtfield_1 as 'buy_type',
        sc.numfield_1 as 'buy_price',
        sc.txtfield_2 as 'sell_type',
        sc.numfield_2 as 'sell_price',
		sc.active
	from masterlist as pc
	left outer join masterlist as c on c.parent_uid = pc.uid and c.active = 1
	left outer join masterlist as sc on sc.parent_uid = c.uid and sc.active = 1
	where 
		pc.code='_product_catalog'
	and sc.uid IS NOT NULL
	order by sc.description;

END;$$

DELIMITER $$

DROP PROCEDURE IF EXISTS `get_spleaddetails`;$$

DELIMITER $$

CREATE  PROCEDURE `get_spleaddetails`(`p_uid` VARCHAR(40))
BEGIN
	SELECT 
        `l`.`client_name` AS `client_name`,
        `l`.`client_phone` AS `client_phone`,
        `l`.`client_email` AS `client_email`,
        `l`.`state_uid` AS `state_uid`,
        `s`.`description` AS `state`,
        `l`.`other_state` AS `other_state`,
        `l`.`city_uid` AS `city_uid`,
        `ct`.`description` AS `city`,
        `l`.`other_city` AS `other_city`,
        `l`.`category_uid` AS `category_uid`,
        `cat`.`description` AS `category`,
        `l`.`subcategory_uid` AS `subcategory_uid`,
        `scat`.`description` AS `subcategory`,
        `l`.`timeframe_uid` AS `timeframe_uid`,
        `tf`.`description` AS `timeframe`,
        `l`.`ticket_value` AS `ticket_value`,
        `l`.`otherinfo` AS `otherinfo`,
        `l`.`status` AS `lead_status`,
        `spl`.`uid` AS `uid`,
        `spl`.`sp_uid` AS `sp_uid`,
        `spl`.`lead_uid` AS `lead_uid`,
        `spl`.`shared_on` AS `shared_on`,
        `spl`.`viewed_on` AS `viewed_on`,
        `spl`.`acceptance_status` AS `acceptance_status`,
        `spl`.`accepted_on` AS `accepted_on`,
        `spl`.`status` AS `status`,
        `spl`.`comments` AS `comments`,
        `spl`.`apr_ticket_value` AS `apr_ticket_value`,
        `spl`.`active` AS `active`,
        `spl`.`modifiedon` AS `modifiedon`,
        (CASE
            WHEN
                ((`l`.`status` = 'In Progress')
                    AND (`spl`.`acceptance_status` IN ('Yet To View' , 'Viewed')))
            THEN
                (TO_DAYS(NOW()) - TO_DAYS(`spl`.`shared_on`))
            WHEN
                ((`l`.`status` = 'In Progress')
                    AND (`spl`.`acceptance_status` = 'Accepted'))
            THEN
                (TO_DAYS(NOW()) - TO_DAYS(`spl`.`accepted_on`))
        END) AS `age`
    FROM
        ((((((`sp_leads` `spl`
        JOIN `leads` `l` ON ((`l`.`uid` = `spl`.`lead_uid`)))
        LEFT JOIN `masterlist` `s` ON ((`s`.`uid` = `l`.`state_uid`)))
        LEFT JOIN `masterlist` `ct` ON ((`ct`.`uid` = `l`.`city_uid`)))
        JOIN `masterlist` `cat` ON ((`cat`.`uid` = `l`.`category_uid`)))
        JOIN `masterlist` `scat` ON ((`scat`.`uid` = `l`.`subcategory_uid`)))
        LEFT JOIN `masterlist` `tf` ON ((`tf`.`uid` = `l`.`timeframe_uid`)))
    WHERE
       spl.uid = p_uid;
    
END;$$

DELIMITER $$

DROP PROCEDURE IF EXISTS `get_userdocuments`;$$

DELIMITER $$

CREATE  PROCEDURE `get_userdocuments`(`user_uid` VARCHAR(40))
BEGIN
	select 
		d.uid,
		d.user_uid,
		d.doc_type_uid,
		dt.description as 'doc_type',
		d.doc_title,
		d.doc_path,
		d.verified,
		d.createdby,
		d.createdon,
		d.modifiedby,
		d.modifiedon
	from documents as d
	inner join masterlist as dt on dt.uid = d.doc_type_uid
	where
		d.user_uid = user_uid;

END;$$

DELIMITER $$

DROP PROCEDURE IF EXISTS `upd_leadclosure`;$$

DELIMITER $$

CREATE  PROCEDURE `upd_leadclosure`(`p_lead_uid` VARCHAR(40), `p_source` VARCHAR(50))
BEGIN

	if p_source = 'leads' THEN
    
		select @lead_status := status from leads where uid = p_lead_uid;
		
        select @lead_status;
        
		IF @lead_status = 'Terminated' THEN

			update sp_leads
			set status = 'Terminated'
			,	comments = 'Terminated by System Admin'
			where lead_uid = p_lead_uid
			and	active = 1;

		END IF;
        
	ELSEIF p_source = 'sp_leads' THEN
		
		select @sp_total_count := count(*) 
        from sp_leads 
		where 
			lead_uid = p_lead_uid 
        and active = 1;
		
		select @splead_decline_count :=  count(*) 
        from sp_leads 
		where 
			lead_uid = p_lead_uid 
		and status in ('Declined')
		and active = 1;
        
        IF @splead_decline_count = @sp_total_count THEN
            update leads
			set	status = 'Rejected By All'
            where uid = lead_uid;
        
        END IF;

	END IF;

END;$$



