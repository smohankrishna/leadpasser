/**
 * http://usejsdoc.org/
 */

module.exports = (function() {
	var db = require("../services/db.js");
	var util = require("../services/utility.js");

	var getDocumentTypes = function(docTypeCode, options, callback) {

		db.getSP("get_documenttypes", [docTypeCode], function(err, results,
				rowCount) {
			if (err) {
				callback(err, [], rowCount);
			} else {
				callback(null, results, rowCount);
			}
		});
	};

	var getDocumentList = function(user_uid, options, callback) {

		db.getSP("get_userdocuments", [user_uid], function(err, results,
				rowCount) {
			if (err) {
				callback(err, [], rowCount);
			} else {
				callback(null, results, rowCount);
			}
		});
	};

	var insertDocument = function(docItem, callback) {

		if (docItem.uid) {
			var err = "Invalid Call";
			var result = null;
			return callback(err, result);
		} else {
			var insData = JSON.parse(JSON.stringify(docItem));
			insData.createdby = (insData.newcreatedby || 'system');
			insData.modifiedby = (insData.newmodifiedby || 'system');

			delete insData.newcreatedby;
			delete insData.newmodifiedby;
			delete insData.uid;
			delete insData.createdon;
			delete insData.modifiedon;

			db.insert("documents", insData, function(err, results) {
				return callback(err, results);
			});
		}
	};

	var updateDocument = function(docItem, callback) {

		var colValues = {
			str : "",
			data : []
		};

		for ( var key in docItem) {
			if (key == 'uid' || key == 'newcreatedby' || key == 'newmodifiedby'
					|| key == 'createdby' || key == 'createdon'
					|| key == 'modifiedon') { // DO NOTHING

			} else {
				colValues.str += (colValues.str.length > 0) ? ", " : "";
				colValues.str += key + " = ?";
				if (key == "modifiedby") {
					colValues.data.push(docItem.newmodifiedby || 'system');
				} else {
					colValues.data.push(docItem[key]);
				}
			}
		}

		if (docItem.newmodifiedby) {
			delete docItem.newmodifiedby;
		}

		var filter = {
			str : "uid = ?",
			data : [docItem.uid]
		};
		db.update("documents", colValues, filter, function(err, results) { //
//			console.log("DocumentSvc CLASS"); // console.log(err);

			return callback(err, results);
		});
	};
	
	var removeDocument = function(uid, callback){
		if(!uid){
			var err = "Invalid Call";
			var result = null;
			return callback(err, result);
		}else{
			db.removeByUid("documents", uid, function(err, results) {
				return callback(err, results);
			});
		}
	};

	var insertCloudDocument = function(docItem,callback){

		var docItem = JSON.parse(JSON.stringify(docItem));
		docItem.uid = util.guid();
		docItem.createdby = (docItem.createdby || 'system');
		docItem.modifiedby = (docItem.modifiedby || 'system');
		if(docItem.doc_title === 'PAN CARD'){
			docItem.doc_type_uid = 'eb7530d7-18ed-11e6-a7d5-02b453d9ac26';

		}else if (docItem.doc_title === 'Cancelled Cheque'){
            docItem.doc_type_uid = 'ceeebea0-5a06-11e9-a7aa-625f8949ced4';
		}

		db.insert("documents", docItem, function(err, results) {
			console.log(err);
			return callback(err, results);
		});

	};

	// var getCloudDocument = function(uid,callback){
	// 	var filter = {
     //        str: "user_uid = ?",
     //        data: [uid]
     //    };
	// 	db.get("documents", "*",filter, function(err, results) {
	// 		if(err){
	// 			 console.log(err);
	// 			 return callback(err, results);
	// 		}
	//
	// 		return callback(null, results);
	// 	});
	// };

	var insertBankDeatiles =function(docItem,callback){
		
		var docItem = JSON.parse(JSON.stringify(docItem));
		docItem.uid = util.guid();
		docItem.createdby = (docItem.createdby || 'system');
		docItem.modifiedby = (docItem.modifiedby || 'system');
		console.log(docItem);
		db.insert("lg_bank_details", docItem, function(err, results) {
			 console.log(err);
			return callback(err, results);
		});
	}
	
	return {
		getDocumentTypes : getDocumentTypes,
		getDocumentList : getDocumentList,
		insertDocument : insertDocument,
		updateDocument : updateDocument,
		removeDocument: removeDocument,
		insertCloudDocument : insertCloudDocument,
		//getCloudDocument : getCloudDocument,
		insertBankDeatiles : insertBankDeatiles
		
	};
}());
