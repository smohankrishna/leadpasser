/**
 * http://usejsdoc.org/
 */

module.exports = (function() {
	var mysql = require('mysql');
	var env = require('../environment.json');
	var pool = mysql.createPool({
		host : env.db.host,
		port : env.db.port,
		user : env.db.user,
		password : env.db.passwd,
		database : env.db.database,
		connectionLimit : env.db.maxconnections,
		debug : env.db.debug
	});

	// arguments : tablename, [columns, [filter, [options]]], callback
	var getData = function() {

		var argsLength = arguments.length;
		if (argsLength < 2) {
			throw "Invalid number of arguments";
			return;
		}

		var table, columns, filter, options, cb;

		table = arguments[0];
		columns = (argsLength >= 3) ? arguments[1] : "*";
		filter = (argsLength >= 4) ? arguments[2] : null;
		options = (argsLength >= 5) ? arguments[3] : null;
		cb = arguments[argsLength - 1];

		if (!options) {
			options = {
				limit : 999,
				offset : 0
			};
		}

		var data = pool
				.getConnection(function(err, conn) {
					if (err) {
						// conn.release();
						return cb(err);
					}

					var qry = "select SQL_CALC_FOUND_ROWS ${columns} from ${tablename} ${where} ${orderby} limit ${limit} offset ${offset}";

					qry = qry.replace("${tablename}", table).replace(
							"${columns}", columns);
					console.log("\n\n-> "+qry);
					if(options.orderby){
						qry = qry.replace("${orderby}", " order by "+ options.orderby);
					}else{
						qry = qry.replace("${orderby}","");
					}

					if(options.offset){
						qry = qry.replace("${offset}", options.offset);
					}else{
						qry = qry.replace("${offset}", "");
						qry = qry.replace("offset","");
					}

					if(options.limit){
						qry = qry.replace("${limit}",options.limit);
					}else{
						qry = qry.replace("${limit}", "");
						qry = qry.replace("limit", "");
					}

					if (filter && filter.str) {
						var sWhere = "where " + filter.str;
						qry = qry.replace("${where}", sWhere);
					} else {
						qry = qry.replace("${where}", "");
					}

//					console.log(filter);
//					console.log(qry);

					conn.query(qry, (filter ? filter.data : []), function(err,
							results, fields) {

						conn.query("SELECT FOUND_ROWS() as cnt", function(err2,
								rowCntResult) {

							var totalRows = rowCntResult[0].cnt;

							conn.release();
							if (err) {
								console.log(err);
								return cb(err);
							} else {
								return cb(null, results, totalRows);
							}

						});

					});
				});
	};

	var getDataByUid = function() {

		var argsLength = arguments.length;
		if (argsLength < 3 || argsLength > 4) {
			throw "Invalid number of arguments";
			return;
		}

		var table, uid, columns, cb;

		table = arguments[0];
		uid = arguments[1];
		columns = (argsLength == 4) ? arguments[1] : "*";
		cb = arguments[argsLength - 1];

		var data = pool.getConnection(function(err, conn) {
			if (err) {
				// conn.release();
				return cb(err);
			}

			var qry = "select ${columns} from ${tablename} where uid=?";

			qry = qry.replace("${tablename}", table).replace("${columns}",
					columns);

			conn.query(qry, [uid], function(err, results, fields) {
				conn.release();
				if (err) {
					return cb(err);
				} else {
					return cb(null, results, fields);
				}
			});
		});
	};

	// arguments : tablename, values, [options], callback
	var insertData = function() {

		var table, values, options, cb;

		if (arguments.length == 3) {
			table = arguments[0];
			values = arguments[1];
			cb = arguments[2];
		} else if (arguments.length == 4) {
			table = arguments[0];
			values = arguments[1];
			options = arguments[2];
			cb = arguments[3];
		} else {
			throw "Invalid number of arguments : table, values [, options], callback";
			return;
		}

		fixDateValue(values, function(fixedValues) {

			var data = pool.getConnection(function(err, conn) {
				if (err) {
					// conn.release();
					return cb(err);
				}

				var qry = "INSERT INTO ${tablename} SET ?";

				qry = qry.replace("${tablename}", table);

				conn.query(qry, fixedValues, function(err, result, fields) {
					conn.release();
					if (err) {
						return cb(err);
					} else {
						return cb(null, result, fields);
					}
				});
			});
		});

	};
	// arguments : tablename, values, [filter, [options] ], callback
	var updateData = function() {
		if (arguments.length < 3) {
			throw "Invalid number of arguments";
			return;
		}

		var table, values, filter, options, cb;

		if (arguments.length == 3) {
			table = arguments[0];
			values = arguments[1];
			cb = arguments[2];
		} else if (arguments.length == 4) {
			table = arguments[0];
			values = arguments[1];
			filter = arguments[2]
			cb = arguments[3];
		} else if (arguments.length == 5) {
			table = arguments[0];
			values = arguments[1];
			filter = arguments[2]
			options = arguments[3]
			cb = arguments[4];
		}

		fixDateValue(values.data, function(fixedValues) {

			var data = pool.getConnection(function(err, conn) {
				if (err) {
					// conn.release();
					return cb(err);
				}

				var qry = "UPDATE ${tablename} SET ${column_values} ${where}";

				qry = qry.replace("${tablename}", table).replace(
						"${column_values}", values.str);

				var dataArr = fixedValues;// values.data;

				if (filter) {
					var sWhere = "where " + filter.str;
					qry = qry.replace("${where}", sWhere);
					dataArr = dataArr.concat(filter.data);
				} else {
					qry = qry.replace("${where}", "");
				}
				conn.query(qry, dataArr, function(err, result, fields) {
					conn.release();
					if (err) {
						console.log(err);
						return cb(err);
					} else {
						return cb(null, result, fields);
					}
				});
			});
		});

	};

	var fixDateValue = function(dataObj, callback) {

		var datePattern = new RegExp(
				"([0-9]{0,4}-[0-9]{0,2}-[0-9]{0,2}T[0-9]{0,2}:[0-9]{0,2}:[0-9]{0,2}.[0-9]{0,3}Z)");

		for (key in dataObj) {

			var dataType = (typeof dataObj[key]);

			if (dataType == "string" && datePattern.test(dataObj[key])) {
				dataObj[key] = new Date(dataObj[key]);
			}
		}

		return callback(dataObj);
	};

	var callGetSP = function() {

		var argsLength = arguments.length;
		if (argsLength < 2) {
			throw "Invalid number of arguments";
			return;
		}

		var proc, params, cb;

		proc = arguments[0];
		params = (argsLength >= 3) ? arguments[1] : [];
		cb = arguments[argsLength - 1];

		var data = pool.getConnection(function(err, conn) {
			if (err) {
				// conn.release();
				return cb(err);
			}

			var qry = "call ${proc}(${params})";
			qry = qry.replace("${proc}", proc);
			if (params.length > 0) {
				var p = "";
				for (var i = 0; i < params.length; i++) {
					p += " ? " + (i < params.length - 1 ? "," : "");
				}
				qry = qry.replace("${params}", p);
			} else {
				qry = qry.replace("${params}", "");
			}

//			console.log(qry);

			conn.query(qry, (params.length > 0 ? params : []), function(err,
					results, fields) {

				conn.release();
				if (err) {
					console.log(err);
					return cb(err);
				} else {
					return cb(null, results[0], results[0].length);
				}

			});
		});
	};

	var getSPMultiple = function() {

		var argsLength = arguments.length;
		if (argsLength < 2) {
			throw "Invalid number of arguments";
			return;
		}

		var proc, params, cb;

		proc = arguments[0];
		params = (argsLength >= 3) ? arguments[1] : [];
		cb = arguments[argsLength - 1];

		var data = pool.getConnection(function(err, conn) {
			if (err) {
				// conn.release();
				return cb(err);
			}

			var qry = "call ${proc}(${params})";
			qry = qry.replace("${proc}", proc);
			if (params.length > 0) {
				var p = "";
				for (var i = 0; i < params.length; i++) {
					p += " ? " + (i < params.length - 1 ? "," : "");
				}
				qry = qry.replace("${params}", p);
			} else {
				qry = qry.replace("${params}", "");
			}

//			console.log(qry);

			conn.query(qry, (params.length > 0 ? params : []), function(err,
					results, fields) {

//				console.log(results);
				
				conn.release();
				if (err) {
					console.log(err);
					return cb(err);
				} else {
					return cb(null, results);
				}

			});
		});
	};

	var removeByUid = function(table, uid, cb) {

		var table, uid, cb;

		var data = pool.getConnection(function(err, conn) {
			if (err) {
				// conn.release();
				return cb(err);
			}

			var qry = "delete from ${tablename} where uid=?";

			qry = qry.replace("${tablename}", table);

			conn.query(qry, [uid], function(err, results, fields) {
				conn.release();
				if (err) {
					return cb(err);
				} else {
					return cb(null, results);
				}
			});
		});
	};

	return {
		get : getData,
		getByUid : getDataByUid,
		insert : insertData,
		update : updateData,
		removeByUid : removeByUid,
		getSP : callGetSP,
		getSPMultiple : getSPMultiple
	};
}());
