/**
 * http://usejsdoc.org/
 */

/*******************************************************************************
 * Method generates a new GUID
 */
module.exports = (function() {

	var guid = function() {
		var d = new Date().getTime();
		var format = "xxxxxxxxxxxx-4xxx-yxxx-xxxxxxxxxxxx";
		// var format = "xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx";

		var uuid = format.replace(/[xy]/g, function(c) {
			var r = (d + Math.random() * 16) % 16 | 0;
			d = Math.floor(d / 16);
			return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
		});
		return uuid;
	};
	
	return{
		guid : guid
	};
	
}());
