module.exports = (function() {
	var http = require("http");
	var env = require('../environment.json');

	var sendSms = function(to, text, callback) {
		var cfg = env.websms;
		
		var dataUrl = cfg.url;
		
		dataUrl = dataUrl.replace("${apikey}", cfg.apikey).replace("${sender}", cfg.sender).replace("${to}", to).replace("${text}", text);

		var req = http.get(dataUrl, function(response) {

			var buffer = "";

			response.on("data", function(chunk) {
				buffer += chunk;
			});

			response.on("end", function(err) {
				return callback(null, buffer);
			});
		});

		req.on('error', function(err) {
			console.log('problem with request: ' +err);
		});
	};

	

	return {
		sendSms : sendSms
	};

}());
