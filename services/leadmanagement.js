/**
 * http://usejsdoc.org/
 */

module.exports = (function() {
	var db = require("../services/db.js");

	var getLeads = function(utype, leadtype, filterData, searchFilter, options,
			callback) {
		var filter = {
			str : "",
			data : []
		};

		var count = 0;

		for ( var key in filterData) {
			if (count > 0) {
				filter.str += " and ";
			}
			filter.str += key + " = ? ";
			filter.data.push(filterData[key]);
			count++;
		}

		if (searchFilter) {
			for ( var key in searchFilter) {

				if (filter.str.length > 0) {
					filter.str += " and ";
				}

				if (typeof (searchFilter[key]) == 'boolean') {
					// boolean
					filter.str += key
							+ " = ?"
							+ ((searchFilter.length > (count + 1))
									? " and "
									: "");
					filter.data.push(searchFilter[key]);
				} else if (typeof (searchFilter[key]) == 'number') {
					// number
					filter.str += key
							+ " = ?"
							+ ((searchFilter.length > (count + 1))
									? " and "
									: "");
					filter.data.push(searchFilter[key]);
				} else {
					// string
					filter.str += key
							+ " like ?"
							+ ((searchFilter.length > (count + 1))
									? " and "
									: "");

					filter.data.push('%' + searchFilter[key] + '%');
				}
				count++;
			}
		}

		if (!options) {
			options = {
				limit : 999,
				offset : 0
			};
		}

		var viewName = "vw_openleads";
		if (leadtype == "open") {
			viewName = "vw_openleads";
		} else {
			viewName = "vw_pastleads";
		};

		viewName += (utype == 'sp') ? "_sp" : "";

		db.get(viewName, "*", filter, options,
				function(err, results, rowCount) {
					if (err) {
						console.log(err);
						callback(err, [], rowCount);
					} else {
						callback(null, results, rowCount);
					}
				});
	};

	var getLeadDetails = function(uid, callback) {

		db.getSP("get_leaddetails", [uid], function(err, results, rowCount) {
			if (err) {
				console.log(err);
				callback(err, [], rowCount);
			} else {
				callback(null, results, rowCount);
			}
		});

	};

	var insertLead = function(leadinfo, callback) {
		var insData = JSON.parse(JSON.stringify(leadinfo));

		insData.createdby = (leadinfo.newcreatedby || 'system');
		insData.modifiedby = (leadinfo.newmodifiedby || 'system');

		var leadsTbl = ["user_uid", "client_name", "client_org",
				"client_phone", "client_email", "state_uid", "city_uid",
				"other_state", "other_city", "category_uid", "subcategory_uid",
				"timeframe_uid", "ticket_value", "otherinfo",
				"assigned_to_uid", "status", "comments_verification",
				"comments_final", "createdby", "modifiedby"];

		for ( var key in insData) {
			var valid = false;

			for (var i = 0; i < leadsTbl.length; i++) {
				if (leadsTbl[i] == key) {
					valid = true;
					break;
				}
			}

			if (valid == false) {
				delete insData[key];
			}
		}

		db.insert("leads", insData, function(err, results) {
			return callback(err, results);
		});
	};

	var updateLead = function(leadinfo, callback) {
		var insData = JSON.parse(JSON.stringify(leadinfo));

		insData.modifiedby = (leadinfo.newmodifiedby || 'system');

		var leadsTbl = ["user_uid", "client_name", "client_org",
				"client_phone", "client_email", "state_uid", "city_uid",
				"other_state", "other_city", "category_uid", "subcategory_uid",
				"timeframe_uid", "ticket_value", "otherinfo",
				"assigned_to_uid", "status", "comments_verification", "apr_ticket_value",
				"comments_final", "modifiedby"];

		var colValues = {
			str : "",
			data : []
		};
		for ( var key in insData) {
			var valid = false;

			for (var i = 0; i < leadsTbl.length; i++) {
				if (leadsTbl[i] == key) {
					valid = true;
					break;
				}
			}

			if (valid == false) {
				delete insData[key];
			} else {
				colValues.str += (colValues.str.length > 0) ? ", " : "";
				colValues.str += key + " = ?";
				colValues.data.push(insData[key]);
			}
		}

		var filter = {
			str : "uid = ?",
			data : [leadinfo.uid]
		};

		db.update("leads", colValues, filter, function(err, results) {
			return callback(err, results);
		});
	}

	var getLeadServiceProviders = function(leaduid, spleaduid, callback) {

		db.getSPMultiple("get_leadserviceproviders", [leaduid, spleaduid],
				function(err, results, rowCount) {

					if (err) {
						console.log(err);
						callback(err, []);
					} else {
						callback(null, results);
					}
				});

	};

	var getSPLeadDetails = function(uid, callback) {

		db.getSP("get_spleaddetails", [uid], function(err, results, rowCount) {
			if (err) {
				console.log(err);
				callback(err, [], rowCount);
			} else {
				callback(null, results, rowCount);
			}
		});

	};

	var insertLeadServiceProvider = function(leadspinfo, callback) {
		var insData = JSON.parse(JSON.stringify(leadspinfo));

		insData.createdby = (leadspinfo.newcreatedby || 'system');
		insData.modifiedby = (leadspinfo.newmodifiedby || 'system');

		var spLeadsTbl = ["sp_uid", "lead_uid", "shared_on", "viewed_on",
				"acceptance_status", "accepted_on", "status", "comments",
				"apr_ticket_value", "active", "createdby", "modifiedby"];

		for ( var key in insData) {
			var valid = false;

			for (var i = 0; i < spLeadsTbl.length; i++) {
				if (spLeadsTbl[i] == key) {
					valid = true;
					break;
				}
			}

			if (valid == false) {
				delete insData[key];
			}
		}

		db.insert("sp_leads", insData, function(err, results) {
			return callback(err, results);
		});
	};

	var updateLeadServiceProvider = function(leadspinfo, callback) {
		var insData = JSON.parse(JSON.stringify(leadspinfo));

		insData.modifiedby = (leadspinfo.newmodifiedby || 'system');

		var spLeadsTbl = ["shared_on", "viewed_on", "acceptance_status", "accepted_on",
				"status", "comments", "apr_ticket_value", "active",
				"modifiedby"];

		var colValues = {
			str : "",
			data : []
		};
		for ( var key in insData) {
			var valid = false;

			for (var i = 0; i < spLeadsTbl.length; i++) {
				if (spLeadsTbl[i] == key) {
					valid = true;
					break;
				}
			}

			if (valid == false) {
				delete insData[key];
			} else {
				colValues.str += (colValues.str.length > 0) ? ", " : "";
				colValues.str += key + " = ?";
				colValues.data.push(insData[key]);
			}
		}

		var filter = {
			str : "uid = ?",
			data : [leadspinfo.uid]
		};

		db.update("sp_leads", colValues, filter, function(err, results) {
			return callback(err, results);
		});
	}

	var checkForLeadClosure = function(lead_uid, source, callback) {
		db.getSP("upd_leadclosure", [lead_uid, source], function(err, results) {
			return callback(err, results);
		});
	}

	return {
		getLeads : getLeads,
		getLeadDetails : getLeadDetails,
		insertLead : insertLead,
		updateLead : updateLead,
		getLeadServiceProviders : getLeadServiceProviders,
		getSPLeadDetails : getSPLeadDetails,
		insertLeadServiceProvider : insertLeadServiceProvider,
		updateLeadServiceProvider : updateLeadServiceProvider,
		checkForLeadClosure : checkForLeadClosure

	};
}());
