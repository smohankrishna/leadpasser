/**
 * http://usejsdoc.org/
 */

module.exports = (function() {
	var db = require("../services/db.js");
	var util = require("../services/utility.js");

	var getSPCatalog = function(sp_uid, options, callback) {
		var filter = {
			str : "sp_uid = ?",
			data : [sp_uid]
		};

		if (!options) {
			options = {
				limit : 999,
				offset : 0
			};
		}
		
		db.get("vw_spcatalog", "*", filter, options, function(err, results, rowCount) {
			if (err) {
				callback(err, [], rowCount);
			} else {
				callback(null, results, rowCount);
			}
		});
	};
	
	var insertCatalogItem = function(catalogItem, callback){
		
		if(catalogItem.uid){
			var err = "Invalid Call";
			var result = null;
			return callback(err, result);
		}else{
			var insData = JSON.parse(JSON.stringify(catalogItem));
			insData.createdby = (insData.newcreatedby || 'system');
			insData.modifiedby = (insData.newmodifiedby || 'system');
			
			delete insData.newcreatedby;
			delete insData.newmodifiedby;
			delete insData.uid;
			delete insData.createdon;
			delete insData.modifiedon;

			db.insert("sp_catalog", insData, function(err, results) {
				return callback(err, results);
			});
		}
	};

	var updateCatalogItem = function(catalogItem, callback) {

		var colValues = {
				str : "",
				data : []
		};
		
		for(var key in catalogItem){
			if(key == 'uid' || key == 'newcreatedby' || key == 'newmodifiedby' || 
					key == 'createdby' || key == 'createdon' ||
					key == 'modifiedon'){
				// DO NOTHING
			}else{
				colValues.str += (colValues.str.length > 0 )?", ": "";
				colValues.str += key + " = ?";
				if(key == "modifiedby"){
					colValues.data.push(catalogItem.newmodifiedby || 'system');
				}else{
					colValues.data.push(catalogItem[key]);
				}
			}
		}
		

		if (catalogItem.newmodifiedby) {
			delete catalogItem.newmodifiedby;
		}

		var filter = {
			str : "uid = ?",
			data : [catalogItem.uid]
		};
		db.update("sp_catalog", colValues, filter, function(err, results) {
			// console.log("UserSvc CLASS");
			// console.log(err);

			return callback(err, results);
		});
	}
	
	var getSPCatalogLocations = function(sp_catalog_uid, options, callback) {
		var filter = {
			str : "sp_catalog_uid = ?",
			data : [sp_catalog_uid]
		};

		if (!options) {
			options = {
				limit : 999,
				offset : 0
			};
		}
		
		db.get("vw_spcataloglocations", "*", filter, options, function(err, results, rowCount) {
			if (err) {
				callback(err, [], rowCount);
			} else {
				callback(null, results, rowCount);
			}
		});
	};
	var insertSPCatalogLocation = function(locationData, callback){
		
		if(locationData.uid){
			var err = "Invalid Call";
			var result = null;
			return callback(err, result);
		}else{
			var insData = JSON.parse(JSON.stringify(locationData));
			insData.createdby = (insData.newcreatedby || 'system');
			insData.modifiedby = (insData.newmodifiedby || 'system');
			
			delete insData.newcreatedby;
			delete insData.newmodifiedby;
			delete insData.uid;
			delete insData.createdon;
			delete insData.modifiedon;

			db.insert("sp_catalog_locations", insData, function(err, results) {
				return callback(err, results);
			});
		}
	};
	
	var removeSPCatalogLocation = function(uid, callback){
		if(!uid){
			var err = "Invalid Call";
			var result = null;
			return callback(err, result);
		}else{
			db.removeByUid("sp_catalog_locations", uid, function(err, results) {
				return callback(err, results);
			});
		}
	};

	return {
		getSPCatalog : getSPCatalog,
		insertCatalogItem: insertCatalogItem,
		updateCatalogItem : updateCatalogItem,
		getSPCatalogLocations : getSPCatalogLocations,
		insertSPCatalogLocation : insertSPCatalogLocation,
		removeSPCatalogLocation :removeSPCatalogLocation
	};
}());
