module.exports = (function () {
    var nodemailer = require('nodemailer');
    var env = require('../environment.json');

    var smtpConfig = {
        host: process.env.APP_EMAIL_HOST || env.email.host,
        port: process.env.APP_EMAIL_PORT || env.email.port,
        secure: process.env.APP_EMAIL_SECURE || env.email.secure, // use SSL
        auth: {
            user: process.env.APP_EMAIL_USER || env.email.username,
            pass: process.env.APP_EMAIL_PWD || env.email.passwd
        }
    };

    var sendMail = function (toList, ccList, bccList, subject, text, html, callback) {
        var transporter = nodemailer.createTransport(smtpConfig);

        var mailOptions = {
            from: process.env.APP_EMAIL_FROM || env.email.from, // sender address
            to: toList,
            cc: ccList,
            bcc: bccList,
            subject: subject, // Subject line
            text: text,
            html: html
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                return callback(error)
            }
//		    console.log('Message sent: ' + info.response);

            return callback("", info);
        });

    };

    return {
        sendMail: sendMail
    };

}());
