/**
 * http://usejsdoc.org/
 */

module.exports = (function () {
    var db = require("../services/db.js");
    var jwt = require('jsonwebtoken');
    const env = require('../environment.json');
    var date = require('date-and-time');

    var isOTPExpired = function(userinfo) {
        if(userinfo.mcode !=null && userinfo.mcodevadility !=null){
            return date.subtract(new Date(), userinfo.mcodevadility).toMinutes();

        }

        return false;
    };

    var authenticate = function (mobile,username, password, otp , callback) {
        var prefix,username_data,password_data;
        if(mobile && password) {
             prefix = "mobilenum= ? and upass=?";
             username_data=mobile;
             password_data =password;
        }else if(mobile && otp){
            prefix = "mobilenum = ? and mcode=?";
            username_data=mobile;
            password_data =otp;
        }
        else {
            prefix = "uname = ? and upass=?";
            username_data = username;
            password_data =password;
        }

        var filter = {
            str: prefix+" and active=?",
            data: [username_data, password_data, true]
        };
        console.log(filter);
        var usrErr,errorMessage;
        db.get("users", "*", filter, function (err, results, rowCount) {
            if (err) {
                callback(err, false, {});
            } else {
                if (results && results.length === 1) {

                    var obj = {
                        uinfo: results[0]
                    };

                    if (filter.str.includes("mcode")) {
                        if(date.subtract(new Date(results[0].mcodevalidity),new Date()).toMinutes() < 20) {

                            results[0].mcode = null;
                            results[0].mcodevalidity = null;
                            updateUserDetails(results[0], function (err, results) {
                                if (results && results.length === 1) {
                                    obj.uinfo = results[0]
                                }
                            });
                        }else {
                            usrErr = {
                                code: "AUTHORIZATION_ERROR",
                                message: "OTP Expired"
                            };
                            return callback(usrErr, false, {});

                        }
                    }

                    var token = jwt.sign(obj, env.api.secret, {
                        expiresIn: env.api.tokenExpiry
                    });

                    callback(null, true, results[0], token);
                } else {
                    if(otp){
                        errorMessage = "Invalid OTP or OTP Expired";
                    }else{
                        errorMessage ="Username / Password invalid";
                    }
                    usrErr = {
                        code: "AUTHORIZATION_ERROR",
                        message: errorMessage
                    };
                    callback(usrErr, false, {});
                }
            }
        });
    };

    var getUserListByFilter = function (filterData, searchFilter, options,
            callback) {
        var filter = {
            str: "",
            data: []
        };

        var count = 0;
        for (var key in filterData) {
            filter.str += key + "= ?"
                    + ((filterData.length > (count + 1)) ? " and " : "");
            filter.data.push(filterData[key]);
            count++;
        }

        if (searchFilter) {
            for (var key in searchFilter) {

                if (filter.str.length > 0) {
                    filter.str += " and ";
                }

                if (typeof (searchFilter[key]) == 'boolean') {
                    // boolean
                    filter.str += key
                            + " = ?"
                            + ((searchFilter.length > (count + 1))
                                    ? " and "
                                    : "");
                    filter.data.push(searchFilter[key]);
                } else if (typeof (searchFilter[key]) == 'number') {
                    // number
                    filter.str += key
                            + " = ?"
                            + ((searchFilter.length > (count + 1))
                                    ? " and "
                                    : "");
                    filter.data.push(searchFilter[key]);
                } else {
                    // string
                    filter.str += key
                            + " like ?"
                            + ((searchFilter.length > (count + 1))
                                    ? " and "
                                    : "");

                    filter.data.push('%' + searchFilter[key] + '%');
                }
                count++;
            }
        }

        filter.str = "enable_display = ? "
                + ((filter.str.length > 0) ? " and " : "") + filter.str;
        filter.data.unshift(true);

        if (!options) {
            options = {
                limit: 999,
                offset: 0
            };
        }

        db.get("users", "*", filter, options, function (err, results, rowCount) {
            if (err) {
                callback(err, [], rowCount);
            } else {
                callback(null, results, rowCount);
            }
        });
    };

    var getUserDetails = function (uid, callback) {
        db.getByUid("users", uid, function (err, results, rowCount) {
            if (err) {
                callback(err, []);
            } else {
                callback(null, results);
            }
        });
    };

    var getUserDetailsByUname = function (uname, callback) {
//		console.log(uname);
        var filter = {
            str: "uname = ?",
            data: [uname]
        };
        db.get("users", "*", filter, function (err, results, rowCount) {
            if (err) {
                callback(err, []);
            } else {
                callback(null, results);
            }
        });
    };
    var getUserDetailsByMobileNum = function (mobilenum, callback) {
		console.log(mobilenum);
        var filter = {
            str: "mobilenum = ?",
            data: [mobilenum]
        };
        db.get("users", "*", filter, function (err, results, rowCount) {
            if (err) {
                callback(err, []);
            } else {
                console.log("----- > "+rowCount);
                callback(null, results,rowCount);
            }
        });
    };

    var checkForUsername = function (uname, callback) {
        var filter = {
            str: "uname = ?",
            data: [uname]
        };

        db.get("users", "*", filter, function (err, results, rowCount) {
            // console.log(results);
            if (err) {
                callback(err, []);
            } else {
                callback(null, rowCount);
            }
        });
    };

    var insertUserDetails = function (uinfo, callback) {

        var insData = JSON.parse(JSON.stringify(uinfo));

        insData.createdby = (uinfo.newcreatedby || 'system');
        insData.modifiedby = (uinfo.newmodifiedby || 'system');

        delete insData.newcreatedby;
        delete insData.newmodifiedby;

        delete insData.uid;
        delete insData.createdon;
        delete insData.modifiedon;

        db.insert("users", insData, function (err, results) {
            return callback(err, results);
        });
    }

    

    var updateUserDetails = function (uinfo, callback) {

        var colValues = {
            str: "",
            data: []
        };

        for (var key in uinfo) {
            if (key == 'uid' || key == 'newcreatedby' || key == 'newmodifiedby' ||
                    key == 'createdby' || key == 'createdon' ||
                    key == 'modifiedon') {
                // DO NOTHING
            } else {
                colValues.str += (colValues.str.length > 0) ? ", " : "";
                colValues.str += key + " = ?";
                if (key == "modifiedby") {
                    colValues.data.push(uinfo.newmodifiedby || 'system');
                } else {
                    colValues.data.push(uinfo[key]);
                }
            }
        }


//		if (uinfo.newmodifiedby) {
//			delete uinfo.newmodifiedby;
//		}
        console.log(colValues);
        var filter = {
            str: "uid = ?",
            data: [uinfo.uid]
        };
        db.update("users", colValues, filter, function (err, results) {
            // console.log("UserSvc CLASS");
            // console.log(err);

            return callback(err, results);
        });
    }

    var validateOTP = function (uname, mobilenum, userOTP, callback) {
        if (mobilenum) {
            getUserDetailsByMobileNum(mobilenum,function (err,result) {
                if (result && result.length == 1) {
                    if (uinfo.mcode === userOTP) {
                        return callback(true);
                    } else {
                        return callback(false);
                    }
                } else {
                    return callback(false);
                }
            });
        }
        else {
            getUserDetailsByUname(uname, function (err, result) {
                if (result && result.length == 1) {
                    if (uinfo.mcode === userOTP) {
                        return callback(true);
                    } else {
                        return callback(false);
                    }
                } else {
                    return callback(false);
                }
            });
        }
    };

    var getUserBankDetails = function (user_uid, callback) {
        var filter = {
            str: "user_uid = ?",
            data: [user_uid]
        };
        db.get("lg_bank_details", "*", filter, function (err, results, rowCount) {
            if (err) {
                callback(err, []);
            } else {
                callback(null, results);
            }
        });
    };

    var insertUserBankDetails = function (details, callback) {

        var insData = JSON.parse(JSON.stringify(details));

        insData.createdby = (details.newcreatedby || 'system');
        insData.modifiedby = (details.newmodifiedby || 'system');

        delete insData.newcreatedby;
        delete insData.newmodifiedby;

        delete insData.uid;
        delete insData.createdon;
        delete insData.modifiedon;

        db.insert("lg_bank_details", insData, function (err, results) {
            return callback(err, results);
        });
    }

    var updateUserBankDetails = function (details, callback) {

        var colValues = {
            str: "",
            data: []
        };

        for (var key in details) {
            if (key == 'uid' || key == 'newcreatedby' || key == 'newmodifiedby' ||
                    key == 'createdby' || key == 'createdon' ||
                    key == 'modifiedon') {
                // DO NOTHING
            } else {
                colValues.str += (colValues.str.length > 0) ? ", " : "";
                colValues.str += key + " = ?";
                if (key == "modifiedby") {
                    colValues.data.push(details.newmodifiedby || 'system');
                } else {
                    colValues.data.push(details[key]);
                }
            }
        }

        var filter = {
            str: "uid = ?",
            data: [details.uid]
        };
        db.update("lg_bank_details", colValues, filter, function (err, results) {
            // console.log("UserSvc CLASS");
            // console.log(err);

            return callback(err, results);
        });
    }

    return {
        authenticate: authenticate,

        getUserListByFilter: getUserListByFilter,

        getUserDetails: getUserDetails,
        getUserDetailsByUname: getUserDetailsByUname,
        getUserDetailsByMobileNum:getUserDetailsByMobileNum,
        insertUserDetails: insertUserDetails,
        updateUserDetails: updateUserDetails,

        checkForUsername: checkForUsername,

        getUserBankDetails: getUserBankDetails,
        insertUserBankDetails: insertUserBankDetails,
        updateUserBankDetails: updateUserBankDetails,

        validateOTP: validateOTP
    };
}());
