/**
 * http://usejsdoc.org/
 */

module.exports = (function() {
	var db = require("../services/db.js");
	var util = require("../services/utility.js");

	var getMasterlists = function(filter, callback) {

		db.getSP("get_masterlist", [filter.parent_uid], function(err, results) {
			if (err) {
				callback(err, []);
			} else {
				callback(null, results);
			}
		});
	};
	
	var getMasterlistsByCode = function(parent_code, callback){
		
		db.getSP("get_masterlistbycode",[parent_code] ,function(err, results) {
			if (err) {
				callback(err, []);
			} else {
				callback(null, results);
			}
		});
	}
	
	var getCustomlists = function(spName, callback) {

		db.getSPMultiple(spName, function(err, results) {
			if (err) {
				callback(err, []);
			} else {
				callback(null, results);
			}
		});
	};
	
	var getDetailsByUid = function(uid, callback) {
		var filter = {
			str : "uid = ?",
			data : [uid]
		};

		db.get("masterlist", "*", filter, function(err, results,
				rowCount) {
			if (err) {
				callback(err, [], rowCount);
			} else {
				callback(null, results, rowCount);
			}
		});
	};

	var getDetailsByCode = function(code, parent_uid, callback) {
		var filter = {
			str : "code = ? and IFNULL(parent_uid,'') = IFNULL(?,'')",
			data : [code, parent_uid]
		};

		db.get("masterlist", "*", filter, function(err, results,
				rowCount) {
			if (err) {
				callback(err, [], rowCount);
			} else {
				callback(null, results, rowCount);
			}
		});
	};

	var checkForListCode = function(code, parent_uid, callback) {
		var filter = {
			str : "code = ? and parent_uid = ?",
			data : [code, parent_uid]
		};

		db.get("masterlist", "*", filter, function(err, results, rowCount) {
			// console.log(results);
			if (err) {
				callback(err, []);
			} else {
				callback(null, rowCount);
			}
		});
	};

	var insertList = function(pInfo, callback) {

		var insData = JSON.parse(JSON.stringify(pInfo));
		
//		console.log(insData);
		
		insData.createdby = (pInfo.newcreatedby || 'system');
		insData.modifiedby = (pInfo.newmodifiedby || 'system');

		delete insData.newcreatedby;
		delete insData.newmodifiedby;
		delete insData.uid;
		delete insData.createdon;
		delete insData.modifiedon;

		db.insert("masterlist", insData, function(err, results) {
			return callback(err, results);
		});
	}

	var updateList = function(mlInfo, callback) {
		
//		console.log(mlInfo);
		
		var colValues = {
				str : "",
				data : []
		};
		
		for(var key in mlInfo){
			if(key == 'uid' || key == 'newcreatedby' || key == 'newmodifiedby' || 
					key == 'createdby' || key == 'createdon' ||
					key == 'modifiedon'){
				// DO NOTHING
			}else{
				colValues.str += (colValues.str.length > 0 )?", ": "";
				colValues.str += key + " = ?";
				if(key == "modifiedby"){
					colValues.data.push(mlInfo.newmodifiedby || 'system');
				}else{
					colValues.data.push(mlInfo[key]);
				}
			}
		}
		
//		console.log(colValues);
		

		if (mlInfo.newmodifiedby) {
			delete mlInfo.newmodifiedby;
		}

		var filter = {
			str : "uid = ?",
			data : [mlInfo.uid]
		};
		db.update("masterlist", colValues, filter, function(err, results) {
			// console.log("UserSvc CLASS");
//			 console.log(err);

			return callback(err, results);
		});
	}

	return {
		getMasterlists : getMasterlists,
		getMasterlistsByCode :getMasterlistsByCode,
		getCustomlists : getCustomlists,
		getDetailsByUid : getDetailsByUid,
		getDetailsByCode : getDetailsByCode,
		checkForListCode : checkForListCode,
		insertList : insertList,
		updateList : updateList
	};
}());
