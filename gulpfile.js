var gulp = require('gulp');
var pump = require('pump');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var argv = require('yargs').argv;
var zip = require('gulp-zip');
var clean = require('gulp-clean');
const uuidV1 = require('uuid/v1');
var replace = require('gulp-replace');

// ************************************************
var dist = "dist/";
var publishBuild = dist + "/app/web";
var appjs = 'leadpassers.min.js';

var buildEnv = "dev";
var secret = uuidV1();
if (argv.env === "demo" || argv.env === "dev" || argv.env === "prod" || argv.env === "qa") {
    buildEnv = argv.env;
} else {
    console.log("Invalid 'env'. Build proceeding with 'dev' environment");
    console.log("USAGE : gulp --env=[ dev | demo | prod ]");
    buildEnv = "dev";
}
var outputFile = "leadpassers." + buildEnv + ".zip";
gulp.task('copy-src', function (cb) {
    pump([gulp.src(["./**/*.*",
            "!./nbproject/**/*.*",
            "!./environment.json",
            "!./*.md",
            "!./.*",
            "!./gulpfile*.js",
            "!./yarn*.*",
            "!./build/**/*.*",
            "!./data/**/*.*",
            "!./node_modules/**/*.*",
            "!./public/lib/**/*.*",
            "!./public/index.html",
            "!./uploadeddocs/**/*.*",
            "!./dist/**/*.*"
        ]), gulp.dest(publishBuild)], cb);
});
gulp.task('minify-lib', ['copy-src'], function (cb) {
    pump([gulp.src("./public/lib/**/*.js"),
        concat(appjs),
        replace('SECRET_SALT_KEY', secret),
        uglify(),
        gulp.dest(publishBuild + "/public")],
            cb);
});

gulp.task('make-sql', ['minify-lib'], function (cb) {
    pump([gulp.src("./data/sql/**/*.sql"),
        concat("db.sql"),
        gulp.dest(publishBuild)],
            cb);
});

gulp.task('ren-index', ['make-sql'], function (cb) {
    pump([gulp.src(publishBuild + "/public/index_deploy.html"),
        rename('index.html'), gulp.dest(publishBuild + "/public")], cb);
});

gulp.task('clean-index', ['ren-index'], function (cb) {
    pump([gulp.src(publishBuild + "/public/index_deploy.html"), clean({
            force: true
        })], cb);
});

gulp.task('copy-envjson', ['clean-index'], function (cb) {
    pump([gulp.src("./build/environment." + buildEnv + ".json"),
        rename('environment.json'),
        replace('##secret', secret),
        gulp.dest(publishBuild)], cb);
});

gulp.task('copy-docker-files', ['copy-envjson'], function (cb) {
    pump([gulp.src("./build/Dockerfile.web"), gulp.dest(dist + "/app")], cb);
});


gulp.task('make-zip', ['copy-docker-files'], function (cb) {
    pump([gulp.src(dist + "/app/**/*.*"), zip(outputFile),
        gulp.dest(dist)], cb);
});

gulp.task('clean-src', ['make-zip'], function (cb) {
    pump([gulp.src(dist + "/app"), clean({
            force: true
        })], cb);
});

gulp.task('default', ['clean-src']);