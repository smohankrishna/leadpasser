var express = require('express');
var serveStatic = require('serve-static');
var bodyParser = require('body-parser');

var env = require("./environment.json")

var app = express();

var httpPort = process.env.PORT || env.web.port || 5000;

var http = require('http');
var webapi = require('./routes/web-api.js');
var userapi = require('./routes/user-api.js');
var masterlistapi = require('./routes/masterlist-api.js');
var catalogapi = require('./routes/catalog-api.js');
var locationapi = require('./routes/locations-api.js');
var spapi = require('./routes/sp-api.js');
var docapi = require('./routes/doc-api.js');

var leadmgmtapi = require('./routes/leadmgmt-api.js');
var serverApp = http.Server(app);

app.use(serveStatic('public', {
    'index': ['index.html', 'index.htm']
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept, webauthkey");
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS');
    res.setHeader('Content-Type', 'application/json');
    next();
});

app.use('/api/web', webapi.route);
app.use('/api/web/user', userapi.route);
app.use('/api/web/masterlist', masterlistapi.route);
app.use('/api/web/catalog', catalogapi.route);
app.use('/api/web/locations', locationapi.route);

app.use('/api/web/serviceprovider', spapi.route);
app.use('/api/web/documents', docapi.route);

app.use('/api/web/leadmgmt', leadmgmtapi.route);

/*******************************************************************************
 * Read Environment Settings
 ******************************************************************************/
app.route("/web/environment").get(function (req, res, next) {
    if (req.method != 'OPTIONS' && !req.headers['webauthkey']) {
        res.status(401).send({
            "error": "UnAuthorized"
        });
        return;
    }
    res.status(200).send("");
});

/*******************************************************************************
 * HTTP Server Start
 ******************************************************************************/

serverApp.listen(httpPort);
console.log('Server listening at ' + httpPort);

module.export = app;
