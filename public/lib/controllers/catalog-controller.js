lpApp.controller('catalogCtlr', CatalogCtlr);

CatalogCtlr.$inject = ['$scope', '$routeParams', '$route', '$location',
    '$window', 'utils', 'apiService', 'emailService', 'validationService'];

function CatalogCtlr($scope, $routeParams, $route, $location, $window, utils,
        apiService, emailService, validationService) {

    var PAGE_LIMIT = 10;
    var API_URL = utils.getSession("apiUrl");

    $scope.loadCatalog = function () {

        if (!utils.isActiveUser()) {
            return;
        }


        $scope.bcArray = [];
        $scope.bcArray.push({
            title: "Config",
            url: ""
        });
        $scope.bcArray.push({
            title: "Product Catalog",
            url: ""
        });

        $scope.loading = true;

        var category_uid = $location.search().category;

        try {

            apiService.getCall(API_URL + "/catalog", {}).then(function (response) {
                if (response.data) {
                    $scope.catalog = response.data.details;
                    apiService.getCall(API_URL + "/catalog/categories", {}).then(
                            function (response) {
                                if (!response.data) {
                                    throw "UNKNOWN ERROR";
                                }

                                if (response.data.error) {
                                    throw response.data.error;
                                } else {
                                    $scope.categories = response.data.list;
                                    $scope.loading = false;

                                    $scope.category_uid = category_uid;

                                    $scope
                                            .loadProducts(category_uid);

                                }
                            }, function (errResponse) {
                        throw errResponse;
                    });
                } else {
                    $window
                            .alert("Error loading Product Catalog. Try after sometime.");
                    return;
                }
            }, function (err) {
                $scope.loading = false;
                if (err.status === 403) {
                    utils.logoff();
                } else {
                    console.log(err);
                }
            });
        } catch (err) {
            $scope.loading = false;
            console.log(err);
            $window.alert(err);
        }
    };

    $scope.loadProducts = function (category_uid) {

        if (!utils.isActiveUser()) {
            return;
        }
        $location.search("category", category_uid);

        if (!category_uid) {
            $scope.lstProducts = null;
            return;
        }

        $scope.loading = true;

        var params = {};

        try {
            apiService.getCall(API_URL + "/catalog/products/" + category_uid,
                    params).then(function (response) {
                if (!response.data) {
                    throw "UNKNOWN ERROR";
                }

                if (response.data.error) {
                    throw response.data.error;
                } else {
                    $scope.lstProducts = response.data.list;
                    var rowCount = response.data.rowCount;
                    $scope.loading = false;
                }
            }, function (errResponse) {
                throw errResponse;
            });
        } catch (err) {
            $scope.loading = false;
            console.log(err);
            $window.alert(err);
        }
    };

    $scope.changePage = function (pgnum) {
        if (!utils.isActiveUser()) {
            return;
        }

        $location.search("pgnum", pgnum);
        $scope.loadProperties(pgnum);
    };

    $scope.addList = function (category_uid, type) {
        if (!utils.isActiveUser()) {
            return;
        }

        var list = {
            uid: null,
            code: null,
            description: null,
            parent_uid: category_uid,
            seq: 999,
            active: 1,
            createdby: null,
            createdon: null,
            modifiedby: null,
            modifiedon: null,
            txtfield_1: 'Fixed',
            numfield_1: null,
            txtfield_2: 'Fixed',
            numfield_2: null
        }

        var obj = {
            list: list,
            mode: 'add'
        };

        if (type === 'subcategory') {
            utils.showPopup("./views/catalog/popup-subcategory-edit.html",
                    obj, "", "catalogCtlr");
        } else {
            utils.showPopup("./views/catalog/popup-catalog-edit.html", obj,
                    "", "catalogCtlr");
        }
    };

    $scope.editList = function (list, type) {
        if (!utils.isActiveUser()) {
            return;
        }

        var obj = {
            list: angular.copy(list),
            mode: 'edit'
        };
        if (type === 'subcategory') {
            utils.showPopup("./views/catalog/popup-subcategory-edit.html",
                    obj, "", "catalogCtlr");
        } else {
            utils.showPopup("./views/catalog/popup-catalog-edit.html", obj,
                    "", "catalogCtlr");
        }
    };

    $scope.saveList = function (dialogData) {
        var list = dialogData.list;

        if (!utils.isActiveUser()) {
            return;
        }

        // TODO : VALIDATE INFORMATION

        if (!list.code || !list.description) {
            $window.alert("Required information is missing");
            return;
        }

        $scope.eventinprogress = true;
        $scope.eventmessage = "saving";
        validationService.checkForListCode(list.code, list.parent_uid).then(function (response) {
            if (!list.uid && response.data.error) {
                $window
                        .alert("Error validating Name. Try again after sometime");
                $scope.eventinprogress = false;
                return;
            } else if (!list.uid
                    && response.data.result === false) {
                $window
                        .alert("Code already taken. Try some other code");
                $scope.eventinprogress = false;
                return;
            } else {

                // DO WEB API CALL TO SAVE RECORD
                var currentUser = utils.getSession("uinfo");
                try {
                    list.newcreatedby = currentUser.uname;
                    list.newmodifiedby = currentUser.uname;

                    var postData = {
                        mlinfo: list
                    };

                    var apiUrl = API_URL + "/catalog/details/";

                    apiUrl += (list.uid) ? "update" : "insert";

                    apiService.postCall(apiUrl, postData).then(
                            function (response) {
                                if (!response.data) {
                                    throw "UNKNOWN ERROR";
                                } else if (response.data.error) {
                                    throw response.data.error;
                                } else {
                                    $scope.eventinprogress = false;
                                    $scope
                                            .closeThisDialog(dialogData.ngDialogId);
                                    $route.reload();
                                }
                            }, function (errResponse) {
                        throw errResponse;
                    });
                } catch (err) {
                    $window
                            .alert("Unable to Save List. Please try again later.");
                    console.log(err);
                    $scope.eventinprogress = false;
                }

            }
        }, function (errResponse) {
            $window
                    .alert("Unable to validate List Name. Try again after sometime");
            $scope.eventinprogress = false;
            return;
        });
    };

}
