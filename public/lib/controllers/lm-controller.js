lpApp.controller('leadmgmtCtlr', LeadMgmtCtlr);

LeadMgmtCtlr.$inject = ['$scope', '$routeParams', '$route', '$location',
		'$window', '$interval', '$sce', 'utils', 'apiService', 'emailService',
		'commonDataService', 'leadmgmtService'];

function LeadMgmtCtlr($scope, $routeParams, $route, $location, $window,
		$interval, $sce, utils, apiService, emailService, commonDataService,
		leadmgmtService) {

	var PAGE_LIMIT = 10;

	$scope.all_status = [{
		name : "Pending Verification",
		type : "open"
	}, {
		name : "Verified",
		type : "open"
	}, {
		name : "In Progress",
		type : "open"
	// }, {
	// name : "Verified",
	// type : "inprogress"
	// }, {
	// name : "In Progress",
	// type : "inprogress"
	}, {
		name : "Withdrawn",
		type : "past"
	}, {
		name : "Declined",
		type : "past"
	}, {
		name : "Verification Failed",
		type : "past"
	}, {
		name : "Terminated",
		type : "past"
	}, {
		name : "Completed",
		type : "past"
	}, {
		name : "Invalid Lead",
		type : "past"
	}, {
		name : "Rejected By All",
		type : "past"
	}, {
		name : "Verified",
		label : "Successful",
		type : "verification"
	}, {
		name : "Verification Failed",
		label : "Failed",
		type : "verification"
	}, {
		name : "Invalid Lead",
		label : "Invalid Lead",
		type : "verification"
	}, {
		name : "Withdrawn",
		label : "Withdrawn",
		type : "verification"
	}, {
		name : "Completed",
		label : "Approved",
		type : "final"
	}, {
		name : "Declined",
		label : "Declined",
		type : "final"
	}, {
		name : "Rejected By All",
		label : "Rejected By All",
		type : "final"
	}, {
		name : "Terminated",
		label : "Terminated",
		type : "final"
	}];

	$scope.sp_lead_status = [{
		name : "Yet To View",
		label : "Yet To View",
		type : "acceptance"
	}, {
		name : "Viewed",
		label : "Viewed",
		type : "acceptance"
	}, {
		name : "Accepted",
		label : "Accepted",
		type : "acceptance"
	}, {
		name : "Rejected",
		label : "Rejected",
		type : "acceptance"
	}, {
		name : "New",
		label : "New",
		type : "leadstatus"
	}, {
		name : "In Progress",
		label : "In Progress",
		type : "leadstatus"
	}, {
		name : "Approved",
		label : "Approved",
		type : "leadstatus"
	}, {
		name : "Declined",
		label : "Declined",
		type : "leadstatus"
	}, {
		name : "Terminated",
		label : "Terminated",
		type : "leadstatus"
	}, {
		name : "Yet To View",
		label : "Yet To View",
		type : "sp_new"
	}, {
		name : "Viewed",
		label : "Viewed",
		type : "sp_new"
	}, {
		name : "Approved",
		label : "Approved",
		type : "sp_past"
	}, {
		name : "Declined",
		label : "Declined",
		type : "sp_past"
	}, {
		name : "Terminated",
		label : "Terminated",
		type : "sp_past"
	}, {
		name : "General",
		label : "General Comments",
		type : "sp_comments"
	}, {
		name : "Approved",
		label : "Approved This Request",
		type : "sp_comments"
	}, {
		name : "Declined",
		label : "Decline This Request",
		type : "sp_comments"
	}, {
		name : "Terminated",
		label : "Terminate This Request",
		type : "sp_comments"
	}]

	$scope.init = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var currentUser = utils.getSession("uinfo");
		$scope.user = currentUser;
		$scope.utype = currentUser.utype.toLowerCase();

		$scope.bcArray = [];
		$scope.bcArray.push({
			title : "Lead Management",
			url : ""
		});
		$scope.bcArray.push({
			title : "Leads",
			url : ""
		});
	};

	$scope.setDefaultTab = function(tabname) {
		var qry = $location.search();

		if (qry && qry.tab) {
			$scope.selectedTab = qry.tab;
		} else {
			$location.search("tab", tabname);
			$scope.selectedTab = tabname;
		}
	};

	$scope.loadLeads = function(leadtype) {

		$scope.loading = true;

		var pgnum = Number($location.search().pgnum);
		var searchFilter = $location.search().search;

		var currentUser = utils.getSession("uinfo");
		$scope.user = currentUser;
		$scope.utype = currentUser.utype.toLowerCase();
		$scope.leadtype = leadtype;

		if (!pgnum) {
			pgnum = 1;
			$location.search("pgnum", pgnum);
		}
		var options = {
			limit : PAGE_LIMIT,
			offset : (pgnum - 1) * PAGE_LIMIT
		};

		if (searchFilter) {
			searchFilter = JSON.parse(searchFilter);

			$scope.filter = {};
			for (key in searchFilter) {
				$scope.filter[key] = searchFilter[key];
			}

		}

		var filter = {};

		if ($scope.utype == 'lg') {
			filter.user_uid = currentUser.uid;

			if (leadtype == 'draft') {
				filter.status = 'New';
				leadtype = 'open';
			}

		} else if ($scope.utype == 'sp') {
			if (leadtype == 'new') {
				filter.status = 'New';
				filter.sp_uid = currentUser.uid
				leadtype = 'open';
			} else if (leadtype == 'inprocess') {
				filter.acceptance_status = 'accepted';
				filter.sp_uid = currentUser.uid
				leadtype = 'open';
			} else {
				filter.sp_uid = currentUser.uid
				leadtype = 'past';
			}

		} else if ($scope.utype == 'su') {
			if (leadtype == 'pending') {
				filter.status = 'Pending Verification';
				leadtype = 'open';
			} else if (leadtype == 'verified') {
				filter.status = 'Verified';
				leadtype = 'open';
			} else if (leadtype == 'inprogress') {
				filter.status = 'In Progress';
				leadtype = 'open';
			}
		}

		leadmgmtService.getLeads($scope.utype, leadtype, filter, searchFilter,
				options).then(function(response) {
			$scope.leadslst = response.list;

			var rowCount = response.rowCount;

			var prevPage = null;
			var nextPage = null;

			if (rowCount > PAGE_LIMIT) {
				prevPage = (pgnum > 1) ? pgnum - 1 : null;
				var rectillthispage = pgnum * PAGE_LIMIT;

				if (rectillthispage < rowCount) {
					nextPage = pgnum + 1;
				}
			}

			$scope.prevPage = prevPage;
			$scope.nextPage = nextPage;
			$scope.pgnum = pgnum;
			$scope.totalpages = Math.ceil(rowCount / PAGE_LIMIT);

			$scope.loading = false;
		}, function(err) {
			$scope.leadslst = [];
			console.log(err);
			$scope.loading = false;
		});
	};

	$scope.searchLeads = function(filter) {

		for (key in filter) {
			if (!filter[key]) {
				delete filter[key];
			}
		}

		if (!filter || Object.keys(filter).length == 0) {
			$location.search("search", null);
		} else {
			$location.search("search", JSON.stringify(filter));
		}

		$location.search("pgnum", 1);
		$scope.loadLeads($scope.leadtype);
	};

	$scope.clearSearch = function(filter) {

		if (filter && filter.client_name)
			filter.client_name = "";
		if (filter && filter.client_phone)
			filter.client_phone = "";
		if (filter && filter.category_uid)
			filter.category_uid = null;
		if (filter && filter.subcategory_uid)
			filter.subcategory_uid = null;
		if (filter && filter.status)
			filter.status = "";
		if (filter && filter.sp_approvals)
			filter.sp_approvals = "";

		$location.search("search", null);
		$location.search("pgnum", 1);
		$scope.loadLeads($scope.leadtype);
	};

	$scope.createNewLead = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var uinfo = utils.getSession("uinfo");
		var lead = {
			user_uid : uinfo.uid,
			status : 'New'
		};
		var obj = {
			lead : lead
		};
		utils.showPopup("./views/leadmgmt/popup-new-lead.html", obj, "",
				"leadmgmtCtlr");
	};

	$scope.editLead = function(lead, leadtype) {
		if (!utils.isActiveUser()) {
			return;
		}
		var uinfo = utils.getSession("uinfo");

		var obj = {
			lead : angular.copy(lead)
		};

		var popupUrl = "./views/leadmgmt/popup-new-lead.html";

		if (lead.status != 'New') {
			popupUrl = "./views/leadmgmt/popup-view-lead.html";
		}

		utils.showPopup(popupUrl, obj, "", "leadmgmtCtlr");
	}

	$scope.editLeadDetail = function(lead, leadtype) {
		utils.setSession("urlSrc", $location.url());
		$location.url("/su/leadmgmt/details/" + lead.uid);
	};

	$scope.initDetails = function() {
		$scope.bcArray = [];
		$scope.bcArray.push({
			title : "Lead Management",
			url : ""
		});
		$scope.bcArray.push({
			title : "Leads",
			url : utils.getSession("urlSrc")
		});

		$scope.bcArray.push({
			title : "Details",
			url : ""
		});

		var qry = $location.search();

		if (qry && qry.tab) {
			$scope.selectedTab = qry.tab;
		} else {
			$location.search("tab", "details");
			$scope.selectedTab = "details";
		}
	}

	$scope.loadLeadDetail = function() {

		var leaduid = $routeParams.uid;
		$scope.loading = true;

		leadmgmtService
				.getLeadDetails(leaduid)
				.then(
						function(response) {
							var lead = response.data.details;
							$scope.lead = lead;
							$scope.loading = false;
						},
						function(err) {
							console.log(err);
							$scope.loading = false;
							$window
									.alert("Error fetching lead information. Kindly refresh the page and try again");
						});

	};

	$scope.navigateBack = function() {
		$location.url(utils.getSession("urlSrc"));
	};

	$scope.getMasterCatalog = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		commonDataService.getMasterCatalog().then(function(response) {
			$scope.categories = response.categories;
			$scope.subcategories = response.subcategories;
		}, function(err) {
			alert("Error loading Catalog Master");
			console.log(err);
		});
	};

	$scope.getMasterLocations = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		commonDataService.getMasterLocations().then(function(response) {
			$scope.states = response.states;
			$scope.cities = response.cities;
		}, function(err) {
			alert("Error loading Location Master");
			console.log(err);
		});
	}

	$scope.onStateChange = function() {
		
		var lead = null;
		
		if($scope.ngDialogData){
			lead = $scope.ngDialogData.lead;
		}else{
			lead = $scope.lead;
		}

		lead.other_state = null;
		lead.other_city = null;

		if (lead.state_uid == 'other') {
			lead.city_uid = 'other';
		} else {
			lead.city_uid = '';
		}
	};
	$scope.onCityChange = function() {
		var lead = null;
		
		if($scope.ngDialogData){
			lead = $scope.ngDialogData.lead;
		}else{
			lead = $scope.lead;
		}

		lead.other_city = null;
	};

	$scope.loadTimeframes = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		commonDataService.getMasterListByCode("_timeframe").then(
				function(response) {
					$scope.timeframes = response.data.list;
				}, function(err) {
					alert("Error loading Timeframes List");
					console.log(err);
				});
	};

	$scope.cloneLead = function(dialogData) {

		$scope.closeThisDialog();

		var uinfo = utils.getSession("uinfo");

		var lead = angular.copy(dialogData.lead);
		lead.client_name = "Clone of " + lead.client_name;
		lead.status = "New";
		lead.user_uid = uinfo.uid;

		delete lead.uid;
		delete lead.apr_ticket_value;

		var obj = {
			lead : lead
		};
		utils.showPopup("./views/leadmgmt/popup-new-lead.html", obj, "",
				"leadmgmtCtlr");
	};

	$scope.saveLead = function(ngDialogData, newStatus) {
		$scope.savingLead = true;

		var lead = angular.copy(ngDialogData.lead);

		if (newStatus) {
			lead.status = newStatus;
		}

		leadmgmtService.saveLead(lead).then(function(response) {
			$scope.savingLead = false;
			$scope.closeThisDialog();
			$route.reload();
		}, function(err) {
			$scope.savingLead = false;
			console.log(err);
			$window.alert("error saving lead");
		});
	};

	$scope.saveLeadDetail = function() {
		$scope.savingLead = true;
		var lead = angular.copy($scope.lead);

		leadmgmtService.saveLead(lead).then(function(response) {
			$scope.savingLead = false;
			$route.reload();
		}, function(err) {
			$scope.savingLead = false;
			console.log(err);
			$window.alert("error saving lead");
		});
	};

	$scope.loadLeadServiceProviders = function() {
		var leaduid = $routeParams.uid;
		$scope.loading = true;

		leadmgmtService
				.getLeadServiceProviders(leaduid)
				.then(
						function(response) {
							$scope.lead = response.data.details[0][0];
							$scope.splst = response.data.details[1];
							$scope.recLimit = 10;
							$scope.loading = false;
						},
						function(err) {
							console.log(err);
							$scope.loading = false;
							$window
									.alert("Error fetching lead service providers list. Kindly refresh the page and try again");
						});
	};

	$scope.loadMore = function() {
		$scope.recLimit += 10;

	}

	$scope.shareUnshareThisLead = function(sp, type) {
		var uinfo = utils.getSession("uinfo");

		var leadServiceProvider = {
			uid : sp.spl_uid,
			sp_uid : sp.sp_uid,
			lead_uid : sp.lead_uid,
			acceptance_status : (type == 'share' ? 'Yet To View' : null),
			shared_on : (type == 'share' ? new Date() : null),
			status : (type == 'share' ? 'New' : null),
			active : (type == 'share' ? 1 : 0),
			createdby : uinfo.uname,
			modifiedby : uinfo.uname,
			newcreatedby : uinfo.uname,
			newmodifiedby : uinfo.uname
		}

		if (type == 'share') {
			// Placeholder
		} else if (type == 'unshare') {
			delete leadServiceProvider.createdby;
			delete leadServiceProvider.newcreatedby;
		} else {
			return;
		}

		$scope.sharingLead = true;
		if (!$scope.shareSPUID) {
			$scope.shareSPUID = {};
		}
		$scope.shareSPUID[sp.sp_uid] = true;

		leadmgmtService.saveLeadServiceProvider(leadServiceProvider).then(
				function(response) {

					// BELOW CODE IS TO REFRESH SPECIFIC SP_LEADS ENTRY IN UI
					leadmgmtService.getLeadServiceProviders(sp.lead_uid,
							sp.sp_uid).then(function(response) {

						// console.log(response);
						// $scope.lead = response.data.details[0][0];
						var newSP = response.data.details[1][0];

						for ( var k in newSP) {
							sp[k] = newSP[k];
						}

						$scope.sharingLead = false;
						$scope.shareSPUID[sp.sp_uid] = false;
					}, function(err) {
						console.log(err);
						$route.reload();
						// $window
						// .alert("Error fetching lead service providers list.
						// Kindly refresh the page and try again");

						$scope.sharingLead = false;
						$scope.shareSPUID[sp.sp_uid] = false;
					});

				}, function(err) {
					$scope.sharingLead = false;
					$scope.shareSPUID[sp.sp_uid] = false;
					console.log(err);
					$window.alert("error saving lead");
				});

	};

	$scope.changeLeadStatus = function(leadinfo, newStatus, isDialog) {
		var uinfo = utils.getSession("uinfo");

		var lead = {
			uid : leadinfo.uid,
			status : newStatus,
			newmodifiedby : uinfo.uname
		}

		leadmgmtService.saveLead(lead).then(function(response) {
			$scope.savingLead = false;

			if (isDialog) {
				$scope.closeThisDialog();
			}

			$route.reload();
		}, function(err) {
			$scope.savingLead = false;
			console.log(err);
			$window.alert("error saving lead");
		});

	};

	$scope.updateLeadStatusFromDetail = function(leadinfo, newStatus,
			newStatusComment) {

		$scope.savingLead = true;

		var uinfo = utils.getSession("uinfo");

		var lead = {
			uid : leadinfo.uid,
			status : newStatus,
			newmodifiedby : uinfo.uname,
			apr_ticket_value : (newStatus=='Completed')? leadinfo.apr_ticket_value:null
		}

		if (leadinfo.status == "Pending Verification") {
			lead.comments_verification = newStatusComment;
		} else if (leadinfo.status == "In Progress") {
			lead.comments_final = newStatusComment;
		} else {
			// FUTURE SCOPE
		}

		leadmgmtService
				.saveLead(lead)
				.then(
						function(response) {
							$scope.savingLead = false;

							if (newStatus == 'Verified') {
								$location.search("tab", "serviceprovider");
								utils
										.showAlert("Next step is to share the lead with service providers");
							}
							$route.reload();
						}, function(err) {
							$scope.savingLead = false;
							console.log(err);
							$window.alert("error saving lead");
						});

	};

	$scope.changePage = function(pgnum) {
		if (!utils.isActiveUser()) {
			return;
		}

		$location.search("pgnum", pgnum);
		$scope.loadLeads($scope.leadtype);
	};

	$scope.switchTab = function(tabname) {
		if (!utils.isActiveUser()) {
			return;
		}

		$location.search("tab", tabname);

		if ($location.search().pgnum) {
			$location.search("pgnum", null);
		}
		$scope.selectedTab = tabname;
	};

	$scope.truncate = function(fullStr, strLen, separator) {
		if (fullStr.length <= strLen)
			return fullStr;

		separator = separator || '...';

		var sepLen = separator.length, charsToShow = strLen - sepLen, frontChars = Math
				.ceil(charsToShow / 2), backChars = Math.floor(charsToShow / 2);

		return fullStr.substr(0, frontChars) + separator
				+ fullStr.substr(fullStr.length - backChars);
	};

	$scope.popupSPLead = function(splead_uid) {
		if (!utils.isActiveUser()) {
			return;
		}
		var uinfo = utils.getSession("uinfo");

		var obj = {
			splead_uid : splead_uid
		// angular.copy(lead)
		};

		var popupUrl = "./views/leadmgmt/popup-view-splead.html";

		utils.showPopup(popupUrl, obj, "", "leadmgmtCtlr");
	};

	$scope.editSPLead = function(splead_uid) {
		if (!utils.isActiveUser()) {
			return;
		}
		var uinfo = utils.getSession("uinfo");

		utils.setSession("urlSrc", $location.url());
		$location.url("/sp/leads/details/" + splead_uid);
	};

	$scope.loadSPLeadDetails = function(isPopup) {
		var uinfo = utils.getSession("uinfo");

		var splead_uid = "";

		if (isPopup == true) {
			splead_uid = $scope.ngDialogData.splead_uid;
		} else {
			splead_uid = $routeParams.uid;
		}

		leadmgmtService.getSPLeadDetails(splead_uid).then(
				function(response) {
					var lead = response.data.details;
					lead.commentsHistory = $scope.textToHtml(lead.comments);
					$scope.lead = lead;

					if (lead.lead_status == 'In Progress' && lead.acceptance_status == 'Yet To View') {
						lead.acceptance_status = "Viewed";
						lead.viewed_on = new Date();
						lead.newmodifiedby = uinfo.uname;

						$scope.lead = lead;
						leadmgmtService.saveLeadServiceProvider(lead).then(
								function(resp) {
									// DO NOTHING
								}, function(err) {
									console.log(err);
									$window.alert("Error " + err);
								});
					} 
				}, function(err) {
					console.log(err);
					$window.alert("Error " + err);
				})
	};

	$scope.acceptRejectLead = function(acceptance_status) {
		var uinfo = utils.getSession("uinfo");

		var lead = angular.copy($scope.lead);
		lead.acceptance_status = acceptance_status;
		lead.accepted_on = new Date();

		if (acceptance_status == "Accepted") {
			lead.status = "In Progress";
		} else if (acceptance_status == "Rejected") {
			lead.status = "Terminated";
			lead.comments = "Lead Rejected";
		};

		lead.newmodifiedby = uinfo.uname;

		leadmgmtService.saveLeadServiceProvider(lead).then(function(resp) {
			// DO NOTHING

			if (acceptance_status == "Accepted") {
				$scope.closeThisDialog();
				$scope.editSPLead($scope.lead.uid);
			} else {
				$scope.closeThisDialog();
				$route.reload();
			}

		}, function(err) {
			console.log(err);
			$window.alert("Error " + err);
		});
	};

	$scope.updateSPLeadStatusFromDetail = function(newStatus, newStatusComment) {

		$scope.savingLead = true;

		var uinfo = utils.getSession("uinfo");

		var lead = {
			uid : $scope.lead.uid,
			newmodifiedby : uinfo.uname,
			status : $scope.lead.status,
			comments : $scope.lead.comments,
			apr_ticket_value : (newStatus == 'Approved')
					? $scope.lead.apr_ticket_value
					: null
		}

		if (!$scope.lead.comments) {
			lead.comments = '';
		}

		lead.comments = uinfo.uname + " [" + (new Date()).toLocaleString()
				+ "]" + " : " + newStatusComment + "\n\n" + lead.comments;

		lead.status = (newStatus == 'General') ? lead.status : newStatus;

		leadmgmtService.saveLeadServiceProvider(lead).then(function(response) {
			$scope.savingLead = false;
			$route.reload();
		}, function(err) {
			$scope.savingLead = false;
			console.log(err);
			$window.alert("error saving lead");
		});
	};
	$scope.textToHtml = function(text) {
		text = (!text) ? "" : text;

		var re = new RegExp(/\r?\n/g);
		var newText = text.replace(re, '<br />');

		return newText;
	}
	
	$scope.serviceProviderFilter = function(sp){
		var leadtype = $scope.lead.leadtype.toLowerCase();
		return (leadtype != 'past' || (leadtype == 'past' && sp.sharing_status==1));
	}
}
