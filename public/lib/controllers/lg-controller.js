lpApp.controller('lgCtlr', LgCtlr);

LgCtlr.$inject = ['$scope', '$routeParams', '$route', '$location', 'utils',
    'userSvc'];

function LgCtlr($scope, $routeParams, $route, $location, utils, userSvc) {

    $scope.loadHome = function () {
        var currentUser = utils.getSession("uinfo");
        $scope.user = currentUser;
        console.log(currentUser);
        if (currentUser.mobileverified !== true) {
            $location.url('/lg/profile');
            return;
        } else {
            $location.url('/lg/leads');
            return;
        }
    };

    $scope.loadBankDetails = function () {

        if (!utils.isActiveUser()) {
            return;
        }

        var currentUser = utils.getSession("uinfo");

        // var uid = currentUser.uid;
        var utype = currentUser.utype;
        $scope.utype = utype;

        var user_uid;

        if (utype.toLowerCase() === 'lg') {
            user_uid = currentUser.uid;
        } else if (utype.toLowerCase() === 'su') {
            user_uid = $routeParams.uid;
        } else {
            utils.showAlert("Invalid Method Call");
            return;
        }

        $scope.loading = true;
        userSvc
                .getBankDetails(user_uid)
                .then(
                        function (response) {
                            $scope.loading = false;
                            if (!response.data || response.data.error) {
                                utils.showAlert("Error Fetching Bank Details");
                            } else {
                                var bankdetails = response.data.details;

                                if (!bankdetails.uid) {
                                    bankdetails = {
                                        user_uid: user_uid,
                                        verified: 0,
                                        editable: 1
                                    }
                                }
                                ;

                                $scope.bankdetails = bankdetails;
                                $scope.org_bank = angular
                                        .copy($scope.bankdetails);
                            }
                        },
                        function (errResponse) {
                            $scope.loading = false;
                            console.log(err);
                            utils
                                    .showAlert("Unable to fetch profile details. Please try again later");
                        });
    };

    $scope.resetBankTab = function () {
        $scope.bankdetails = angular.copy($scope.org_bank);
    };

    $scope.saveBankTab = function (bankinfo) {
        if (!utils.isActiveUser()) {
            return;
        }

        var currentUser = utils.getSession("uinfo");
        var utype = currentUser.utype;
        var user_uid;

        if (utype.toLowerCase() === 'lg') {
            user_uid = currentUser.uid;
        } else if (utype.toLowerCase() === 'su') {
            user_uid = $routeParams.uid;
        } else {
            utils.showAlert("Invalid Method Call");
            return;
        }

        $scope.eventinprogress = true;
        $scope.eventmessage = "saving";

        var binfo = angular.copy(bankinfo);
        binfo.editable = !binfo.verified;

        userSvc.saveBankDetail(binfo).then(function (message) {
//			utils.showAlert("Bank Details saved successfully");
            $scope.eventinprogress = false;
            $route.reload();
        }, function (errMessage) {
            $scope.eventinprogress = false;
            utils.showAlert(errMessage);
            console.log(errMessage);
        });
    };

    $scope.toggleVerification = function (bankdetails) {

        var binfo = angular.copy(bankdetails);
        binfo.verified = !binfo.verified;
        binfo.editable = !binfo.verified;

        $scope.saveBankTab(binfo);
    }

}
