lpApp.controller('locationsCtlr', LocationsCtlr);

LocationsCtlr.$inject = ['$scope', '$routeParams', '$route', '$location',
    '$window', 'utils', 'apiService', 'emailService', 'validationService'];

function LocationsCtlr($scope, $routeParams, $route, $location, $window, utils,
        apiService, emailService, validationService) {

    var API_URL = utils.getSession("apiUrl");

    $scope.loadHome = function () {

        if (!utils.isActiveUser()) {
            return;
        }

        $scope.bcArray = [];
        $scope.bcArray.push({
            title: "Config",
            url: ""
        });
        $scope.bcArray.push({
            title: "Locations",
            url: ""
        });

        $scope.loading = true;

        $scope.filter = {
            country: null,
            state: null,
            district: null,
            city: null
        };

        $scope.loadLocations();

    };

    $scope.loadLocations = function (country, state, district, city) {

        if (!utils.isActiveUser()) {
            return;
        }

        var filter = $scope.filter;

        var country = filter.country;
        var state = filter.state;
        var district = filter.district;
        var city = filter.city;

        var url = API_URL + "/locations/";
        var bcArray = $scope.bcArray;

        var locationType = "country";

        if (city) {
            url += "zipcodes/" + city.uid; // FUTURE
            locationType = "zipcodes";
        } else if (district) {
            url += "cities/" + district.uid;
            locationType = "cities";
        } else if (state) {
            url += "districts/" + state.uid;
            locationType = "districts";
        } else if (country) {
            url += "states/" + country.uid;
            locationType = "states";
        } else {
            locationType = "country";
        }

        $scope.loading = true;

        var params = {};

        try {
            apiService.getCall(url, params).then(function (response) {
                if (!response.data) {
                    throw "UNKNOWN ERROR";
                }

                if (response.data.error) {
                    throw response.data.error;
                } else {
                    $scope.locations = response.data.list;
                    var rowCount = response.data.rowCount;
                    $scope.loading = false;

                    if (locationType === "country") {
                        $scope.lstCountries = response.data.list;
                        $scope.lstStates = null;
                        $scope.lstDistricts = null;
                        $scope.lstCities = null;
                    } else if (locationType === "states") {
                        $scope.lstStates = response.data.list;
                        $scope.lstDistricts = null;
                        $scope.lstCities = null;
                    } else if (locationType === "districts") {
                        $scope.lstDistricts = response.data.list;
                        $scope.lstCities = null;
                    } else if (locationType === "cities") {
                        $scope.lstCities = response.data.list;
                    }
                }
            }, function (errResponse) {
                throw errResponse;
            });
        } catch (err) {
            $scope.loading = false;
            console.log(err);
            $window.alert(err);
        }
    };

    $scope.filterChange = function (whatChanged) {

        var country = $scope.filter.country;
        var state = $scope.filter.state;
        var district = $scope.filter.district;
        var city = $scope.filter.city;

        if (whatChanged == 'country') {
            $scope.lstStates = $scope.filter.state = null;
            $scope.lstDistricts = $scope.filter.district = null;
            $scope.lstCities = $scope.filter.city = null;
        } else if (whatChanged == 'state') {
            $scope.lstDistricts = $scope.filter.district = null;
            $scope.lstCities = $scope.filter.city = null;
        } else if (whatChanged == 'district') {
            $scope.lstCities = null;
        } else if (whatChanged == 'city') { // Future
        }

//		$scope.loadLocations(country, state, district, city);
        $scope.loadLocations();
    }

    $scope.resetFilter = function () {
        $scope.filter = {
            country: null,
            state: null,
            district: null,
            city: null
        };

        $scope.loadLocations();
    }

    $scope.addList = function (listType) {
        if (!utils.isActiveUser()) {
            return;
        }

        var country = $scope.filter.country;
        var state = $scope.filter.state;
        var district = $scope.filter.district;
        var city = $scope.filter.city;

        var parent_uid = null;

        if (listType === 'city') {
            parent_uid = district.uid;
        } else if (listType === 'district') {
            parent_uid = state.uid;
        } else if (listType === 'state') {
            parent_uid = country.uid;
        }
        var list = {
            uid: null,
            code: null,
            description: null,
            parent_uid: parent_uid,
            seq: 999,
            active: 1,
            createdby: null,
            createdon: null,
            modifiedby: null,
            modifiedon: null
        }

        var obj = {
            list: list,
            type: listType,
            filter: $scope.filter,
            parent: $scope,
            mode: 'add'
        };
        utils.showPopup("./views/locations/popup-location-edit.html", obj,
                "", "locationsCtlr");
    };

    $scope.editList = function (list) {
        if (!utils.isActiveUser()) {
            return;
        }

        var listType = '';

        if ($scope.filter.city) {
            listType = 'zipcode';
        } else if ($scope.filter.district) {
            listType = 'city';
        } else if ($scope.filter.state) {
            listType = 'district';
        } else if ($scope.filter.country) {
            listType = 'state';
        }

        var obj = {
            list: angular.copy(list),
            type: listType,
            filter: $scope.filter,
            parent: $scope,
            mode: 'edit'
        };
        utils.showPopup("./views/locations/popup-location-edit.html", obj,
                "", "locationsCtlr");
    };

    $scope.saveList = function (dialogData) {
        var list = dialogData.list;

        if (!utils.isActiveUser()) {
            return;
        }

        // TODO : VALIDATE INFORMATION

        if (!list.code || !list.description) {
            $window.alert("Required information is missing");
            return;
        }

        $scope.eventinprogress = true;
        $scope.eventmessage = "saving";
        validationService
                .checkForListCode(list.code, list.parent_uid)
                .then(
                        function (response) {
                            if (!list.uid && response.data.error) {
                                $window
                                        .alert("Error validating Name. Try again after sometime");
                                $scope.eventinprogress = false;
                                return;
                            } else if (!list.uid
                                    && response.data.result === false) {
                                $window
                                        .alert("Code already taken. Try some other code");
                                $scope.eventinprogress = false;
                                return;
                            } else {

                                // DO WEB API CALL TO SAVE RECORD
                                var currentUser = utils.getSession("uinfo");
                                try {
                                    list.newcreatedby = currentUser.uname;
                                    list.newmodifiedby = currentUser.uname;

                                    var postData = {
                                        mlinfo: list
                                    };

                                    var apiUrl = API_URL + "/locations/details/";

                                    apiUrl += (list.uid) ? "update" : "insert";

                                    apiService
                                            .postCall(apiUrl, postData)
                                            .then(
                                                    function (response) {
                                                        if (!response.data) {
                                                            throw "UNKNOWN ERROR";
                                                        } else if (response.data.error) {
                                                            throw response.data.error;
                                                        } else {
                                                            $scope.eventinprogress = false;
                                                            $scope
                                                                    .closeThisDialog(dialogData.ngDialogId);
                                                            dialogData.parent.loadLocations();

                                                        }
                                                    }, function (errResponse) {
                                                throw errResponse;
                                            });
                                } catch (err) {
                                    $window
                                            .alert("Unable to Save List. Please try again later.");
                                    console.log(err);
                                    $scope.eventinprogress = false;
                                }

                            }
                        },
                        function (errResponse) {
                            $window
                                    .alert("Unable to validate List Name. Try again after sometime");
                            $scope.eventinprogress = false;
                            return;
                        });
    };

}
