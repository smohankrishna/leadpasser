lpApp.controller('suCtlr', SuCtlr);

SuCtlr.$inject = ['$scope', '$rootScope', '$routeParams', '$route',
		'$location', '$window', 'utils', 'apiService', 'validationService',
		'emailService', 'userSvc'];

function SuCtlr($scope, $rootScope, $routeParams, $route, $location, $window,
		utils, apiService, validationService, emailService, userSvc) {

	var PAGE_LIMIT = 10;
	var API_URL = utils.getSession("apiUrl");

	$scope.loadHome = function() {

		if (!utils.isActiveUser()) {
			return;
		}

	};

	var getUTypeName = function(utype) {
		var utypeName = "";
		switch (utype.toUpperCase()) {
			case "SU" :
				utypeName = "System Users";
				break;
			case "LG" :
				utypeName = "Lead Passers";
				break;
			case "SP" :
				utypeName = "Service Providers";
				break;
			case "CL" :
				utypeName = "Prospects";
				break;
		}

		return utypeName;
	};

	$scope.loadPageTab = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var qry = $location.search();
		if (qry && qry.tab) {
			$scope.switchTab(qry.tab);
		} else {
			$scope.switchTab("info");
		}
	};

	$scope.loadUsersList = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		$scope.loading = true;
		var pgnum = Number($location.search().pgnum);
		var searchFilter = $location.search().search;

		if (searchFilter) {
			searchFilter = JSON.parse(searchFilter);
			
			var filter = {};
			for(key in searchFilter){
				filter[key] = searchFilter[key];
			}
			$scope.filter = filter;
		}

		utils.setSession("urlSrc", "");

		var utype = $routeParams.utype.toUpperCase();
		var utypename = getUTypeName(utype);

		$scope.utype = utype.toUpperCase();

		var bcArray = [];
		bcArray.push({
			title : "Accounts",
			url : ""
		});
		bcArray.push({
			title : utypename,
			url : ""
		});
		$scope.bcArray = bcArray;

		if (!pgnum) {
			pgnum = 1;
		}
		var options = {
			limit : PAGE_LIMIT,
			offset : (pgnum - 1) * PAGE_LIMIT,
			orderby : (utype == 'SP') ? "organization" : "name"
		};

		var params = {
			filter : {},
			options : options,
			search : searchFilter
		};

		try {
			apiService.getCall(API_URL + "/user/list/" + utype, params).then(
					function(response) {
						if (!response.data) {
							throw "UNKNOWN ERROR";
						}

						if (response.data.error) {
							throw response.data.error;
						} else {
							$scope.users = response.data.list;

							var rowCount = response.data.rowCount;

							var prevPage = null;
							var nextPage = null;

							if (rowCount > PAGE_LIMIT) {
								prevPage = (pgnum > 1) ? pgnum - 1 : null;
								var rectillthispage = pgnum * PAGE_LIMIT;

								if (rectillthispage < rowCount) {
									nextPage = pgnum + 1;
								}
							}

							$scope.prevPage = prevPage;
							$scope.nextPage = nextPage;
							$scope.pgnum = pgnum;
							$scope.totalpages = Math
									.ceil(rowCount / PAGE_LIMIT);

							$scope.loading = false;
						}
					}, function(errResponse) {
						throw errResponse;
					});
		} catch (err) {
			$scope.loading = false;
			console.log(err);
			$window.alert(err);
		}
	};

	$scope.searchUserList = function(filter) {

		if (!utils.isActiveUser()) {
			return;
		}

		for (key in filter) {
			if (!filter[key]) {
				delete filter[key];
			}
		}

		if (!filter || Object.keys(filter).length == 0) {
			$location.search("search", null);
		} else {
			$location.search("search", JSON.stringify(filter));
		}

		$location.search("pgnum", 1);
		$scope.loadUsersList();
	};
	
	$scope.clearSearch = function(filter) {
		
		for(key in filter){
			filter[key] = null;
		}

		$location.search("search", null);
		$location.search("pgnum", 1);
		$scope.loadUsersList();
	};

	$scope.changePage = function(pgnum) {
		if (!utils.isActiveUser()) {
			return;
		}

		$location.search("pgnum", pgnum);
		$scope.loadUsersList(pgnum);
	};

	$scope.navigateToUserList = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		var utype = $routeParams.utype;
		var urlSrc = utils.getSession("urlSrc");

		if (urlSrc) {
			$location.url(urlSrc);
		} else {
			$location.url("/su/accounts/" + utype);
		}
	};

	$scope.navigateToNewUserDetails = function() {

		if (!utils.isActiveUser()) {
			return;
		}
		var utype = $routeParams.utype;
		utils.setSession("urlSrc", $location.url());

		$location.url("/su/accounts/" + utype + "/new");
	};

	$scope.navigateToUserDetails = function(uid, selectedUser) {

		if (!utils.isActiveUser()) {
			return;
		}

		var utype = $routeParams.utype;
		utils.setSession("urlSrc", $location.url());
		utils.setSession("detailsOfUser", selectedUser);

		$location.url("/su/accounts/" + utype + "/details/" + uid);
	};

	$scope.readUserType = function(actiontype) {

		if (!utils.isActiveUser()) {
			return;
		}

		var utype = $routeParams.utype;
		$scope.utype = utype.toUpperCase();
		var utypename = getUTypeName(utype);

		var selectedUser = utils.getSession("detailsOfUser");

		var bcArray = [];
		bcArray.push({
			title : "Accounts",
			url : ""
		});
		bcArray.push({
			title : utypename,
			url : utils.getSession("urlSrc")
		});

		bcArray.push({
			title : (!actiontype || actiontype === "edit")
					? "Edit User Details"
					: "New User Details",
			url : ""
		});
		$scope.bcArray = bcArray;
	};

	$scope.prepareNewUser = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		var utype = $routeParams.utype.toUpperCase();
		var user = {
			uid : null,
			name : null,
			organization : null,
			utype : utype,
			uname : null,
			upass : null,
			temp_pwd : null,
			resetpwd : 1,
			email : null,
			emailverified : 0,
			ecode : utils.guid(),
			ecodevalidity : null,
			mobilenum : null,
			mobileverified : 0,
			mcode : null,
			mcodevalidity : null,
			name2 : null,
			email2 : null,
			mobilenum2 : null,
			active : 1,
			createdby : null,
			createdon : null,
			modifiedby : null,
			modifiedon : null
		};

		// setting the ecodevalidity to [today + 2 days]
		var ecodevalidity = new Date();
		ecodevalidity = ecodevalidity.setDate(ecodevalidity.getDate() + 2);
		user.ecodevalidity = new Date(ecodevalidity);

		user.temp_pwd = utils.randomPassword(10);
		user.upass = utils.hash(user.temp_pwd);

		$scope.maxDoB = new Date();
		$scope.maxDoB = new Date($scope.maxDoB.getFullYear() - 18,
				$scope.maxDoB.getMonth(), $scope.maxDoB.getDate());

		$scope.user = user;
	};

	$scope.loadInfoTab = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		var uid = $routeParams.uid;
		var utype = $routeParams.utype;

		$scope.maxDoB = new Date();
		$scope.maxDoB = new Date($scope.maxDoB.getFullYear() - 18,
				$scope.maxDoB.getMonth(), $scope.maxDoB.getDate());

		$scope.loading = true;
		try {
			apiService.getCall(API_URL + "/user/details/" + uid).then(
					function(response) {
						if (!response.data) {
							throw "UNKNOWN ERROR";
						}

						if (response.data.error) {
							throw response.data.error;
						} else {
							$scope.user = response.data.details;
							if ($scope.user.dob) {
								$scope.user.dob = new Date($scope.user.dob);
							}
							$scope.org_user = angular.copy($scope.user);
							$scope.loading = false;
						}
					}, function(errResponse) {
						throw errResponse;
					});
		} catch (err) {
			$scope.loading = false;
			console.log(err);
			$window
					.alert("Unable to fetch User Details. Please try again later");
			$location.url("/su/accounts/" + utype);
		}
	};

	$scope.saveInfoTab = function(user) {
		if (!utils.isActiveUser()) {
			return;
		}

		// TODO : VALIDATE INFORMATION

		if (!user.name || !user.uname || !user.mobilenum) {
			$window.alert("Required information is missing");
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "saving";

		// USE USERSVC

		var newUser = angular.copy(user);
		if (newUser.dob) {
			newUser.dob = new Date(user.dob);
		}

		if (!newUser.uid) {
			newUser.mcode = utils.randomOTP();
			var mcodevalidity = new Date();
			mcodevalidity = mcodevalidity
					.setMinutes(mcodevalidity.getMinutes() + 20);
			newUser.mcodevalidity = new Date(mcodevalidity);
		}

		userSvc.saveUserDetails(newUser).then(function(response) {
			$scope.eventinprogress = false;
			if (response) {
				console.log(response);
			}
			$scope.navigateToUserList();
		}, function(err) {
			$scope.eventinprogress = false;
			$window.alert(err);

			console.log(err);
		});
	};

	$scope.resendValidationEmail = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "emailing";

		var user = angular.copy($scope.org_user);

		user.emailverified = 0;
		user.ecode = utils.guid();

		var ecodevalidity = new Date();
		ecodevalidity = ecodevalidity.setDate(ecodevalidity.getDate() + 2);
		user.ecodevalidity = new Date(ecodevalidity);

		// save the new ecode

		userSvc
				.saveUserDetails(user)
				.then(
						function(message) {
							$scope.user.emailverified = user.emailverified;
							$scope.user.ecode = user.ecode;
							$scope.user.ecodevalidity = user.ecodevalidity;

							$scope.org_user.emailverified = user.emailverified;
							$scope.org_user.ecode = user.ecode;
							$scope.org_user.ecodevalidity = user.ecodevalidity;

							userSvc
									.emailChangeNotification(user)
									.then(
											function(message) {
												if (message) {
													$scope.eventinprogress = false;
													$window.alert(message);
												} else {
													$scope.eventinprogress = false;
													$window
															.alert("New Email Address saved and verification email sent.");
												}
											},
											function(err) {
												$window
														.message("New Email Address saved and but unable to send verification email.");
												$scope.eventinprogress = false;
											});
						},
						function(err) {
							$scope.eventinprogress = false;
							$window
									.alert("Unable to update the new email address");
							console.log(err);
						});
	};

	$scope.resetPassword = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "resetpwd";

		var user = angular.copy($scope.org_user);

		user.temp_pwd = utils.randomPassword(10);
		user.upass = utils.hash(user.temp_pwd);
		user.resetpwd = 1;

		userSvc
				.saveUserDetails(user)
				.then(
						function(message) {
							$scope.user.temp_pwd = user.temp_pwd;
							$scope.user.upass = user.upass;
							$scope.user.resetpwd = user.resetpwd;

							$scope.org_user.temp_pwd = user.temp_pwd;
							$scope.org_user.upass = user.upass;
							$scope.org_user.resetpwd = user.resetpwd;

							userSvc
									.passwordNotification(user)
									.then(
											function(message) {
												$scope.eventinprogress = false;
												$window
														.alert("Password reset and notified on email/mobile");

											},
											function(err) {
												$window
														.alert("New Password saved but unable to notify. Please retry.");
												$scope.eventinprogress = false;
											});
						}, function(err) {
							$scope.eventinprogress = false;
							$window.alert("Unable to reset the password");
							console.log(err);
						});
	};

	$scope.editEmail = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var obj = {
			user : angular.copy($scope.org_user),
			org_user : $scope.org_user
		};
		utils.showPopup("./views/su/users/popup-email-edit.html", obj, "",
				"suCtlr");
	};

	$scope.updateEmail = function(dialogData) {
		if (!utils.isActiveUser()) {
			return;
		}

		var user = dialogData.user;
		var org_user = dialogData.org_user;

		if (user.email === org_user.email) {
			// NO Action required
			$scope.closeThisDialog(dialogData.ngDialogId);
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "updatingemail";

		user.emailverified = 0;
		user.ecode = utils.guid();

		var ecodevalidity = new Date();
		ecodevalidity = ecodevalidity.setDate(ecodevalidity.getDate() + 2);
		user.ecodevalidity = new Date(ecodevalidity);

		userSvc
				.saveUserDetails(user)
				.then(
						function(message) {

							if (!user.email) {
								$window.alert("Email cleared from record");
								$scope.eventinprogress = false;
								$scope.closeThisDialog(dialogData.ngDialogId);
								$route.reload();
							} else {

								userSvc
										.emailChangeNotification(user)
										.then(
												function(message) {
													if (message) {
														$scope.eventinprogress = false;
														$window.alert(message);
														$scope
																.closeThisDialog(dialogData.ngDialogId);
														$route.reload();
													} else {
														$scope.eventinprogress = false;
														$window
																.alert("New Email Address saved and verification email sent.");
														$scope
																.closeThisDialog(dialogData.ngDialogId);
														$route.reload();
													}
												},
												function(err) {
													$scope.eventinprogress = false;
													$window
															.alert("New Email Address saved and but unable to send verification email.");
													$scope
															.closeThisDialog(dialogData.ngDialogId);
													$route.reload();
												});
							}
						},
						function(err) {
							$scope.eventinprogress = false;
							$window
									.alert("Unable to update the new email address");
							console.log(err);
						});

	};

	$scope.editMobile = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var obj = {
			user : angular.copy($scope.org_user),
			org_user : $scope.org_user
		};
		utils.showPopup("./views/su/users/popup-mobile-edit.html", obj, "",
				"suCtlr");
	};

	$scope.updateMobile = function(dialogData) {

	};

	$scope.resetInfoTab = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		$scope.user = angular.copy($scope.org_user);
	};

	$scope.switchTab = function(tabname) {
		if (!utils.isActiveUser()) {
			return;
		}

		$location.search("tab", tabname);
		$scope.selectedTab = tabname;
	};

	/***************************************************************************
	 * Location Tab
	 **************************************************************************/
}
