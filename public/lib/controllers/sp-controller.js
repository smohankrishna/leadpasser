lpApp.controller('spCtlr', SpCtlr);

SpCtlr.$inject = ['$scope', '$routeParams', '$route', 'utils',
		'apiService', 'commonDataService'];

function SpCtlr($scope, $routeParams, $route, utils, apiService,
		commonDataService) {

	var API_URL = utils.getSession("apiUrl");
	
	$scope.loadProducts = function() {
		commonDataService.getMasterCatalog().then(function(response){
			$scope.categories = response.categories;
			$scope.subcategories = response.subcategories;
		}, function(err){
			alert("Error loading Catalog Master");
			console.log(err);
		});
	};

	$scope.loadSPCatalog = function() {
		var sp_uid = $routeParams.uid;
		$scope.loadingCatalog = true;
		apiService.getCall(API_URL + "/serviceprovider/catalog/" + sp_uid).then(
				function(response) {
					$scope.loadingCatalog = false;
					$scope.myCatalog = response.data.list;

				}, function(err) {
					$scope.loading = false;
					$scope.myCatalog = null;
					alert("Error loading Service Provider Catalog");
					console.log(err);
				});
	}

	$scope.addToCatalog = function(category, subcategory) {
		var uinfo = utils.getSession("uinfo");

		if (uinfo.utype.toUpperCase() != "SU") {
			alert("You are not allowed to perform this operation");
			return;
		}

		var newObj = {
			uid : null,
			sp_uid : $routeParams.uid,
			category_uid : category,
			subcategory_uid : subcategory,
			active : 1,
			newcreatedby : uinfo.uname,
			newmodifiedby : uinfo.uname
		};

		apiService
				.postCall(API_URL + "/serviceprovider/catalog/insert", {
					data : newObj
				})
				.then(
						function(response) {
							var err = response.data.error;
							if (err) {
								if (err.code === 'ER_DUP_ENTRY') {
									alert("Record already exists");
								} else {
									console.log(err);
									alert("Error adding the product to catalog");
								}
							} else {
								$route.reload();
							};
						},
						function(err) {
							alert("Unknown error occurred while inserting the record. Please try after sometime.");
							console.log(err);
						});
	}

	$scope.toggleCatalog = function(item, currentStatus) {
		if (!utils.isActiveUser()) {
			return;
		}

		var uinfo = utils.getSession("uinfo");

		if (uinfo.utype.toUpperCase() != "SU") {
			alert("You are not allowed to perform this operation");
			return;
		}

		var obj = {
			uid : item.uid,
			active : (item.active == 1) ? 0 : 1,
			modifiedby : item.modifiedby,
			newmodifiedby : uinfo.uname
		};

		item.active = -1;

		apiService
				.postCall(API_URL+
						"/serviceprovider/catalog/update/" + item.uid,
						{
							data : obj
						})
				.then(
						function(response) {
							var err = response.data.error;
							if (err) {
								console.log(err);
								alert("Error updating the product to catalog");
								item.active = currentStatus;
							} else {
								item.active = (currentStatus == 1) ? 0 : 1;
								item.modifiedby = uinfo.uname;
								// $route.reload();
							};
						},
						function(err) {
							item.active = currentStatus;
							alert("Unknown error occurred while updating the record. Please try after sometime.");
							console.log(err);
						});

	};

	$scope.showLocationsPopup = function(catalogItem) {
		if (!utils.isActiveUser()) {
			return;
		}

		var data = {
			sp_uid : $routeParams.uid,
			sp_catalog_uid : catalogItem.uid,
			country : '',
			state : '',
			district : '',
			city : ''
		};

		utils.showPopup("./views/sp/catalog-locations-popup.html", data, "",
				"spCtlr");
	};

	$scope.loadAllLocations = function() {
		if (!utils.isActiveUser()) {
			return;
		}
		commonDataService.getMasterLocations().then(function(response){
			$scope.countries = response.countries;
			$scope.states = response.states;
			$scope.districts = response.districts;
			$scope.cities = response.cities;
		}, function(err){
			alert("Error loading Location Master");
			console.log(err);
		});
	};

	$scope.loadServiceLocations = function() {
		if (!utils.isActiveUser()) {
			return;
		}
		$scope.loadingServiceLocations = true;
		var sp_catalog_uid = $scope.ngDialogData.sp_catalog_uid;
		var sp_uid = $scope.ngDialogData.sp_uid;
		apiService.getCall(API_URL+
				"/serviceprovider/locations/" + sp_catalog_uid).then(
				function(response) {
					$scope.serviceLocations = response.data.list;
					$scope.loadingServiceLocations = false;
				}, function(err) {
					alert("Error loading Location Master");
					console.log(err);
					$scope.loadingServiceLocations = false;
				});

	};

	$scope.addLocation = function(dialogData) {
		if (!utils.isActiveUser()) {
			return;
		}
		var uinfo = utils.getSession("uinfo");

		if (uinfo.utype.toUpperCase() != "SU") {
			alert("You are not allowed to perform this operation");
			return;
		}

		var newObj = {
			sp_catalog_uid : dialogData.sp_catalog_uid,
			country_uid : dialogData.country,
			state_uid : dialogData.state,
			district_uid : dialogData.district,
			city_uid : dialogData.city,
			newcreatedby : uinfo.uname,
			newmodifiedby : uinfo.uname
		};

		apiService
				.postCall(API_URL+"/serviceprovider/locations/insert", {
					data : newObj
				})
				.then(
						function(response) {
							var err = response.data.error;
							if (err) {
								if (err.code === 'ER_DUP_ENTRY') {
									alert("Record already exists");
								} else {
									console.log(err);
									alert("Error adding the product to catalog");
								}
							} else {
//								dialogData.country = '';
								dialogData.state = '';
								dialogData.district = '';
								dialogData.city = '';
								$scope.loadServiceLocations();
							};
						},
						function(err) {
							alert("Unknown error occurred while inserting the record. Please try after sometime.");
							console.log(err);
						});

	};

	$scope.removeSPCatalogLocation = function(dialogData, location) {
		if (!utils.isActiveUser()) {
			return;
		}
		var uinfo = utils.getSession("uinfo");

		if (uinfo.utype.toUpperCase() != "SU") {
			alert("You are not allowed to perform this operation");
			return;
		}

		apiService
				.postCall(API_URL + "/serviceprovider/locations/remove/"+location.uid, {
					data : {}
				})
				.then(
						function(response) {
							var err = response.data.error;
							if (err) {
								console.log(err);
								alert("Error adding the product to catalog");
							} else {
//								dialogData.country = '';
								dialogData.state = '';
								dialogData.district = '';
								dialogData.city = '';
								$scope.loadServiceLocations();
							};
						},
						function(err) {
							alert("Unknown error occurred while removing the record. Please try after sometime.");
							console.log(err);
						});
	};
	
	$scope.closeDialog = function(){
		$scope.closeThisDialog();
		$route.reload();
	};
};
