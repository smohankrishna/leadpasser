lpApp.controller('deCtlr', DeCtlr);

DeCtlr.$inject = ['$scope', '$rootScope', '$routeParams', '$route',
		'$location', '$window', 'utils', 'apiService', 'validationService',
		'emailService'];

function DeCtlr($scope, $rootScope, $routeParams, $route, $location, $window,
		utils, apiService, validationService, emailService) {

	var PAGE_LIMIT = 10;
	var API_URL = utils.getSession("apiUrl");

	$scope.loadHome = function() {

//		if (!utils.isActiveUser()) {
//			return;
//		}

	};

	$scope.loadUsersList = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		$scope.loading = true;
		var pgnum = Number($location.search().pgnum);
		var searchFilter = $location.search().search;

		if (searchFilter) {
			searchFilter = JSON.parse(searchFilter);
		}

		utils.setSession("urlSrc", "");

		var utype = $routeParams.utype;
		var utypename = getUTypeName(utype);

		$scope.utype = utype.toUpperCase();

		var bcArray = [];
		bcArray.push({
			title : "Accounts",
			url : ""
		});
		bcArray.push({
			title : utypename,
			url : ""
		});
		$scope.bcArray = bcArray;

		if (!pgnum) {
			pgnum = 1;
		}
		var options = {
			limit : PAGE_LIMIT,
			offset : (pgnum - 1) * PAGE_LIMIT
		};

		var params = {
			filter : {},
			options : options,
			search : searchFilter
		};

		try {
			apiService.getCall(API_URL+"/user/list/" + utype, params).then(
					function(response) {
						if (!response.data) {
							throw "UNKNOWN ERROR";
						}

						if (response.data.error) {
							throw response.data.error;
						} else {
							$scope.users = response.data.list;

							var rowCount = response.data.rowCount;

							var prevPage = null;
							var nextPage = null;

							if (rowCount > PAGE_LIMIT) {
								prevPage = (pgnum > 1) ? pgnum - 1 : null;
								var rectillthispage = pgnum * PAGE_LIMIT;

								if (rectillthispage < rowCount) {
									nextPage = pgnum + 1;
								}
							}

							$scope.prevPage = prevPage;
							$scope.nextPage = nextPage;
							$scope.pgnum = pgnum;
							$scope.totalpages = Math
									.ceil(rowCount / PAGE_LIMIT);

							$scope.loading = false;
						}
					}, function(errResponse) {
						throw errResponse;
					});
		} catch (err) {
			$scope.loading = false;
			console.log(err);
			$window.alert(err);
		}
	};

	$scope.searchUserList = function(filter) {

		if (!utils.isActiveUser()) {
			return;
		}

		$location.search("search", JSON.stringify(filter));
		$location.search("pgnum", 1);
		$scope.loadUsersList();
	};

	$scope.changePage = function(pgnum) {
		if (!utils.isActiveUser()) {
			return;
		}

		$location.search("pgnum", pgnum);
		$scope.loadUsersList(pgnum);
	};

	$scope.navigateToNewUserDetails = function() {

		if (!utils.isActiveUser()) {
			return;
		}
		var utype = $routeParams.utype;
		utils.setSession("urlSrc", $location.url());

		$location.url("/de/users/new");
	};

	$scope.navigateToUserDetails = function(uid) {

		if (!utils.isActiveUser()) {
			return;
		}

		var utype = $routeParams.utype;
		utils.setSession("urlSrc", $location.url());

		$location.url("/de/users/details/" + uid);
	};

	$scope.readUserType = function(actiontype) {

		if (!utils.isActiveUser()) {
			return;
		}

		var utype = $routeParams.utype;
		$scope.utype = utype.toUpperCase();
		var utypename = getUTypeName(utype);

		var bcArray = [];
		bcArray.push({
			title : "Accounts",
			url : ""
		});
		bcArray.push({
			title : utypename,
			url : utils.getSession("urlSrc")
		});
		bcArray.push({
			title : (!actiontype || actiontype === "edit")
					? "Edit User Details"
					: "New User Details",
			url : ""
		});
		$scope.bcArray = bcArray;
	};

	$scope.loadUserDetails = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var selectedTab = $location.search().tab;

		if (!selectedTab) {
			selectedTab = "info";
		}

		$scope.selectedTab = selectedTab;
	};

	$scope.prepareNewUser = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		var utype = $routeParams.utype;
		var user = {
			uid : null,
			name : null,
			organization : null,
			utype : utype,
			uname : null,
			upass : null,
			temp_pwd : null,
			resetpwd : 1,
			email : null,
			emailverified : 0,
			ecode : utils.guid(),
			ecodevalidity : null,
			mobilenum : null,
			mobileverified : 0,
			mcode : null,
			mcodevalidity : null,
			active : 1,
			createdby : null,
			createdon : null,
			modifiedby : null,
			modifiedon : null
		};

		// setting the ecodevalidity to [today + 2 days]
		var ecodevalidity = new Date();
		ecodevalidity = ecodevalidity.setDate(ecodevalidity.getDate() + 2);

		user.ecodevalidity = new Date(ecodevalidity);

		user.temp_pwd = utils.randomPassword(10);
		user.upass = utils.hash(user.temp_pwd);

		$scope.user = user;
	};

	$scope.loadInfoTab = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		var uid = $routeParams.uid;
		var utype = $routeParams.utype;

		$scope.loading = true;
		try {
			apiService.getCall(API_URL+"/user/details/" + uid).then(
					function(response) {
						if (!response.data) {
							throw "UNKNOWN ERROR";
						}

						if (response.data.error) {
							throw response.data.error;
						} else {
							$scope.user = response.data.details;
							$scope.org_user = angular.copy($scope.user);
							$scope.loading = false;
						}
					}, function(errResponse) {
						throw errResponse;
					});
		} catch (err) {
			$scope.loading = false;
			console.log(err);
			$window
					.alert("Unable to fetch User Details. Please try again later");
			$location.url("/de/user/");
		}
	};

	$scope.saveInfoTab = function(user) {
		if (!utils.isActiveUser()) {
			return;
		}

		// TODO : VALIDATE INFORMATION

		if (!user.name || !user.email || !user.uname || !user.mobilenum) {
			$window.alert("Required information is missing");
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "saving";
		validationService
				.checkForUsername(user.uname)
				.then(
						function(response) {
							console.log(response);

							if (!user.uid && response.data.error) {
								$window
										.alert("Error validating username. Try again after sometime");
								$scope.eventinprogress = false;
								return;
							} else if (!user.uid && response.data.result === false) {
								$window
										.alert("Username already taken. Try some other name");
								$scope.eventinprogress = false;
								return;
							} else {

								// DO WEB API CALL TO SAVE RECORD
								var currentUser = utils.getSession("uinfo");
								try {
									user.newcreatedby = currentUser.uname;
									user.newmodifiedby = currentUser.uname;

									var postData = {
										userinfo : user
									};

									var apiUrl = API_URL + "/user/details/";

									apiUrl += (user.uid) ? "update" : "insert";

									apiService
											.postCall(apiUrl, postData)
											.then(
													function(response) {
														if (!response.data) {
															throw "UNKNOWN ERROR";
														} else if (response.data.error) {
															throw response.data.error;
														} else {

															if (!user.uid) {
																emailService
																		.registrationEmail(
																				user)
																		.then(
																				function(
																						emailResponse) {
																					$window
																							.alert("User Registration Successful");
																					$scope
																							.navigateToUserList();
																					$scope.eventinprogress = false;
																				},
																				function(
																						errEmailResponse) {
																					$window
																							.alert("User Registration Successful but could not send email");
																					$scope
																							.navigateToUserList();
																					$scope.eventinprogress = false;
																				});
															} else {
																$scope
																		.navigateToUserList();
																$scope.eventinprogress = false;

															}
														}
													}, function(errResponse) {
														throw errResponse;
													});
								} catch (err) {
									$window
											.alert("Unable to Save User Details. Please try again later.");
									console.log(err);
									$scope.eventinprogress = false;
								}

							}
						},
						function(errResponse) {
							$window
									.alert("Unable to validate username. Try again after sometime");
							$scope.eventinprogress = false;
							return;
						});
	};

	$scope.resendValidationEmail = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "emailing";

		var user = angular.copy($scope.org_user);

		user.emailverified = 0;
		user.ecode = utils.guid();

		var ecodevalidity = new Date();
		ecodevalidity = ecodevalidity.setDate(ecodevalidity.getDate() + 2);
		user.ecodevalidity = new Date(ecodevalidity);

		// save the new ecode

		var currentUser = utils.getSession("uinfo");
		try {
			user.newmodifiedby = currentUser.uname;

			var postData = {
				userinfo : user
			};

			var apiUrl = API_URL + "/user/details/update";

			apiService
					.postCall(apiUrl, postData)
					.then(
							function(response) {
								if (!response.data) {
									throw "UNKNOWN ERROR";
								} else if (response.data.error) {
									throw response.data.error;
								} else {
									$scope.user.emailverified = user.emailverified;
									$scope.user.ecode = user.ecode;
									$scope.user.ecodevalidity = user.ecodevalidity;

									$scope.org_user.emailverified = user.emailverified;
									$scope.org_user.ecode = user.ecode;
									$scope.org_user.ecodevalidity = user.ecodevalidity;

									emailService
											.validateEmailAddress(user)
											.then(
													function(response) {
														$scope.eventinprogress = false;
														$window
																.alert("Email Sent");
														$scope.eventinprogress = false;
													},
													function(errResponse) {
														$window
																.alert("New Verification Code saved but could not send email");
														console
																.log(errResponse);
														$scope.eventinprogress = false;
													});
								}
							}, function(errResponse) {
								throw errResponse;
							});
		} catch (err) {
			$window
					.alert("Unable to Send Email Verification Link. Please try again later.");
			console.log(err);
			$scope.eventinprogress = false;
		}
	};

	$scope.resetPassword = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "resetpwd";

		var user = angular.copy($scope.org_user);

		user.temp_pwd = utils.randomPassword(10);
		user.upass = utils.hash(user.temp_pwd);
		user.resetpwd = 1;

		try {
			var currentUser = utils.getSession("uinfo");
			user.newmodifiedby = currentUser.uname;

			var postData = {
				userinfo : user
			};

			var apiUrl = API_URL + "/user/details/update";

			apiService
					.postCall(apiUrl, postData)
					.then(
							function(response) {
								if (!response.data) {
									throw "UNKNOWN ERROR";
								} else if (response.data.error) {
									throw response.data.error;
								} else {
									$scope.user.temp_pwd = user.temp_pwd;
									$scope.user.upass = user.upass;
									$scope.user.resetpwd = user.resetpwd;

									$scope.org_user.temp_pwd = user.temp_pwd;
									$scope.org_user.upass = user.upass;
									$scope.org_user.resetpwd = user.resetpwd;

									emailService
											.passwordResetEmail(user)
											.then(
													function(response) {
														$scope.eventinprogress = false;
														$window
																.alert("Email Sent");
														$scope.eventinprogress = false;
													},
													function(errResponse) {
														$window
																.alert("Password Reset successful but could not send email");
														console
																.log(errResponse);
														$scope.eventinprogress = false;
													});
								}
							}, function(errResponse) {
								throw errResponse;
							});
		} catch (err) {
			$window
					.alert("Unable to Reset the Password. Please try again later.");
			console.log(err);
			$scope.eventinprogress = false;
		}
	};

	$scope.editEmail = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var obj = {
			user : angular.copy($scope.org_user),
			org_user : $scope.org_user
		};
		utils.showPopup("./views/de/users/popup-email-edit.html", obj, "",
				"deCtlr");
	};

	$scope.updateEmail = function(dialogData) {
		if (!utils.isActiveUser()) {
			return;
		}

		var user = dialogData.user;
		var org_user = dialogData.org_user;

		console.log("1");

		if (user.email === org_user.email) {
			// NO Action required
			$scope.closeThisDialog(dialogData.ngDialogId);
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "updatingemail";

		user.emailverified = 0;
		user.ecode = utils.guid();

		var ecodevalidity = new Date();
		ecodevalidity = ecodevalidity.setDate(ecodevalidity.getDate() + 2);
		user.ecodevalidity = new Date(ecodevalidity);

		console.log("2");
		// save the new ecode

		var currentUser = utils.getSession("uinfo");
		try {
			user.newmodifiedby = currentUser.uname;

			var postData = {
				userinfo : user
			};

			var apiUrl = API_URL + "/user/details/update";

			console.log("3");
			apiService
					.postCall(apiUrl, postData)
					.then(
							function(response) {
								if (!response.data) {
									throw "UNKNOWN ERROR";
								} else if (response.data.error) {
									throw response.data.error;
								} else {
									console.log("4");
									emailService
											.validateEmailAddress(user)
											.then(
													function(response) {
														console.log("5");
														$scope.eventinprogress = false;
														$scope
																.closeThisDialog(dialogData.ngDialogId);
														$route.reload();
													},
													function(errResponse) {
														console.log("6");
														$window
																.alert("New Verification Code saved but could not send email");
														console
																.log(errResponse);
														$scope.eventinprogress = false;
														$scope
																.closeThisDialog(dialogData.ngDialogId);
														$route.reload();
													});
								}
							}, function(errResponse) {
								throw errResponse;
							});
		} catch (err) {
			$window.alert("Unable to Update Email. Please try again later.");
			console.log(err);
			$scope.eventinprogress = false;
		}

	};

	$scope.resetInfoTab = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		$scope.user = angular.copy($scope.org_user);
	};

	$scope.switchTab = function(tabname) {
		if (!utils.isActiveUser()) {
			return;
		}

		$location.search("tab", tabname);
		$scope.selectedTab = tabname;
	};
}
