lpApp.controller('menuCtlr', MenuCtlr);

MenuCtlr.$inject = ['$scope', '$rootScope', '$routeParams', '$route',
		'$location', '$q', 'utils', 'apiService'];

function MenuCtlr($scope, $rootScope, $routeParams, $route, $location, $q,
		utils, apiService) {

	$scope.loadUserMenu = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var currentUser = utils.getSession("uinfo");
		$scope.user = currentUser;
		$scope.utype = currentUser.utype.toLowerCase();
		$scope.urole = currentUser.role.toLowerCase();
		
	};

	$scope.navigate = function(url) {
		
		utils.setSession("urlSrc", "")

		if ($location.path() === url) {
			$route.reload();
		} else if (url === '/logoff') {
			utils.clearSession();
			$location.url("/login");
		} else {
			$location.url(url);
		}
	};

	$scope.gotoUserHome = function() {
		if (!utils.isActiveUser()) {
			return;
		}
		
		var url = $location.url().toLowerCase();
		
		if (url.startsWith('/login')) {
			url = "/login";
		} else if (url.startsWith('/su')) {
			url = "/su";
		} else if (url.startsWith('/lg')) {
			url = "/lg";
		} else if (url.startsWith('/sp')) {
			url = "/sp";
		} else if (url.startsWith('/cl')) {
			url = "/cl";
		} else if (url.startsWith('/de')) {
			url = "/de";
		}
		
		$location.url(url);
	}

	$scope.setActiveMenu = function() {

		var url = $location.url().toLowerCase();

		if (!url.startsWith('/login') && !utils.isActiveUser()) {
			return;
		}
		
		var currentUser = utils.getSession("uinfo");

		$scope.user = currentUser;
		
		if (url == '/login') {
			$scope.activeMenu = "login";
		} else if (url.startsWith('/login/register')) {
			$scope.activeMenu = "register";
		} else if (url.startsWith("/su")) {
			if (url.startsWith("/su/leadmgmt")) {
				$scope.activeMenu = "leadmgmt";
			} else if (url.startsWith("/su/accounts")) {
				$scope.activeMenu = "accounts";
			} else if (url.startsWith("/su/config")) {
				$scope.activeMenu = "config";
			} else if (url.startsWith("/su/profile")) {
				$scope.activeMenu = "profile";
			} else {
				$scope.activeMenu = "home";
			}
		} else if (url.startsWith("/lg")) {
			if (url.startsWith("/lg/leads")) {
				$scope.activeMenu = "leads";
			} else if (url.startsWith("/lg/profile")) {
				$scope.activeMenu = "profile";
			} else {
				$scope.activeMenu = "leads";
			}
		} else if (url.startsWith("/sp")) {
			if (url.startsWith("/sp/leads")) {
				$scope.activeMenu = "leads";
			} else if (url.startsWith("/sp/profile")) {
				$scope.activeMenu = "profile";
			} else {
				$scope.activeMenu = "home";
			}
		} else if (url.startsWith("/cl")) {

		} else if (url.startsWith("/de")) {
			if (url.startsWith("/de/users")) {
				$scope.activeMenu = "users";
			} else if (url.startsWith("/de/config")) {
				$scope.activeMenu = "config";
			} else {
				$scope.activeMenu = "home";
			}
		} else {

		}

	};

}
