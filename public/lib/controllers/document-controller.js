lpApp.controller('documentCtlr', DocumentCtlr);

DocumentCtlr.$inject = ['$scope', '$q', '$routeParams', '$route', '$location',
		'$window', 'utils', 'apiService', 'emailService', 'validationService',
		'Upload'];

function DocumentCtlr($scope, $q, $routeParams, $route, $location, $window,
		utils, apiService, emailService, validationService, Upload) {

	var API_URL = utils.getSession("apiUrl");
	
	$scope.loadMyDocuments = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var uinfo = utils.getSession("uinfo");
		$scope.uname = uinfo.uname;

		var url = $location.url().toLowerCase();

		var manage = {
			add : false,
			edit : false,
			download : true,
			utype : '',
			user_uid : '',
			allowverification : false
		};

		if (url.startsWith('/su/profile')) {
			manage = {
				add : true,
				edit : true,
				download : true,
				utype : 'su',
				user_uid : uinfo.uid,
				allowverification : false
			};
		} else if (url.startsWith('/sp/profile')) {
			manage.add = true;
			manage.download = true;
			manage.utype = 'sp';
			manage.user_uid = uinfo.uid;
		} else if (url.startsWith('/lg/profile')) {
			manage = {
				add : true,
				edit : true,
				download : true,
				utype : 'lg',
				user_uid : uinfo.uid,
				allowverification : false
			};
		} else if (url.startsWith('/su/accounts/lg/details/')) {
			manage = {
				add : true,
				edit : true,
				download : true,
				utype : 'lg',
				user_uid : $routeParams.uid,
				allowverification : true
			};
		} else if (url.startsWith('/su/accounts/sp/details/')) {
			manage = {
				add : true,
				edit : true,
				download : true,
				utype : 'sp',
				user_uid : $routeParams.uid,
				allowverification : true
			};
		}

		$scope.manage = manage;
		loadDocs(manage.user_uid);
	};

	var loadDocs = function(user_uid) {

		$scope.loadingDocs = true;
		apiService
				.getCall(API_URL+"/documents/list/" + user_uid)
				.then(
						function(response) {
							if (!response.data) {
								alert("Unknown Error loading Documents List. Please try again.");
							} else if (response.data.error) {
								console.log(response.data.error);
								alert("Error loading Documents List. Please try again.");
							} else {
								$scope.lstDocuments = response.data.list;

							}
							$scope.loadingDocs = false;
						},
						function(err) {
							$scope.loadingDocs = false;
							console.log(err);
							alert("Error loading Documents List. Please try after some time.");
						});

	}

	$scope.addEditDocument = function(doc) {
		if (!utils.isActiveUser()) {
			return;
		}

		var uinfo = utils.getSession("uinfo");

		var newDoc = {
			user_uid : $scope.manage.user_uid,
			doc_type_uid : '',
			doc_title : '',
			doc_path : '',
			verified : 0,
			createdby : uinfo.uname,
			newcreatedby : uinfo.uname,
			modifiedby : uinfo.uname,
			newmodifiedby : uinfo.uname
		};

		var obj = {
			utype : $scope.manage.utype,
			doc : (doc) ? angular.copy(doc) : newDoc,
			allowverification : $scope.manage.allowverification

		};
		utils.showPopup("./views/documents/add-edit-document-popup.html",
				obj, "", "documentCtlr");
	};

	$scope.loadDocTypes = function(utype) {

		$scope.loadingDocTypes = true;
		apiService
				.getCall(API_URL+"/documents/types/" + utype)
				.then(
						function(response) {
							if (!response.data) {
								alert("Error loading Document Types");
								loadingDocTypes = false;
								return;
							}

							if (response.data.error) {
								console.log(response.data.error);
								loadingDocTypes = false;
								alert("Error loading Documents List. Please try again.");
							} else {
								$scope.docTypes = response.data.list;
								$scope.loadingDocTypes = false;
							}
						},
						function(err) {
							$scope.loadingDocTypes = false;
							console.log(err);
							alert("Error loading Documents List. Please try after some time.");
						});
	};

	$scope.setDocumentTitle = function() {
		console.log($scope.ngDialogData.doc_type_uid);
	};

	$scope.saveDocument = function(ngDialogData) {

		var doc = angular.copy(ngDialogData.doc);
		var file = ngDialogData.file;
		delete doc.doc_type;

		$scope.savingDoc = true;
		if (doc.uid && !ngDialogData.file) {
			// SAVE RECORD WITHOUT FILE
			$scope
					.saveDBRecord(doc)
					.then(
							function() {
								$scope.savingDoc = false;
								$scope.closeThisDialog();
								$route.reload();
							},
							function(err) {
								$scope.savingDoc = false;
								$window
										.alert("Error Saving record. Please try again after sometime");
								console.log(err);
							});
		} else if (ngDialogData.file) {
			// ADD/EDIT WITH FILE
			$scope
					.uploadFile(doc, file)
					.then(
							function(response) {
								console.log(response);
								$scope.savingDoc = false;
								$scope.closeThisDialog();
								$route.reload();
							},
							function(err) {
								$scope.savingDoc = false;
								$window
										.alert("Error uploading file & saving record. Please try again after sometime");
							});
		} else {
			alert("Please attach the file before saving");
			$scope.savingDoc = false;
		}
	};

	$scope.saveDBRecord = function(doc) {
		var defer = $q.defer();

		var url = API_URL+"/documents/";

		if (doc.uid) {
			url += "update/" + doc.uid;
		} else {
			url += "insert";
		}
		delete doc.doc_type;

		apiService.postCall(url, {
			data : doc
		}).then(function(response) {
			if (!response.data) {
				defer.reject({
					error : "Error Saving Document"
				});
			} else if (response.data.error) {
				console.log(response.data.error);
				defer.reject({
					error : "Error Saving Document. Please try again."
				});
			} else {
				defer.resolve();
			}
		}, function(err) {
			console.log(err);
			defer.reject({
				error : "Error saving Document. Please try after some time."
			});
		});

		return defer.promise;
	};

	$scope.uploadFile = function(doc, file) {
		var defer = $q.defer();

		var uinfo = utils.getSession("uinfo");

		if (!doc.uid) {
			var ext = file.name.split('.')[file.name.split('.').length - 1]
			doc.doc_path = Date.now() + "." + ext;
		}

		var postData = {
			url : './api/web/documents/upload',
			data : {
				doc : doc,
				file : file
			}
		};

		Upload.upload(postData).then(function(resp) { // upload function
			// returns a promise
			console.log(resp);
			if (resp.data.error) { // validate success
				defer.reject({
					error : "Error Uploading file. Please try again."
				});
			} else {
				defer.resolve();
			}
		}, function(err) { // catch error
			console.log(err);
			defer.reject({
				error : "Error Uploading file. Please try after sometime."
			});
		}, function(evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		});
		return defer.promise;
	};

	
	$scope.removeDocument = function(item) {

		var result = $window
				.confirm("Are you sure you want to remove this document?");

		if (result) {
			apiService
					.postCall('./api/web/documents/remove/' + item.uid)
					.then(
							function(response) {
								if (!response.data) {
									alert("Error Removing Document");
								} else if (response.data.error) {
									console.log(response.data.error);
									alert("Error Removing Document. Please try again.");
								} else {
									$route.reload();
								}
							}, function(err) {
								console.log(err);
							});
		}
	};
}
