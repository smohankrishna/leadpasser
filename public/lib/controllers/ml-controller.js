lpApp.controller('masterlistCtlr', MasterListCtlr);

MasterListCtlr.$inject = ['$scope', '$routeParams', '$route', '$location',
		'$window', 'utils', 'apiService', 'emailService', 'validationService',
		'masterListService'];

function MasterListCtlr($scope, $routeParams, $route, $location, $window,
		utils, apiService, emailService, validationService, mlService) {

	var PAGE_LIMIT = 10;
	var API_URL = utils.getSession("apiUrl");

	$scope.loadMasterLists = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		$scope.bcArray = [];
		$scope.bcArray.push({
			title : "Config",
			url : ""
		});
		$scope.bcArray.push({
			title : "Master Lists",
			url : ""
		});

		$scope.loading = true;
		var options = {
			limit : 999,
			offset : 0
		};

		var params = {
			filter : {
				parent_uid : null
			},
			options : options,
		};

		var list_uid = $location.search().list;

		console.log(list_uid);

		mlService.getAllMasterLists(params).then(function(response) {
			$scope.masterlists = response.list;
			$scope.loading = false;

			$scope.list_uid = list_uid;

			$scope.loadListItems(list_uid);

		}, function(errResponse) {
			$scope.loading = false;
			console.log(err);
			$window.alert(err);
		});

	};
	
	$scope.loadListItems = function(list_uid) {

		if (!utils.isActiveUser()) {
			return;
		}
		$location.search("list", list_uid);

		if (!list_uid) {
			$scope.listItems = null;
			return;
		}

		$scope.loading = true;
		var pgnum = Number($location.search().pgnum);

		if (!pgnum) {
			pgnum = 1;
		}
		var options = {
			limit : PAGE_LIMIT,
			offset : (pgnum - 1) * PAGE_LIMIT
		};

		var params = {
			filter : {
				parent_uid : list_uid
			},
			options : options
		};

		mlService.getListItems(params).then(function(response, rowCount) {
			$scope.listItems = response;
			var rowCount = rowCount;

			var prevPage = null;
			var nextPage = null;

			if (rowCount > PAGE_LIMIT) {
				prevPage = (pgnum > 1) ? pgnum - 1 : null;
				var rectillthispage = pgnum * PAGE_LIMIT;

				if (rectillthispage < rowCount) {
					nextPage = pgnum + 1;
				}
			}

			$scope.prevPage = prevPage;
			$scope.nextPage = nextPage;
			$scope.pgnum = pgnum;
			$scope.totalpages = Math.ceil(rowCount / PAGE_LIMIT);

			$scope.loading = false;
		}, function(errResponse) {
			$scope.loading = false;
			console.log(err);
			$window.alert(err);
		});
	};

	$scope.changePage = function(pgnum) {
		if (!utils.isActiveUser()) {
			return;
		}

		$location.search("pgnum", pgnum);
		$scope.loadProperties(pgnum);
	};

	$scope.addList = function(list_uid) {
		if (!utils.isActiveUser()) {
			return;
		}

		var list = {
			uid : null,
			code : null,
			description : null,
			parent_uid : list_uid,
			seq : 999,
			active : 1,
			createdby : null,
			createdon : null,
			modifiedby : null,
			modifiedon : null
		}

		var obj = {
			list : list,
			mode : 'add'
		};
		utils.showPopup("./views/masterlist/popup-list-edit.html", obj, "",
				"masterlistCtlr");
	};

	$scope.editList = function(list) {
		if (!utils.isActiveUser()) {
			return;
		}

		var obj = {
			list : angular.copy(list),
			mode : 'edit'
		};
		utils.showPopup("./views/masterlist/popup-list-edit.html", obj, "",
				"masterlistCtlr");
	}

	$scope.saveList = function(dialogData) {
		var list = dialogData.list;

		if (!utils.isActiveUser()) {
			return;
		}

		// TODO : VALIDATE INFORMATION

		if (!list.code || !list.description) {
			$window.alert("Required information is missing");
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "saving";
		validationService
				.checkForListCode(list.code, list.parent_uid)
				.then(
						function(response) {
							if (!list.uid && response.data.error) {
								$window
										.alert("Error validating Name. Try again after sometime");
								$scope.eventinprogress = false;
								return;
							} else if (!list.uid
									&& response.data.result === false) {
								$window
										.alert("Code already taken. Try some other code");
								$scope.eventinprogress = false;
								return;
							} else {

								// DO WEB API CALL TO SAVE RECORD
								var currentUser = utils.getSession("uinfo");

								list.newcreatedby = currentUser.uname;
								list.newmodifiedby = currentUser.uname;

								mlService
										.saveList(list)
										.then(
												function() {
													$scope.eventinprogress = false;
													$scope
															.closeThisDialog(dialogData.ngDialogId);
													$route.reload();
												},
												function(err) {
													$window
															.alert("Unable to Save List. Please try again later.");
													console.log(err);
													$scope.eventinprogress = false;

												});
							}
						},
						function(errResponse) {
							$window
									.alert("Unable to validate List Name. Try again after sometime");
							$scope.eventinprogress = false;
							return;
						});
	};
	
	$scope.loadSingleMasterList = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		var bcArray = [];
		bcArray.push({
			title : "Config",
			url : ""
		});
		$scope.bcArray = bcArray;


		$scope.loading = true;
		var options = {
			limit : 999,
			offset : 0
		};

		var listCode = $location.search().code;
		
		if(!listCode){
			utils.showAlert("Invalid List Code")
			return;
		}
		
		mlService.getItemDetailsByCode(listCode).then(function(response){
			$scope.list_uid = response.uid;
			
			$scope.bcArray.push({
				title : response.description,
				url : ""
			});
			
			mlService.getListItemsByCode(listCode).then(function(response){
				$scope.listItems = response;
				$scope.loading = false;
			}, function(err){
				$scope.loading = false;
				utils.showAlert("Error loading list. Please try after sometime");
				console.log(err);
			});
			
			
			
		}, function(err){
			utils.showAlert("Error Fetching List Details");
			console.log(err);
		});
		
		
		

	};


}
