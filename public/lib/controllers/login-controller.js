lpApp.controller('loginCtlr', LoginCtlr);

LoginCtlr.$inject = ['$scope', '$rootScope', '$routeParams', '$route',
    '$location', '$q', '$window', 'utils', 'apiService',
    'validationService', 'emailService', 'userSvc'];

function LoginCtlr($scope, $rootScope, $routeParams, $route, $location,
        $q, $window, utils, apiService, validationService, emailService,
        userSvc) {

    var API_URL = utils.getSession("apiUrl");

    $scope.loginInfo = {
        username: '',
        passwd: ''
    };

    $scope.init = function () {
        utils.clearSession();

        utils.setSession("apiUrl", $rootScope.api_url);
        API_URL = utils.getSession("apiUrl");
    };

    $scope.authenticate = function () {
        $scope.loginInfo.error = "";
        if (!$scope.loginInfo || !$scope.loginInfo.username
                || !$scope.loginInfo.passwd) {
            $scope.loginInfo.error = "Username/Password are manadatory";
            return;
        }
        var params = {
            username: $scope.loginInfo.username,
            passwd: utils.hash($scope.loginInfo.passwd)
        };

        $scope.loading = true;
        try {
            apiService.getCall(API_URL + '/user/authenticate', params).then(function (response) {
                $scope.loading = false;
                if (response.data.error
                        && response.data.error.code === "AUTHORIZATION_ERROR") {
                    $scope.loginInfo.error = "Invalid Username/Password combination";
                } else if (response.data.error) {
                    $scope.loginInfo.error = "Problem in authenticating user. Please try after sometime.";
                    console.log(response.data.error);
                } else {
                    $scope.loginInfo.error = "Authenticated : "
                            + response.data.authenticated;

                    var uinfo = response.data.userinfo;
                    delete uinfo.upass;
                    utils.setSession("uinfo", uinfo);
                    utils.setSession("authtoken", response.headers("app-auth-token"));

                    if (uinfo.utype.toUpperCase() === 'SU') {
                        $location.url("/su");
                    } else if (uinfo.utype.toUpperCase() === 'LG') {
                        $location.url("/lg");
                    } else if (uinfo.utype.toUpperCase() === 'SP') {
                        $location.url("/sp");
                    } else if (uinfo.utype.toUpperCase() === 'CL') {
                        $location.url("/cl");
                    } else if (uinfo.utype.toUpperCase() === 'DE') {
                        $location.url("/de");
                    }
                }
            }, function (err) {
                $scope.loading = false;
                utils.showAlert("Problem in authenticating the user. Please try again");
                console.log(err);
            });
        } catch (err) {
            $scope.loading = false;
            alert("Error occurred while doing the operation\n\n" + err);
        }
    };

    $scope.navigateToRegistration = function (utype) {
        console.log("navigateToRegistration");
        $location.url("/login/register/" + utype);
    };

    $scope.navigateToLogin = function () {
        $location.url("/login");
    };

    $scope.prepareForRegistration = function () {
        var utype = $routeParams.utype;

        var user = {
            uid: null,
            name: null,
            organization: null,
            utype: utype,
            uname: null,
            upass: null,
            temp_pwd: null,
            resetpwd: 0,
            email: null,
            emailverified: 0,
            ecode: utils.guid(),
            ecodevalidity: null,
            mobilenum: null,
            mobileverified: 0,
            mcode: null,
            mcodevalidity: null,
            active: 1,
            createdby: null,
            createdon: null,
            modifiedby: null,
            modifiedon: null,
        };

        // setting the ecodevalidity to [today + 2 days]
        var ecodevalidity = new Date();
        ecodevalidity = ecodevalidity.setDate(ecodevalidity.getDate() + 2);

        user.ecodevalidity = new Date(ecodevalidity);

        $scope.user = user;
        $scope.maxDoB = new Date();
        $scope.maxDoB = new Date($scope.maxDoB.getFullYear() - 18,
                $scope.maxDoB.getMonth(), $scope.maxDoB.getDate());
    };

    $scope.saveRegistration = function (user) {
        // TODO : VALIDATE INFORMATION

        $scope.formerror = "";
        $scope.eventinprogress = true;
        $scope.eventmessage = "registering";

        if (!user.name || !user.dob || !user.mobilenum || !user.uname
                || !user.passwd1 || !user.passwd2) {
            $scope.formerror = "Required information is missing";
            $scope.eventinprogress = false;
            return;
        } else if (user.passwd1 != user.passwd2) {
            user.passwd1 = null;
            user.passwd2 = null;
            $scope.formerror = "Passwords do not match";
            $scope.eventinprogress = false;
            return;
        }

        var newUser = angular.copy(user);
        if (newUser.dob) {
            newUser.dob = new Date(user.dob);
        }

        newUser.upass = utils.hash(newUser.passwd1);
        delete newUser.passwd1;
        delete newUser.passwd2;

        newUser.newcreatedby = "self";
        newUser.newmodifiedby = "self";

        newUser.mcode = utils.randomOTP();
        newUser.mobileverified = 0;

        var mcodevalidity = new Date();
        mcodevalidity = mcodevalidity
                .setMinutes(mcodevalidity.getMinutes() + 20);
        newUser.mcodevalidity = new Date(mcodevalidity);

        userSvc
                .saveUserDetails(newUser)
                .then(
                        function (msg) {
                            // open OTP popup
                            $scope.eventinprogress = false;

                            if (newUser.mobilenum) {
                                var obj = {
                                    userotp: null,
                                    user: newUser
                                };
                                utils
                                        .showPopup(
                                                "./views/login/popup-mobile-verify.html",
                                                obj, "", "loginCtlr");
                            }

                            console.log(msg);
                        },
                        function (errResponse) {
                            $scope.eventinprogress = false;

                            if (errResponse) {
                                $window.alert(errResponse);
                                console.log(errResponse);
                            } else {
                                $window
                                        .alert("Error registering your account. Kindly try again later.");
                            }
                        });
    };

    $scope.verifyOTP = function (dialogData) {
        var userotp = dialogData.userotp;
        var user = dialogData.user;

        if (userotp == user.mcode) {

            // CHECK OTP Expiry;

            var dt = new Date();
            if (user.mcodevalidity < dt) {
                $window
                        .alert("OTP Expired. Please Login to your account and validate your mobile again");
                $scope.closeThisDialog(dialogData.ngDialogId);
                $scope.navigateToLogin();
                return;
            }

            $scope.eventinprogress = true;
            $scope.eventmessage = "saving";

            userSvc.getDetailsByUsername(user.uname).then(function (userObj) {

                userObj.mcode = null;
                userObj.mcodevalidity = null;
                userObj.mobileverified = 1;

                userSvc.saveUserDetails(userObj).then(function (message) {
                    if (message) {
                        $window.alert(message);
                    } else {
                        $window
                                .alert("Mobile Number Verified Successfully. !");
                    }

                    $scope.eventinprogress = false;
                    $scope
                            .closeThisDialog(dialogData.ngDialogId);
                    $scope.navigateToLogin();
                }, function (errMessage) {

                    $scope.eventinprogress = false;

                    if (errMessage) {
                        $window
                                .alert(errMessage);
                        console.log(errMessage);
                    } else {
                        $window
                                .alert("OTP verified but unable to update in system. Please login and verify your profile.");
                    }
                });
            }, function (err) {
                console.log(err);
                $window
                        .alert("Error fetching user information during OTP verification process");
            });
        } else {
            $window.alert('Invalid OTP. Please try again.');
        }
    };

    $scope.doVerify = function () {
        utils.setSession("apiUrl", $rootScope.api_url);
        API_URL = utils.getSession("apiUrl");

        var uname = $location.search().uname;
        var ecode = $location.search().ecode;

        if (!uname || !ecode) {
            alert("Invalid call");
            return
        }

        validationService.validateEmail(uname, ecode).then(function (response) {
            if (response.valid == true) {
                var user = response.user;
                user.emailverified = 1;
                user.ecode = null;
                user.ecodevalidity = null;

                console.log(user);

                var postData = {
                    userinfo: user
                };

                var apiUrl = API_URL + "/user/details/update";

                apiService.postCall(apiUrl, postData).then(function (response) {
                    if (!response.data) {
                        alert("Email verfied but unable to update the record. Please try again after sometime");
                    } else if (response.data.error) {
                        alert("Email verfied but unable to update the record. Please try again after sometime");
                    } else {
                        alert("Email verfied");
                        $scope
                                .navigateToLogin();
                    }
                }, function (errResponse) {
                    alert("Email verfied but unable to update the record. Please try again after sometime");
                    console.log(errResponse);
                });

            } else {
                alert("Email verification link is no more valid.");
                console.log(response);
            }
        }, function (errResponse) {
            console.log(errResponse);
        });
    }

    $scope.cancelOTPVerification = function () {
        $window
                .alert("User registration is successful, but mobile verification is pending. Please login to your account to verify the mobile number");
        $scope.closeThisDialog($scope.ngDialogData.ngDialogId);
        $location.url("/login");
    };

    $scope.showForgotPassword = function () {
        var obj = {
            uname: ""
        };
        utils.showPopup("./views/login/popup-forgot-pwd.html", obj, "",
                "loginCtlr");
    };

    $scope.requestResetPassword = function (dialogData) {
        var uname = dialogData.uname;

        $scope.eventmessage = "resetpwd";
        $scope.eventinprogress = true;

        userSvc.forgotPassword(uname).then(function (response, err) {
            $scope.eventinprogress = false;
            if (err) {
                console.log(err);
                $window.alert(err.error);
                $scope.closeThisDialog();
            } else {
                $window.alert("New Password has been sent to registered email address and mobile number");
                $scope.closeThisDialog();
            }

        }, function (err) {
            $scope.eventinprogress = false;
            if (err.error_code) {
                $window.alert(err.error);
                console.log(err);
            } else {
                $window.alert(err);
                console.log(err);
            }

        });
    };

    $scope.switchTab = function (tabname) {
        $location.search("tab", tabname);
        $scope.selectedTab = tabname;
    };

}
;
