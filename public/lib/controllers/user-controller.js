lpApp.controller('userCtlr', UserCtlr);

UserCtlr.$inject = ['$scope', '$routeParams', '$route', '$location', '$window',
		'utils', 'apiService', 'userSvc', 'emailService', 'smsService'];

function UserCtlr($scope, $routeParams, $route, $location, $window, utils,
		apiService, userSvc, emailService, smsService) {

	var API_URL = utils.getSession("apiUrl");
	
	$scope.init = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var currentUser = utils.getSession("uinfo");
		$scope.user = currentUser;
		$scope.utype = currentUser.utype.toLowerCase();

		var bcArray = [];
		bcArray.push({
			title : "Profile",
			url : ""
		});
		bcArray.push({
			title : "My Profile",
			url : ""
		});
		$scope.bcArray = bcArray;
		
		var qry = $location.search();
		if(qry && qry.tab){
			$scope.switchTab(qry.tab);
		}else{
			$scope.switchTab("info");
		}
		
		
		
	};

	$scope.loadInfoTab = function() {

		if (!utils.isActiveUser()) {
			return;
		}

		var currentUser = utils.getSession("uinfo");

		var uid = currentUser.uid;
		var utype = currentUser.utype;

		$scope.loading = true;
		try {
			apiService.getCall(API_URL+"/user/details/" + uid).then(
					function(response) {
						if (!response.data) {
							throw "UNKNOWN ERROR";
						}
						if (response.data.error) {
							throw response.data.error;
						} else {
							$scope.user = response.data.details;
							if ($scope.user.dob) {
								$scope.user.dob = new Date($scope.user.dob);
							}
							
							$scope.maxDoB = new Date();
							$scope.maxDoB = new Date(
								      $scope.maxDoB.getFullYear()-18,
								      $scope.maxDoB.getMonth(),
								      $scope.maxDoB.getDate()); 

							$scope.org_user = angular.copy($scope.user);
							$scope.loading = false;
						}
					}, function(errResponse) {
						throw errResponse;
					});
		} catch (err) {
			$scope.loading = false;
			console.log(err);
			utils.showAlert("Unable to fetch profile details. Please try again later");
			// $location.url("/lp/accounts/" + utype);
		}
	};

	$scope.saveInfoTab = function(user) {
		if (!utils.isActiveUser()) {
			return;
		}

		// TODO : VALIDATE INFORMATION

		if (!user.name || !user.uname || !user.mobilenum) {
			utils.showAlert("Required information is missing");
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "saving";
		
		var newUser = angular.copy(user);
		if(newUser.dob){
			newUser.dob = new Date(user.dob);
		}
		
		userSvc.saveUserDetails(newUser).then(function(message) {
			if (message) {
				utils.showAlert(message);
			} else {
				utils.showAlert("Profile saved successfully");
			}
			$scope.eventinprogress = false;
			$route.reload();

		}, function(errMessage) {
			$scope.eventinprogress = false;
			utils.showAlert(errMessage);
			console.log(errMessage);
		});
	};

	$scope.resendValidationEmail = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "emailing";

		var user = angular.copy($scope.org_user);

		user.emailverified = 0;
		user.ecode = utils.guid();

		var ecodevalidity = new Date();
		ecodevalidity = ecodevalidity.setDate(ecodevalidity.getDate() + 2);
		user.ecodevalidity = new Date(ecodevalidity);

		userSvc
				.saveUserDetails(user)
				.then(
						function(message) {
							if (message) {
								utils.showAlert(message);
							}

							userSvc
									.emailChangeNotification(user)
									.then(
											function(msg) {
												$scope.eventinprogress = false;
												if (msg) {
													utils.showAlert(msg);
												} else {
													$window
															.alert("New email verification link sent to registered email address.");
												}
												$route.reload();

											},
											function(err) {
												$scope.eventinprogress = false;
												console.log(err);
												$window
														.alert("New email verification link prepared but could not send email. Kindly retry.");
												$route.reload();
											});
						},
						function(err) {
							$scope.eventinprogress = false;
							console.log(err);
							// utils.showAlert(err);
							$window
									.alert("Unable to send Email Verification Link. Please try again later.");
						});
	};

	$scope.editMobile = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var obj = {
			user : angular.copy($scope.org_user)
		};
		utils.showPopup("./views/profile/popup-mobile-edit.html", obj, "",
				"userCtlr");
	}

	$scope.updateMobile = function(dialogData) {
		if (!utils.isActiveUser()) {
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "updatingmobile";

		var user = dialogData.user;// angular.copy($scope.org_user);

		user.mobileverified = 0;
		user.mcode = utils.randomOTP();

		var mcodevalidity = new Date();
		console.log(mcodevalidity);
		mcodevalidity = mcodevalidity
				.setMinutes(mcodevalidity.getMinutes() + 20);
		user.mcodevalidity = new Date(mcodevalidity);

		// save the new mcode

		userSvc
				.saveUserDetails(user)
				.then(
						function(message) {
							if (message) {
								utils.showAlert(message);
							}

							userSvc
									.mobileChangeText(user)
									.then(
											function(response) {
													var obj = {
														userotp : null,
														user : user
													};
													$scope.closeThisDialog(dialogData.ngDialogId);
													utils
															.showPopup(
																	"./views/profile/popup-mobile-verify.html",
																	obj, "",
																	"userCtlr");
													$scope.eventinprogress = false;
//												} else { // Unable to send SMS
//													$scope.eventinprogress = false;
//													$window
//															.alert("Unable to send OTP. Please try again later.");
//													console.log(err);
//												}
//
											},
											function(err) {
												$scope.eventinprogress = false;
												$window
														.alert("Unable to send OTP. Please try again later.");
												console.log(err);
											});
						},
						function(err) {
							$scope.eventinprogress = false;
							$window
									.alert("Unable to send OTP. Please try again later.");
							console.log(err);
						});
	};

	$scope.verifyMobile = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var obj = {
			userotp : null,
			user : angular.copy($scope.org_user)
		};
		utils.showPopup("./views/profile/popup-mobile-verify.html", obj, "",
				"userCtlr");

		console.log(obj);
	}

	$scope.verifyOTP = function(dialogData) {
		var userotp = dialogData.userotp;
		var user = dialogData.user;

		if (userotp == user.mcode) {
			$scope.eventinprogress = true;
			$scope.eventmessage = "saving";
			
			user.mcode = null;
			user.mcodevalidity = null;
			user.mobileverified = 1;

			userSvc.saveUserDetails(user).then(function(message) {
				if (message) {
					utils.showAlert(message);
				}else{
					utils.showAlert("Mobile Number Verified Successfully. !");
				}

				$scope.eventinprogress = false;
				$scope.closeThisDialog(dialogData.ngDialogId);
				$route.reload();

			}, function(errMessage) {
				$scope.eventinprogress = false;
				utils.showAlert(errMessage);
				console.log(errMessage);
			});
		} else {
			utils.showAlert('Invalid OTP');
		}
	};

	$scope.changePassword = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var obj = {
			user : angular.copy($scope.org_user)
		};
		utils.showPopup("./views/profile/popup-change-password.html", obj,
				"", "userCtlr");

		// console.log(obj);
	};

	$scope.updatePassword = function(dialogData) {
		if (!utils.isActiveUser()) {
			return;
		}

		if (dialogData.newpasswd1 != dialogData.newpasswd2) {
			utils.showAlert("New and Confirm passwords do not match");
			return;
		}else if (dialogData.currentpasswd === dialogData.newpasswd1){
			utils.showAlert("New must be different from current password");
			return;
		}

		var user = dialogData.user;
		var currentPasswd = dialogData.currentpasswd;
		var newPasswd = dialogData.newpasswd1;

		if (user.upass != utils.hash(currentPasswd)) {
			utils.showAlert("Invalid current password");
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "changepwd";

		user.upass = utils.hash(newPasswd);
		user.temp_pwd = null;
		user.resetpwd = 0;

		userSvc
				.saveUserDetails(user)
				.then(
						function(message) {
							if (message) {
								utils.showAlert(message);
							}

							userSvc
									.passwordNotification(user)
									.then(
											function(message) {
												$scope.eventinprogress = false;
												if (message) {
													utils.showAlert(message);
												} else {
													$window
															.alert("Password changed successfully");
												}
												$scope.closeThisDialog(dialogData.ngDialogId);
												$route.reload();
												utils.logoff();
											},
											function(err) {
												$window
														.alert("Password changed successfully but could not send email");
												console.log(err);
												$scope.eventinprogress = false;
												$scope.closeThisDialog(dialogData.ngDialogId);
												$route.reload();
												utils.logoff();
											});
						},
						function(err) {
							$window
									.alert("Unable to save the new Password. Please try again later.");
							console.log(err);
							$scope.eventinprogress = false;
						});

	};

	$scope.editEmail = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		var obj = {
			user : angular.copy($scope.org_user),
			org_user : $scope.org_user
		};
		utils.showPopup("./views/profile/popup-email-edit.html", obj, "",
				"userCtlr");
	};

	$scope.updateEmail = function(dialogData) {
		if (!utils.isActiveUser()) {
			return;
		}

		var user = dialogData.user;
		var org_user = dialogData.org_user;

		if (user.email === org_user.email) {
			// NO Action required
			$scope.closeThisDialog(dialogData.ngDialogId);
			return;
		}

		$scope.eventinprogress = true;
		$scope.eventmessage = "updatingemail";

		user.emailverified = 0;
		user.ecode = utils.guid();

		var ecodevalidity = new Date();
		ecodevalidity = ecodevalidity.setDate(ecodevalidity.getDate() + 2);
		user.ecodevalidity = new Date(ecodevalidity);

		// save the new ecode

		userSvc.saveUserDetails(user).then(function(message) {
			// if (message) {
			// utils.showAlert(message);
			// }
			userSvc.emailChangeNotification(user).then(function(message) {
				$scope.eventinprogress = false;
				if (message) {
					utils.showAlert(message);
				}
				$scope.closeThisDialog(dialogData.ngDialogId);
				$route.reload();
			}, function(err) {
				$scope.eventinprogress = false;
				utils.showAlert(err);
				console.log(errResponse);
				$scope.closeThisDialog(dialogData.ngDialogId);
				$route.reload();
			});
		}, function(err) {
			$scope.eventinprogress = false;
			utils.showAlert("Unable to Update Email. Please try again later.");
			console.log(err);
		});
	};

	$scope.resetInfoTab = function() {
		if (!utils.isActiveUser()) {
			return;
		}

		$scope.user = angular.copy($scope.org_user);
	};

	$scope.switchTab = function(tabname) {
		if (!utils.isActiveUser()) {
			return;
		}

		$location.search("tab", tabname);
		$scope.selectedTab = tabname;
	};
}
