var lpApp = angular.module('leadpassers', ['ngRoute', 'ngDialog', 'ngStorage',
    'ngSanitize', 'ngMd5', 'ngIdle', '720kb.datepicker', 'ngFileUpload', '720kb.tooltips', 'mdo-angular-cryptography']);

lpApp.run(['$rootScope', '$templateCache', '$window', 'utils',
    function ($rootScope, $templateCache, $window, utils) {

        var api_url = "./api/web";
        utils.setSession("apiUrl", api_url);
        $rootScope.api_url = api_url;

        $rootScope.$on('$routeChangeStart', function () {
            $templateCache.removeAll();

            utils.setSession("apiUrl", api_url);
            $rootScope.api_url = api_url;
        });

        $rootScope.online = navigator.onLine;

        $window.addEventListener("offline", function () {
            $rootScope.$apply(function () {
                $rootScope.online = false;
            });
        }, false);

        $window.addEventListener("online", function () {
            $rootScope.$apply(function () {
                $rootScope.online = true;
            });
        }, false);

    }]);


lpApp.config(['$cryptoProvider', function ($cryptoProvider) {
        $cryptoProvider.setCryptographyKey("SECRET_SALT_KEY");
    }]);