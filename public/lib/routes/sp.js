lpApp.config(SpRoute);

SpRoute.$inject = ['$routeProvider', '$locationProvider'];

function SpRoute($routeProvider, $locationProvider) {
	$routeProvider

	.when('/sp', {
		redirectTo : '/sp/leads'
	})
	.when('/sp/leads', {
		templateUrl : 'views/leadmgmt/sp.html',
		controller : 'leadmgmtCtlr',
		reloadOnSearch: false
	})
	.when('/sp/leads/details/:uid', {
		templateUrl : 'views/leadmgmt/lead-details-sp.html',
		controller : 'leadmgmtCtlr',
		reloadOnSearch: false
	})

	.when('/sp/profile', {
		templateUrl : 'views/profile/profile.html',
		controller : 'userCtlr',
		reloadOnSearch: false
	});
	
	// $locationProvider.html5Mode(true);
	$locationProvider.hashPrefix('!');
}
