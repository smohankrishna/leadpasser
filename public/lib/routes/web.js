lpApp.config(WebConfigRoute);

WebConfigRoute.$inject = ['$routeProvider', '$locationProvider'];

function WebConfigRoute($routeProvider, $locationProvider) {
	$routeProvider.when('/login', {
		templateUrl : 'views/login/login.html',
		controller : 'loginCtlr',
		reloadOnSearch : false
	}).when('/login/register/verify', {
		templateUrl : 'views/login/verify.html',
		controller : 'loginCtlr',
		reloadOnSearch : false
	}).when('/login/register/:utype', {
		templateUrl : 'views/login/user-registration.html',
		controller : 'loginCtlr',
		reloadOnSearch : false
	}).when('/logoff', {
		templateUrl : 'views/profile/logoff.html',
		controller : 'userCtlr',
		reloadOnSearch : false
	}).otherwise({
		redirectTo : '/login'
	});
	// $locationProvider.html5Mode(true);
	$locationProvider.hashPrefix('!');
}
