lpApp.config(LPpRoute);

LPpRoute.$inject = ['$routeProvider', '$locationProvider'];

function LPpRoute($routeProvider, $locationProvider) {
	$routeProvider

	.when('/de', {
		templateUrl : 'views/de/home.html',
		controller : 'deCtlr',
		reloadOnSearch: false
	})

	.when('/de/users', {
		templateUrl : 'views/de/users/list.html',
		controller : 'deCtlr',
		reloadOnSearch: false
	}).when('/de/users/new', {
		templateUrl : 'views/de/users/new.html',
		controller : 'deCtlr',
		reloadOnSearch: false
	}).when('/de/users/details/:uid', {
		templateUrl : 'views/de/users/details.html',
		controller : 'deCtlr',
		reloadOnSearch: false
	})

	.when('/de/config/masterlist', {
		templateUrl : 'views/masterlist/home.html',
		controller : 'masterlistCtlr',
		reloadOnSearch: false
	})

	.when('/de/config/admin', {
		templateUrl : 'views/de/home.html',
		controller : 'lpCtlr',
		reloadOnSearch: false
	})

	.when('/de/profile', {
		templateUrl : 'views/user/profile.html',
		controller : 'userCtlr',
		reloadOnSearch: false
	});
	
	// $locationProvider.html5Mode(true);
	$locationProvider.hashPrefix('!');
}
