lpApp.config(LgRoute);

LgRoute.$inject = ['$routeProvider', '$locationProvider'];

function LgRoute($routeProvider, $locationProvider) {
	$routeProvider

	.when('/lg', {
		redirectTo : '/lg/leads'
	})
	.when('/lg/leads', {
		templateUrl : 'views/leadmgmt/lg.html',
		controller : 'leadmgmtCtlr',
		reloadOnSearch: false
	})

	.when('/lg/profile', {
		templateUrl : 'views/profile/profile.html',
		controller : 'userCtlr',
		reloadOnSearch: false
	});
	
	// $locationProvider.html5Mode(true);
	$locationProvider.hashPrefix('!');
}
