lpApp.config(SuRoute);

SuRoute.$inject = ['$routeProvider', '$locationProvider'];

function SuRoute($routeProvider, $locationProvider) {
	$routeProvider

	.when('/su', {
		redirectTo : '/su/leadmgmt'
	})

	.when('/su/leadmgmt', {
		templateUrl : 'views/leadmgmt/su.html',
		controller : 'leadmgmtCtlr',
		reloadOnSearch : false
	}).when('/su/leadmgmt/details/:uid', {
		templateUrl : 'views/leadmgmt/lead-details.html',
		controller : 'leadmgmtCtlr',
		reloadOnSearch : false
	})

	.when('/su/accounts/:utype', {
		templateUrl : 'views/su/users/list.html',
		controller : 'suCtlr',
		reloadOnSearch : false
	}).when('/su/accounts/:utype/new', {
		templateUrl : 'views/su/users/new.html',
		controller : 'suCtlr',
		reloadOnSearch : false
	}).when('/su/accounts/:utype/details/:uid', {
		templateUrl : 'views/su/users/details.html',
		controller : 'suCtlr',
		reloadOnSearch : false
	})

	.when('/su/config/catalog', {
		templateUrl : 'views/catalog/home.html',
		controller : 'catalogCtlr',
		reloadOnSearch : false
	})
	.when('/su/config/locations', {
		templateUrl : 'views/locations/home.html',
		controller : 'locationsCtlr',
		reloadOnSearch : false
	})
	.when('/su/config/list', {
		templateUrl : 'views/masterlist/single-list.html',
		controller : 'masterlistCtlr',
		reloadOnSearch: true
	})
	.when('/su/profile', {
		templateUrl : 'views/profile/profile.html',
		controller : 'userCtlr',
		reloadOnSearch : false
	});

	// $locationProvider.html5Mode(true);
	$locationProvider.hashPrefix('!');
}
