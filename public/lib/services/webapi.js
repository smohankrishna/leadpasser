lpApp.factory('apiService', ApiService);

ApiService.$inject = ['$http', '$q', '$rootScope', 'utils'];

function ApiService($http, $q, $rootScope, utils) {

    var getCall = function (aUrl, aParams) {

        var defer = $q.defer();

        var req = {
            method: 'GET',
            url: aUrl,
            params: aParams,
            headers: {}
        };

        var token = utils.getSession("authtoken");
        if (token) {
            req.headers["app-auth-token"] = token;
        }

        if ($rootScope.online === false) {
            throw "USER_OFFLINE";
        } else {
            $http(req).then(function (response) {
                defer.resolve(response);
            }, function (errResponse) {
                if (errResponse.status === 403) {
                    utils.logoff();
                } else {
                    defer.reject(errResponse);
                }
            });
        }
        return defer.promise;
    };

    var postCall = function (aUrl, aData) {

        var defer = $q.defer();

        var req = {
            method: 'POST',
            url: aUrl,
            data: aData,
            headers: {}
        };

        var token = utils.getSession("authtoken");

        if (token) {
            req.headers["app-auth-token"] = token;
        }

        if ($rootScope.online === false) {
            throw "USER_OFFLINE";
        } else {
            $http(req).then(function (response) {
                defer.resolve(response);
            }, function (errResponse) {
                if (errResponse.status === 403) {
                    utils.logoff();
                } else {
                    defer.reject(errResponse);
                }
            });
        }
        return defer.promise;
    };

    return {
        getCall: getCall,
        postCall: postCall
    };
}
