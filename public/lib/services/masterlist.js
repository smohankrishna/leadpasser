lpApp.factory('masterListService', MasterListService);

MasterListService.$inject = ['$q', '$location', 'utils', 'apiService'];

function MasterListService($q, $location, utils, apiService) {

	var API_URL = utils.getSession("apiUrl");

	var getAllMasterLists = function(params) {
		var defer = $q.defer();

		apiService.getCall(API_URL + "/masterlist/list", params).then(
				function(response) {
					var data = response.data;

					if (!data) {
						defer.reject({
							error : "Invalid Response"
						});
					} else if (response.data.error) {
						defer.reject(response.data.error);
					} else {
						defer.resolve(response.data);
					}
				}, function(errResponse) {
					defer.reject(errResponse);
				});
		return defer.promise;
	}

	var getListItems = function(params) {
		var defer = $q.defer();

		apiService.getCall(API_URL + "/masterlist/list", params).then(
				function(response) {
					var data = response.data;
					if (!data) {
						defer.reject({
							error : "Invalid Response"
						});
					} else if (data.error) {
						defer.reject(data.error);
					} else {
						defer.resolve(data.list, data.rowCount);
					}
				}, function(errResponse) {
					defer.reject(errResponse);
				});
		return defer.promise;
	};
	
	var getListItemsByCode = function(code) {
		var defer = $q.defer();

		apiService.getCall(API_URL + "/masterlist/list/"+code).then(
				function(response) {
					var data = response.data;
					if (!data) {
						defer.reject({
							error : "Invalid Response"
						});
					} else if (data.error) {
						defer.reject(data.error);
					} else {
						defer.resolve(data.list, data.rowCount);
					}
				}, function(errResponse) {
					defer.reject(errResponse);
				});
		return defer.promise;
	};
	
	var getItemDetailsByCode = function(code, parent_uid) {
		var defer = $q.defer();
		
		var params = {
				filter : {
						code : code,
						parent_uid : parent_uid
				}
		}

		apiService.getCall(API_URL + "/masterlist/detailsbycode", params).then(
				function(response) {
					var data = response.data;
					if (!data) {
						defer.reject({
							error : "Invalid Response"
						});
					} else if (data.error) {
						defer.reject(data.error);
					} else {
						defer.resolve(data.details);
					}
				}, function(errResponse) {
					defer.reject(errResponse);
				});
		return defer.promise;
	};
	

	var saveList = function(list) {
		var defer = $q.defer();
		
		var postData = {
			mlinfo : list
		};

		var apiUrl = API_URL + "/masterlist/details/";

		apiUrl += (list.uid) ? "update" : "insert";

		apiService.postCall(apiUrl, postData).then(function(response) {
			if (!response.data) {
				defer.reject({error:"Invalid Response"});
			} else if (response.data.error) {
				defer.reject(response.data.error);
			} else {
				defer.resolve();
			}
		}, function(errResponse) {
			defer.reject(errResponse);
		});
		return defer.promise;
	};

	return {
		getAllMasterLists : getAllMasterLists,
		getListItems : getListItems,
		getListItemsByCode : getListItemsByCode,
		getItemDetailsByCode : getItemDetailsByCode,
		saveList : saveList
	}

}