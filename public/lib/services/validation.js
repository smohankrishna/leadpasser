lpApp.factory('validationService', ValidationService);

ValidationService.$inject = ['$q', '$location', 'utils', 'apiService'];

function ValidationService($q, $location, utils, apiService) {
	var API_URL = utils.getSession("apiUrl");
	
	var checkForUsername = function(uname) {

		var defer = $q.defer();

		var params = {
			uname : uname
		};
		apiService.getCall('./api/web/user/usernamecheck/', params).then(
				function(response) {
					defer.resolve(response);
				}, function(errResponse) {
					defer.reject(errResponse);
				});
		return defer.promise;
	};

	var validateEmail = function(uname, ecode) {
		var defer = $q.defer();
		var now = new Date();

		apiService.getCall(API_URL+"/user/detailsbyuname/" + uname).then(
				function(response) {
					var data = response.data;
					
					if (data.error) {
						defer.reject(data.error);
					} else {
						var user = data.details;
						
						var ecodevalidity = new Date(user.ecodevalidity);
						
						if (user.ecode == ecode) {
							if(now <= ecodevalidity){
								defer.resolve({valid: true, user: user});
							}else{
								defer.resolve({valid: false, user: user});
							}
						} else {
							defer.resolve({valid: false, user: user});
						}
					}
				}, function(errResponse) {
					console.log(errResponse);
					defer.reject(errResponse);
				});

		return defer.promise;
	};
	
	var checkForListCode = function(code, parent_uid) {

		var defer = $q.defer();

		var params = {
			code : code,
			parent_uid : parent_uid
		};
		apiService.getCall('./api/web/masterlist/checklistcode/', params).then(
				function(response) {
					defer.resolve(response);
				}, function(errResponse) {
					defer.reject(errResponse);
				});
		return defer.promise;
	};


	return {
		checkForUsername : checkForUsername,
		validateEmail : validateEmail,
		checkForListCode : checkForListCode
	};
}
