lpApp.factory('commonDataService', CommonDataService);

CommonDataService.$inject = ['$q', 'utils', 'apiService'];

function CommonDataService($q, utils, apiService) {
	
	var API_URL = utils.getSession("apiUrl");
	var getMasterCatalog = function() {
		var defer = $q.defer();

		apiService.getCall(API_URL+"/catalog/fullcatalog").then(
				function(response) {
					var result = {
						categories : response.data.details[0],
						subcategories : response.data.details[1]
					}
					defer.resolve(result)
				}, function(err) {
					defer.reject(err);
				});
		return defer.promise;
	};

	var getMasterLocations = function() {
		var defer = $q.defer();
		apiService.getCall(API_URL+"/locations/alllocations").then(
				function(response) {
					var result = {
						countries : response.data.details[0],
						states : response.data.details[1],
						districts : response.data.details[2],
						cities : response.data.details[3]
					}
					defer.resolve(result);
				}, function(err) {
					defer.reject(err);
				});
		return defer.promise;
	};
	
	var getMasterListByCode = function(parent_code) {
		var defer = $q.defer();
		apiService.getCall(API_URL+"/masterlist/list/"+parent_code).then(
				function(response) {
					defer.resolve(response);
				}, function(err) {
					defer.reject(err);
				});
		return defer.promise;
	};

	return {
		getMasterCatalog : getMasterCatalog,
		getMasterLocations : getMasterLocations,
		getMasterListByCode : getMasterListByCode
	};
}