lpApp.factory('leadmgmtService', LeadMgmtService);

LeadMgmtService.$inject = ['$q', '$location', 'utils', 'apiService'];

function LeadMgmtService($q, $location, utils, apiService) {

    var API_URL = utils.getSession("apiUrl");

    var getLeads = function (utype, leadtype, filter, searchFilter, options) {
        var defer = $q.defer();
        var params = {
            filter: filter,
            options: options,
            search: searchFilter
        };

        var url = API_URL + "/leadmgmt/list/" + utype;

        url += (leadtype === 'open') ? "/open" : "/past";

        apiService.getCall(url, params).then(function (response) {
            var data = response.data;
            if (data.error) {
                defer.reject(data.error);
            } else {
                defer.resolve(data);
            }
        }, function (err) {
            defer.reject(err);
        });

        return defer.promise;
    }

    var getLeadDetails = function (uid) {
        var defer = $q.defer();

        var url = API_URL + "/leadmgmt/details/" + uid;

        var param = {};

        apiService.getCall(url, param).then(function (response) {
            defer.resolve(response);
        }, function (err) {
            defer.reject(err);
        });

        return defer.promise;
    };

    var getLeadServiceProviders = function (leaduid, spleaduid) {
        var defer = $q.defer();

        var url = API_URL + "/leadmgmt/splist/" + leaduid;
        if (spleaduid) {
            url += "/" + spleaduid;
        }
        var param = {};

        apiService.getCall(url, param).then(function (response) {
            defer.resolve(response);
        }, function (err) {
            defer.reject(err);
        });

        return defer.promise;
    };

    var getSPLeadDetails = function (uid) {
        var defer = $q.defer();

        var url = API_URL + "/leadmgmt/spleaddetails/" + uid;

        var param = {};

        apiService.getCall(url, param).then(function (response) {
            defer.resolve(response);
        }, function (err) {
            defer.reject(err);
        });

        return defer.promise;
    };

    var saveLead = function (lead) {
        var defer = $q.defer();

        var url = API_URL + "/leadmgmt/details/";

        if (lead.uid) {
            url += "update";
        } else {
            url += "insert";
        }
        var param = {
            data: lead
        };

        apiService.postCall(url, param).then(function (response) {
            defer.resolve(response);
        }, function (err) {
            defer.reject(err);
        });

        return defer.promise;
    };

    var saveLeadServiceProvider = function (leadServiceProvider) {
        var defer = $q.defer();

        var url = API_URL + "/leadmgmt/splist/";

        if (leadServiceProvider.uid) {
            url += "update";
        } else {
            url += "insert";
        }
        var param = {
            data: leadServiceProvider
        };

        apiService.postCall(url, param).then(function (response) {
            defer.resolve(response);
        }, function (err) {
            defer.reject(err);
        });

        return defer.promise;
    };



    return {
        getLeads: getLeads,
        getLeadDetails: getLeadDetails,
        getLeadServiceProviders: getLeadServiceProviders,
        getSPLeadDetails: getSPLeadDetails,
        saveLead: saveLead,
        saveLeadServiceProvider: saveLeadServiceProvider
    }

}