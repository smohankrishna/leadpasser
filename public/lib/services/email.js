lpApp.factory('emailService', EmailService);

EmailService.$inject = ['$q', '$location', 'utils', 'apiService'];

function EmailService($q, $location, utils, apiService) {

	var templates = {
		registration : "./templates/registration-email.txt",
		emailValidation : "./templates/verification-email.txt",
		passwordReset : {
			template : "./templates/passwordreset-email.txt",
			subject : "Reset Password Successful",
			temp_pwd : "(as provided by user)"
		}
	}

	/***************************************************************************
	 * Registration Successful email -- auto send
	 */
	var registrationEmail = function(user) {

		var defer = $q.defer();

		apiService
				.getCall(templates.registration)
				.then(
						function(response) {

							var tmpl = response.data;

							tmpl = tmpl
									.replace("${name}", user.name)
									.replace("${uname}", user.uname)
									.replace(
											"${upass}",
											user.temp_pwd
													|| "(as entered during registration)");

							var newUrl = $location.absUrl();

							newUrl = newUrl.substring(0, newUrl.indexOf("#!"));

							newUrl += "#!/login/register/verify?uname="
									+ user.uname + "&ecode=" + user.ecode;

							tmpl = tmpl.replace("${link}", newUrl).replace(
									"${ecodevalidity}", user.ecodevalidity);

							var data = {
								to : user.email,
								subject : "Registration Successful",
								html : tmpl
							};

							apiService.postCall('/api/web/sendmail', data)
									.then(function(response) {
										defer.resolve(response);
									}, function(errResponse) {
										defer.reject(errResponse);
									});

						}, function(errResponse) {
							console.log(errResponse);
							defer.reject(errResponse);
						});

		return defer.promise;
	};

	/***************************************************************************
	 * Send Email Validation email to user -- on request
	 */
	var validateEmailAddress = function(user) {

		var defer = $q.defer();

		apiService.getCall(templates.emailValidation).then(
				function(response) {

					var tmpl = response.data;

					tmpl = tmpl.replace("${name}", user.name);

					var newUrl = $location.absUrl();

					newUrl = newUrl.substring(0, newUrl.indexOf("#!"));

					newUrl += "#!/login/register/verify?uname=" + user.uname
							+ "&ecode=" + user.ecode;

					tmpl = tmpl.replace("${link}", newUrl).replace(
							"${ecodevalidity}", user.ecodevalidity);

					var data = {
						to : user.email,
						subject : "Email Verification Required",
						html : tmpl
					};

					apiService.postCall('/api/web/sendmail', data).then(
							function(response) {
								defer.resolve(response);
							}, function(errResponse) {
								defer.reject(errResponse);
							});

				}, function(errResponse) {
					console.log(errResponse);
					defer.reject(errResponse);
				});

		return defer.promise;
	};

	/***************************************************************************
	 * Password Reset Email
	 **************************************************************************/
	var passwordResetEmail = function(user) {

		var defer = $q.defer();

		apiService.getCall(templates.passwordReset.template).then(
				function(response) {

					var tmpl = response.data;

					tmpl = tmpl.replace("${name}", user.name).replace(
							"${uname}", user.uname).replace("${upass}",
							user.temp_pwd || templates.passwordReset.temp_pwd);

					var data = {
						to : user.email,
						subject : templates.passwordReset.subject,
						html : tmpl
					};

					apiService.postCall('/api/web/sendmail', data).then(
							function(response) {
								defer.resolve(response);
							}, function(errResponse) {
								defer.reject(errResponse);
							});

				}, function(errResponse) {
					console.log(errResponse);
					defer.reject(errResponse);
				});

		return defer.promise;
	};

	return {
		registrationEmail : registrationEmail,
		validateEmailAddress : validateEmailAddress,
		passwordResetEmail : passwordResetEmail
	};
}
