lpApp.factory('utils', UtilsService);

UtilsService.$inject = ['$rootScope', '$sessionStorage',
    '$location', 'md5', 'ngDialog', '$crypto'];

function UtilsService($rootScope, $sessionStorage, $location, md5,
        ngDialog, $crypto) {

    var getSession = function (key) {
        try {
            var encrypted = $sessionStorage[key];
            if (encrypted) {
                var value = $crypto.decrypt(encrypted);

                try {
                    value = angular.copy(JSON.parse(value));
                } catch (err) {

                }
                return angular.copy(value);
//            return encrypted;
            } else {
                return null;
            }
        } catch (err) {
            logoff();
        }
    };

    var setSession = function (key, value) {
        if (value === null || angular.isUndefined(value)) {
            delete $sessionStorage[key];
        } else {
            var v = angular.copy(value);
            if (typeof value === 'object') {
                v = JSON.stringify(value);
            }
            $sessionStorage[key] = $crypto.encrypt(v);
        }
        return {
            key: key,
            value: value
        };
    };

    var clearSession = function () {
        $sessionStorage.$reset();
    };

    var logoff = function () {
        clearSession();
        $location.url("/login");
    };

    var isActiveUser = function () {
        var uinfo = getSession("uinfo");

        if (uinfo && uinfo.uname) {

            var url = $location.url().toLowerCase();

            switch (uinfo.utype.toLowerCase()) {
                case "su" :
                    if (!url.startsWith("/su")) {
                        logoff();
                    }
                    break;
                case "de" :
                    if (!url.startsWith("/de")) {
                        logoff();
                    }
                    break;
                case "lg" :
                    if (!url.startsWith("/lg")) {
                        logoff();
                    }
                    break;
                case "sp" :
                    if (!url.startsWith("/sp")) {
                        logoff();
                    }
                    break;
                case "cl" :
                    if (!url.startsWith("/cl")) {
                        logoff();
                    }
                    break;
                default :
                    logoff();
                    break;
            }

            return true;
        }

        logoff();
        return false;
    };

    var guid = function () {
        var d = new Date().getTime();
        var format = "xxxxxxxxxxxx-4xxx-yxxx-xxxxxxxxxxxx";

        var uuid = format.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    };

    // templateUrl, dataObj, confirmStr, controllerName
    var showPopup = function () {

        if (arguments.length < 2) {
            throw "Invalid parameters \n"
                    + " { templateUrl, dataObj [, confirmStr [, controllerName]]} ";
        }

        var templateUrl = "";
        var dataObj = {};
        var confirmStr = "";
        var controllerName = "";

        if (arguments.length == 2) {
            templateUrl = arguments[0];
            dataObj = arguments[1];
        } else if (arguments.length == 3) {
            templateUrl = arguments[0];
            dataObj = arguments[1];
            confirmStr = arguments[2];
        } else if (arguments.length == 4) {
            templateUrl = arguments[0];
            dataObj = arguments[1];
            confirmStr = arguments[2];
            controllerName = arguments[3];
        }

        ngDialog.open({
            template: templateUrl,
            className: 'ngdialog-theme-plain custom-width',
            showClose: false,
            closeByEscape: false,
            closeByDocument: false,
            cache: false,
            controller: controllerName,
            data: dataObj,
            preCloseCallback: function (value) {

                if (value && confirmStr) {
                    if (confirm(confirmStr)) {
                        return true;
                    }
                    return false;
                }
            }
        });
    };

    var hash = function (str) {
        return md5.createHash(str);
    };

    var randomPassword = function (plength) {
        var keylist = "abcdefghijklnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var temp = '';

        if (plength == null || plength == undefined) {
            plength = 8;
        }

        for (i = 0; i < plength; i++) {
            temp += keylist.charAt(Math.floor(Math.random() * keylist.length));
        }
        return temp;
    };

    var randomOTP = function (plength) {
        var keylist = "0123456789";
        var temp = '';

        if (plength == null || plength == undefined) {
            plength = 6;
        }

        for (i = 0; i < plength; i++) {
            temp += keylist.charAt(Math.floor(Math.random() * keylist.length));
        }
        return temp;
    };

    var confirm = function (str) {
        return $window.confirm(str);
    };

    var showAlert = function (message, title) {
        var obj = {
            title: title,
            message: message
        }
        showPopup("./views/common/alert.html", obj, "", "menuCtlr");
    }


    return {
        getSession: getSession,
        setSession: setSession,
        clearSession: clearSession,
        logoff: logoff,
        guid: guid,
        isActiveUser: isActiveUser,
        showPopup: showPopup,
        hash: hash,
        randomPassword: randomPassword,
        randomOTP: randomOTP,
        showAlert: showAlert
    };
}
