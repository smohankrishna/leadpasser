lpApp.factory('smsService', WebSmsService);

WebSmsService.$inject = ['$q', '$location', 'utils', 'apiService'];

function WebSmsService($q, $location, utils, apiService) {

	var API_URL = utils.getSession("apiUrl");
	var api = API_URL+"/sendtext";
	

	var templates = {
		registration : "Welcome to LeadPassers.com. Your UserID: ${uname}, Password: ${upass}",
		activation : "Your LeadPassers a/c activation OTP for UserID ${uname} is ${otp}. This is valid for 15 min.",
		forgot_password : "Your new password for UserID ${uname} is ${upass}",
		change_password : "You have successfully changed the password for UserID ${uname}",
		mobile_change : "Kindly enter ${otp} as OTP for UserID ${uname}. This is valid for 15 min."
	};

	var sendText = function(to, text) {

		var defer = $q.defer();

		var data = {
			to : to,
			text : text
		};

		apiService.postCall(api, data).then(function(response) {
			defer.resolve(response);
		}, function(errResponse) {
			console.log(errResponse);
			defer.reject(errResponse);
		});
		return defer.promise;
	};

	var registration = function(user) {
		var defer = $q.defer();

		if (!user.mobilenum || !user.uname || !user.temp_pwd) {
			defer.reject("Invalid Username/MobileNum/Password");
		} else {
			var text = templates.registration;

			text = text.replace("${uname}", user.uname).replace("${upass}",
					user.temp_pwd);

			sendText(user.mobilenum, text).then(function(response) {
				defer.resolve(response);
			}, function(err) {
				console.log(err);
				defer.reject(err);
			});
		}

		return defer.promise;
	};

	var activation = function(user) {
		var defer = $q.defer();

		if (!user.mobilenum || !user.uname || !user.mcode) {
			defer.reject("Invalid Username/MobileNum/OTP");
		} else {
			var text = templates.activation;

			text = text.replace("${uname}", user.uname).replace("${otp}",
					user.mcode);

			sendText(user.mobilenum, text).then(function(response) {
				defer.resolve(response);
			}, function(err) {
				console.log(err);
				defer.reject(err);
			});
		}

		return defer.promise;
	};

	var forgotPassword = function(user) {
		var defer = $q.defer();
		if (!user.mobilenum || !user.uname || !user.temp_pwd) {
			defer.reject("Invalid Username/MobileNum/Password");
		} else {

			var text = templates.forgot_password;

			text = text.replace("${uname}", user.uname).replace("${upass}",
					user.temp_pwd);

			sendText(user.mobilenum, text).then(function(response) {
				defer.resolve(response);
			}, function(err) {
				console.log(err);
				defer.reject(err);
			});
		}
		return defer.promise;
	};
	
	var changePassword = function(user) {
		var defer = $q.defer();
		if (!user.mobilenum || !user.uname) {
			defer.reject("Invalid Username/MobileNum");
		} else {

			var text = templates.change_password;

			text = text.replace("${uname}", user.uname);

			sendText(user.mobilenum, text).then(function(response) {
				defer.resolve(response);
			}, function(err) {
				console.log(err);
				defer.reject(err);
			});
		}
		return defer.promise;
	};
	
	var mobileChange = function(user) {

		var defer = $q.defer();

		if (!user.mobilenum || !user.uname || !user.mcode) {
			defer.reject("Invalid Username/MobileNum/OTP");
		} else {
			var text = templates.mobile_change;

			text = text.replace("${uname}", user.uname).replace("${otp}",
					user.mcode);

			sendText(user.mobilenum, text).then(function(response) {
				defer.resolve(response);
			}, function(err) {
				console.log(err);
				defer.reject(err);
			});
		}

		return defer.promise;
	};

	return {
		registration : registration,
		activation : activation,
		forgotPassword : forgotPassword,
		changePassword : changePassword,
		mobileChange : mobileChange
	};
}
