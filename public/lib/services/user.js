lpApp.factory('userSvc', UserService);

UserService.$inject = ['$q', '$rootScope', '$sessionStorage', '$location',
    'md5', 'ngDialog', 'utils', 'validationService', 'apiService',
    'emailService', 'smsService'];

function UserService($q, $rootScope, $sessionStorage, $location, md5, ngDialog,
        utils, validationService, apiService, emailService, smsService) {

    var API_URL = utils.getSession("apiUrl");

    var getDetailsByUsername = function (uname) {
        var defer = $q.defer();

        apiService.getCall(API_URL + "/user/detailsbyuname/" + uname).then(
                function (response) {

                    var data = response.data;

                    if (data.error) {
                        defer.reject(data.error);
                    } else {
                        defer.resolve(data.details);
                    }
                }, function (err) {
            defer.reject(err);
        });

        return defer.promise;
    };

    var saveUserDetails = function (user) {
        var defer = $q.defer();

        validationService
                .checkForUsername(user.uname)
                .then(
                        function (response) {

                            if (!user.uid && response.data.error) {
                                defer
                                        .reject("Error validating username. Try again after sometime");
                            } else if (!user.uid
                                    && response.data.result === false) {
                                defer
                                        .reject("Username already taken. Try some other name");
                            } else {

                                // DO WEB API CALL TO SAVE RECORD
                                var currentUser = utils.getSession("uinfo");
                                try {
                                    user.newcreatedby = currentUser
                                            ? currentUser.uname
                                            : "self";
                                    user.newmodifiedby = currentUser
                                            ? currentUser.uname
                                            : "self";

                                    var postData = {
                                        userinfo: user
                                    };

                                    var apiUrl = API_URL + "/user/details/";

                                    apiUrl += (user.uid) ? "update" : "insert";

                                    apiService
                                            .postCall(apiUrl, postData)
                                            .then(
                                                    function (response) {
                                                        if (!response.data) {
                                                            throw "UNKNOWN ERROR";
                                                        } else if (response.data.error) {
                                                            throw response.data.error;
                                                        } else {

                                                            if (user.uid) {
                                                                defer.resolve();
                                                            } else {
                                                                registrationNotification(
                                                                        user)
                                                                        .then(
                                                                                function (
                                                                                        response) {
                                                                                    defer
                                                                                            .resolve();
                                                                                },
                                                                                function (
                                                                                        err) {
                                                                                    defer
                                                                                            .resolve(err);
                                                                                });
                                                            }

                                                        }
                                                    }, function (errResponse) {
                                                throw errResponse;
                                            });
                                } catch (err) {
                                    defer.reject(err);
                                }
                            }
                        }, function (errResponse) {
                    defer.reject(errResponse);
                });

        return defer.promise;
    };

    var registrationNotification = function (user) {

        var promises = [];

        var defer = $q.defer();

        console.log(user);

        if (user.email) {
            promises.push(registrationEmail(user));
        }

        if (user.mobilenum) {
            if (user.utype.toUpperCase() == 'SU'
                    || user.utype.toUpperCase() === 'SP') {
                promises.push(registrationText(user));
            } else {
                promises.push(activationText(user));
            }

        }

        $q.all(promises).then(function (response) {
            console.log(response);
            defer.resolve();
        }, function (err) {
            console.log(err);
            defer.reject(err);
        });

        return defer.promise;

    };

    var passwordNotification = function (user) {

        var promises = [];

        var defer = $q.defer();

        if (user.email) {
            promises.push(forgotPasswordEmail(user));
        }

        if (user.mobilenum) {
            if (user.temp_pwd) {
                promises.push(forgotPasswordText(user));
            } else {
                promises.push(changePasswordText(user));
            }
        }

        $q.all(promises).then(function (response) {
            console.log(response);
            defer.resolve();
        }, function (err) {
            console.log(err);
            defer.reject(err);
        });

        return defer.promise;

    };

    var forgotPassword = function (uname) {
        var defer = $q.defer();

        getDetailsByUsername(uname)
                .then(
                        function (uinfo) {
                            if (uinfo && uinfo.uname) {
                                // GENERATE NEW PWD
                                uinfo.temp_pwd = utils.randomPassword(10);
                                uinfo.upass = utils.hash(uinfo.temp_pwd);
                                uinfo.resetpwd = 1;

                                // SAVE USER OBJECT
                                console.log(uinfo);
                                saveUserDetails(uinfo)
                                        .then(
                                                function (response) {
                                                    // NOTIFY FOR PWD CHANGE
                                                    passwordNotification(uinfo)
                                                            .then(
                                                                    function (
                                                                            resp) {
                                                                        defer
                                                                                .resolve(resp);
                                                                    },
                                                                    function (
                                                                            err) {
                                                                        defer
                                                                                .resolve(
                                                                                        resp,
                                                                                        {
                                                                                            error: "Password Reset Successful but error sending notification",
                                                                                            error_code: "ERR_NOTIFY"
                                                                                        });
                                                                    });
                                                },
                                                function (err) {
                                                    defer
                                                            .reject({
                                                                error: "Error Saving Temp Password",
                                                                error_code: "ERR_RESET_PASSWORD"
                                                            });
                                                });
                            } else {
                                defer.reject({
                                    error: "Invalid Username",
                                    error_code: "INVALID_UNAME"
                                });
                            }
                        }, function (err) {
                    console.log(err);
                    defer.reject(err);
                });

        return defer.promise;
    };

    /***************************************************************************
     * Email Notifications
     **************************************************************************/

    var registrationEmail = function (user) {
        var defer = $q.defer();
        try {
            emailService.registrationEmail(user).then(function (response) {
                defer.resolve();
            }, function (errResponse) {
                defer.resolve(errResponse);
            });
        } catch (err) {
            defer.reject(err);
            console.log(err);
        }

        return defer.promise;
    };
    var emailChangeNotification = function (user) {
        var defer = $q.defer();
        try {
            emailService
                    .validateEmailAddress(user)
                    .then(
                            function (response) {
                                defer.resolve("Email Sent");
                            },
                            function (errResponse) {
                                defer
                                        .resolve("New Verification Code saved but could not send email");
                            });
        } catch (err) {
            defer.reject(err);
            console.log(err);
        }

        return defer.promise;
    };

    var forgotPasswordEmail = function (user) {
        var defer = $q.defer();
        try {
            emailService
                    .passwordResetEmail(user)
                    .then(
                            function (response) {
                                defer.resolve("Email Sent");
                            },
                            function (errResponse) {
                                defer
                                        .resolve("Password Reset successful but could not send email");
                            });
        } catch (err) {
            defer.reject(err);
            console.log(err);
        }

        return defer.promise;
    }

    /***************************************************************************
     * Mobile Notifications
     **************************************************************************/

    // ONLY FOR SYSTEM USER AND SERVICE PROVIDER
    var registrationText = function (user) {
        var defer = $q.defer();
        try {
            smsService.registration(user).then(function (response) {
                defer.resolve(response);
            }, function (errResponse) {
                defer.reject(errResponse);
            });
        } catch (err) {
            defer.reject(err);
            console.log(err);
        }

        return defer.promise;
    }

    // ONLY FOR LEAD PASSER -- OTP IS SENT TO USER
    var activationText = function (user) {
        var defer = $q.defer();
        try {
            smsService.activation(user).then(function (response) {
                defer.resolve(response);
            }, function (errResponse) {
                defer.reject(errResponse);
            });
        } catch (err) {
            defer.reject(err);
            console.log(err);
        }

        return defer.promise;
    }

    var mobileChangeText = function (user) {
        var defer = $q.defer();
        try {
            smsService.mobileChange(user).then(function (response) {
                defer.resolve(response);
            }, function (errResponse) {
                defer.reject(errResponse);
            });
        } catch (err) {
            console.log(err);
            defer.reject(err);
        }

        return defer.promise;
    }

    var changePasswordText = function (user) {
        var defer = $q.defer();
        try {
            smsService.changePassword(user).then(function (response) {
                defer.resolve(response);
            }, function (errResponse) {
                defer.reject(errResponse);
            });
        } catch (err) {
            defer.reject(err);
            console.log(err);
        }

        return defer.promise;
    }

    var forgotPasswordText = function (user) {
        var defer = $q.defer();
        try {
            smsService.forgotPassword(user).then(function (response) {
                defer.resolve(response);
            }, function (errResponse) {
                defer.reject(errResponse);
            });
        } catch (err) {
            defer.reject(err);
            console.log(err);
        }

        return defer.promise;
    }

    var getBankDetails = function (uid) {
        var defer = $q.defer();
        apiService.getCall(API_URL + "/user/bankdetails/" + uid).then(
                function (response) {
                    defer.resolve(response);
                }, function (err) {
            defer.reject(err)
        });
        return defer.promise;
    };

    var saveBankDetail = function (bankinfo) {
        var defer = $q.defer();

        var currentUser = utils.getSession("uinfo");
        bankinfo.newcreatedby = currentUser ? currentUser.uname : "self";
        bankinfo.newmodifiedby = currentUser ? currentUser.uname : "self";

        var postData = {
            bankinfo: bankinfo
        };

        var apiUrl = API_URL + "/user/bankdetails/";

        apiUrl += (bankinfo.uid) ? "update" : "insert";

        apiService.postCall(apiUrl, postData).then(function (response) {
            defer.resolve(response);
        }, function (err) {
            defer.reject(err);
        });

        return defer.promise;

    };

    return {
        getDetailsByUsername: getDetailsByUsername,
        saveUserDetails: saveUserDetails,
        registrationNotification: registrationNotification,
        passwordNotification: passwordNotification,
        emailChangeNotification: emailChangeNotification,
        mobileChangeText: mobileChangeText,
        forgotPassword: forgotPassword,
        getBankDetails: getBankDetails,
        saveBankDetail: saveBankDetail
    };
}
