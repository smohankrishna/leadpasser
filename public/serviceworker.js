var CACHE_NAME = 'myfincare-cache-v1';
var urlsToCache = [
    '/',
    '/#!/',
    '/css/bootstrap.css',
    '/css/bootstrap-theme.css',
    '/css/bootstrap-personalize.css',
    '/css/font-awesome.min.css',
    '/css/style.css',
    '/css/ngDialog.min.css',
    '/css/ngDialog-theme-default.min.css',
    '/css/ngDialog-theme-plain.min.css',
    '/css/ngDialog-custom-width.css',
    '/css/angular-datepicker.min.css',
    '/css/angular-tooltips.min.css',
    '/css/app.css',
    '/css/navbar.css',
    
    '/fonts/FontAwesome.otf',
    '/fonts/fontawesome-webfont.eot',
    '/fonts/fontawesome-webfont.svg',
    '/fonts/fontawesome-webfont.ttf',
    '/fonts/fontawesome-webfont.woff',
    '/fonts/fontawesome-webfont.woff2',
    '/fonts/glyphicons-halflings-regular.eot',
    '/fonts/glyphicons-halflings-regular.svg',
    '/fonts/glyphicons-halflings-regular.ttf',
    '/fonts/glyphicons-halflings-regular.woff',
    '/fonts/glyphicons-halflings-regular.woff2',

    '/js/jquery.min.js',
    '/js/angular.min.js',
    '/js/bootstrap.js',
    '/js/angular-locale_en-in.js',
    '/js/ngDialog.min.js',
    '/js/angular-route.min.js',
    '/js/ngStorage.min.js',
    '/js/angular-sanitize.min.js',
    '/js/angular-md5.min.js',
    '/js/angular-idle.min.js',
    '/js/aes.js',
    '/js/mdo-angular-cryptography.js',
    '/js/angular-datepicker.min.js',
    '/js/ng-file-upload.min.js',
    '/js/ng-file-upload-shim.min.js',
    '/js/angular-tooltips.min.js'
];

self.addEventListener('install', function (event) {
    // Perform install steps

    event.waitUntil(
            caches.open(CACHE_NAME)
            .then(function (cache) {
//                console.log('Opened cache');
                self.skipWaiting();
                return cache.addAll(urlsToCache);
            })
            );
});

self.addEventListener('fetch', function (event) {
    event.respondWith(
            caches.match(event.request)
            .then(function (response) {
                // Cache hit - return response
                if (response) {
                    return response;
                }
                return fetch(event.request);
            }
            )
            );
});
