lpApp.directive('breadcrumb', Breadcrumb);
Breadcrumb.$inject = ['$location'];

function Breadcrumb($location) {
	return {
		restrict : 'E',
		require : '?ngModel',
		scope : {
			ngModel : "="
		},
		templateUrl : 'directives/breadcrumb/directive.html',
		link : function(scope, element, attr, ctrl) {

			/**
			 * ng-model = [
			 * 	{title, url}, {title, url}
			 * ]
			 * */
			scope.links = scope.ngModel;

			scope.navigateTo = function(url){
				$location.url(url);
			};

		},
	};
};
