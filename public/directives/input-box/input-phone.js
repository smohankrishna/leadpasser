// Allowed :0-9, +, -, (, )
lpApp.directive('phonetype', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, modelCtrl) {
			var name = function(inputValue) {
				if (inputValue == undefined) {
					inputValue = '';
				}
				var named = inputValue.replace(
						/[a-zA-Z`~!@#$%^&*_|=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				if (named !== inputValue) {
					modelCtrl.$setViewValue(named);
					modelCtrl.$render();
				}
				return named;
			};
			modelCtrl.$parsers.push(name);
			name(scope[attrs.ngModel]); // capitalize initial value
		}
	};
});
