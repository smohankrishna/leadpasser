// NO NUMBERS & NO SPECIAL CHARACTER - SPACE NOT ALLOWED
lpApp.directive('textPlain', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, modelCtrl) {
			var name = function(inputValue) {
				if (inputValue == undefined) {
					inputValue = '';
				}
				var named = inputValue.replace(
						/[0-9 `~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				if (named !== inputValue) {
					modelCtrl.$setViewValue(named);
					modelCtrl.$render();
				}
				return named;
			};
			modelCtrl.$parsers.push(name);
			name(scope[attrs.ngModel]); 
		}
	};
});

// NO NUMBERS & NO SPECIAL CHARACTER - SPACE ALLOWED
lpApp.directive('textTitle', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, modelCtrl) {
			var name = function(inputValue) {
				if (inputValue == undefined) {
					inputValue = '';
				}
				var named = inputValue.replace(
						/[0-9`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				if (named !== inputValue) {
					modelCtrl.$setViewValue(named);
					modelCtrl.$render();
				}
				return named;
			};
			modelCtrl.$parsers.push(name);
			name(scope[attrs.ngModel]); 
		}
	};
});

//NO SPECIAL CHARACTER - NUMBERS & SPACE ALLOWED
lpApp.directive('textAlphanum', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, modelCtrl) {
			var name = function(inputValue) {
				if (inputValue == undefined) {
					inputValue = '';
				}
				var named = inputValue.replace(
						/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				if (named !== inputValue) {
					modelCtrl.$setViewValue(named);
					modelCtrl.$render();
				}
				return named;
			};
			modelCtrl.$parsers.push(name);
			name(scope[attrs.ngModel]); 
		}
	};
});

//NO SPECIAL CHARACTER - NUMBERS & SPACE ALLOWED
lpApp.directive('textAlphanumNoSpace', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, modelCtrl) {
			var name = function(inputValue) {
				if (inputValue == undefined) {
					inputValue = '';
				}
				var named = inputValue.replace(
						/[ `~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				if (named !== inputValue) {
					modelCtrl.$setViewValue(named);
					modelCtrl.$render();
				}
				return named;
			};
			modelCtrl.$parsers.push(name);
			name(scope[attrs.ngModel]); 
		}
	};
});

//NO SPECIAL CHARACTER - NUMBERS & SPACE ALLOWED
lpApp.directive('textOrgName', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, modelCtrl) {
			var name = function(inputValue) {
				if (inputValue == undefined) {
					inputValue = '';
				}
				var named = inputValue.replace(
						/[`~!$%^*()|+=?;'",.<>\{\}\[\]\\\/]/gi, '');
				if (named !== inputValue) {
					modelCtrl.$setViewValue(named);
					modelCtrl.$render();
				}
				return named;
			};
			modelCtrl.$parsers.push(name);
			name(scope[attrs.ngModel]); 
		}
	};
});


//NUMBERS AND SOME SPECIAL CHARACTERS ALLOWED - SPACE,$,&,*,_,(,),+,-,:,?
lpApp.directive('textComplex', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, modelCtrl) {
			var name = function(inputValue) {
				if (inputValue == undefined) {
					inputValue = '';
				}
				var named = inputValue.replace(
						/[`~!@#^|=;'",<>\{\}\[\]\\\/]/gi, '');
				if (named !== inputValue) {
					modelCtrl.$setViewValue(named);
					modelCtrl.$render();
				}
				return named;
			};
			modelCtrl.$parsers.push(name);
			name(scope[attrs.ngModel]); 
		}
	};
});

//ALLOWED : a-z,A-Z,0-9,_
//NOT ALLOWED : NO SPECIAL CHARACTER, SPACE NOT ALLOWED
lpApp.directive('textUsername', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, modelCtrl) {
			var name = function(inputValue) {
				if (inputValue == undefined) {
					inputValue = '';
				}
				var named = inputValue.replace(
						/[ `~!@#$%^&*()|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '').toLowerCase();
				if (named !== inputValue) {
					modelCtrl.$setViewValue(named);
					modelCtrl.$render();
				}
				return named;
			};
			modelCtrl.$parsers.push(name);
			name(scope[attrs.ngModel]); 
		}
	};
});

// ALLOWED : a-z,A-Z,0-9,_
//NOT ALLOWED : NO SPECIAL CHARACTER, SPACE NOT ALLOWED
lpApp.directive('textCode', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, modelCtrl) {
			var name = function(inputValue) {
				if (inputValue == undefined) {
					inputValue = '';
				}
				var named = inputValue.replace(
						/[ `~!@#$%^&*()|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				if (named !== inputValue) {
					modelCtrl.$setViewValue(named);
					modelCtrl.$render();
				}
				return named;
			};
			modelCtrl.$parsers.push(name);
			name(scope[attrs.ngModel]); // capitalize initial value
		}
	};
});