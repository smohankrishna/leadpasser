# LeadPassers

Online system to capture and pass-on the leads to stakeholders. 

## Client

Deepak Bang from Coltivare.

## Development Team

1. Manish Gupta (Engineering)
2. Amit Kumar (Functional)
3. Amit Christian (QA)

## Technologies

1. Angular JS
2. Node JS
3. MySQL

## About Project

1. ......
2. ......
3. ......
4. ......


## Environment Variables

1. APP_EMAIL_HOST (default : smtp.zoho.com)
2. APP_EMAIL_PORT (default: 465)
3. APP_EMAIL_SECURE (true/false)
4. APP_EMAIL_USER
5. APP_EMAIL_PWD
6. APP_EMAIL_FROM (format : "name <email>")