var gulp = require('gulp');
var pump = require('pump');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var gulpUtil = require('gulp-util');
var argv = require('yargs').argv;
var zip = require('gulp-zip');
var clean = require('gulp-clean');
// ************************************************

var srcDir = "public";
var dist = "dist/";
var publishSrc = dist + "/web";
var publishWeb = dist + "/webp";
var publishSrv = dist + "/servicesp";

gulp.task('copy-web-src', function (cb) {
    pump([gulp.src(srcDir + "/**/*.*"), gulp.dest(publishSrc)], cb);
});

gulp.task('build-web-routes', ['copy-web-src'], function (cb) {
    pump([gulp.src(publishSrc + "/lib/**/*.js"),
        concat('leadpassers.min.js'), uglify(), gulp.dest(publishWeb)],
            cb);
});

gulp.task('copy-web-others', ['build-web-services'], function (cb) {
    pump([
        gulp.src([publishSrc + "/**/*.*",
            "!" + publishSrc + "/lib/**/*.*",
            "!" + publishSrc + "/leadpassers*.js",
            "!" + publishSrc + "/index*.html"]),
        gulp.dest(publishWeb)], cb);
});

gulp.task('copy-web-app', ['copy-web-others'], function (cb) {
    pump([gulp.src(publishSrc + "/leadpassers.js"), uglify(),
        rename('leadpassers.min.js'), gulp.dest(publishWeb)], cb);
});

gulp.task('copy-web-index', ['copy-web-app'], function (cb) {
    pump([gulp.src(publishSrc + "/index_deploy.html"), rename('index.html'),
        gulp.dest(publishWeb)], cb);
});

gulp.task('make-web-zip', ['copy-web-index'], function (cb) {
    pump([gulp.src(publishWeb + "/**/*.*"), zip('leadpassers-web.zip'),
        gulp.dest(dist)], cb);
});

gulp.task('clean-web-src', ['make-web-zip'], function (cb) {
    pump([gulp.src(publishSrc), clean({
            force: true
        })], cb);
});

gulp.task('clean-web-published', ['clean-web-src'], function (cb) {
    pump([gulp.src(publishWeb), clean({
            force: true
        })], cb);
});

gulp.task('make-service-zip', ['clean-web-published'], function (cb) {
    pump([
        gulp.src(["./**/*.*",
            "!./data/**/*.*", "!./dist/**/*.*",
            "!./public/**/*.*",
//							"!./node_modules/**/*.*",
            "!./gulpfile.js"]),
        zip('leadpassers-service.zip'),
        gulp.dest(dist)], cb);
});


//gulp.task('make-service-zip', ['copy-srv'], function(cb) {
//	pump([gulp.src(publishSrv + "/**/*.*"), zip('leadpassers-service.zip'),
//			gulp.dest(dist)], cb);
//});

gulp.task('clean-service-published', ['make-service-zip'], function (cb) {
    pump([gulp.src(publishSrv), clean({
            force: true
        })], cb);
});


gulp.task('default', ['clean-service-published']);
